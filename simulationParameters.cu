#include "simulationParameters.cuh"
#include "defaults.h"
#include "cuda_runtime.h"
#include "nvidiaHelperFiles\helper_cuda.h"
#include <sstream>
#include <iostream>
#include <fstream>

void SimParams::setDefaultParams()
{
		currentTime = 0;
		gridSize = make_uint2 (DEFAULT_GRID_DIMENSION, DEFAULT_GRID_DIMENSION);
		numCells = DEFAULT_GRID_DIMENSION*DEFAULT_GRID_DIMENSION;
		cellSize = DEFAULT_CELL_SIZE;
		numBodies = DEFAULT_NUM_AGENTS;
		defSpeed = DEFAULT_SPEED;
		noiseIntensity = DEFAULT_NOISE_INTESITY;
		inertia = DEFAULT_INERTIA;
		repulsionRadius = DEFAULT_REPULSION_RADIUS;
		alignmentRadius = DEFAULT_ALIGNMENT_RADIUS;
		attractionRadius = DEFAULT_ATTRACTION_RADIUS;
		attraction = DEFAULT_ATTRACTION_STRENGTH;
		alignment = DEFAULT_ALIGNMENT_STRENGTH;
		repulsion = DEFAULT_REPULSION_STRENGTH;
		numOcclusion = DEFAULT_OCCLUSION_NUMBER;
		cullProportion = DEFAULT_CULL_PROPORTION;
		fastSpeed = DEFAULT_FAST_SPEED;
		jumpDistance = DEFAULT_JUMP_DISTANCE;
		jumpProbability = DEFAULT_JUMP_PROBABILITY;
		jumpProbabilityDense = DEFAULT_JUMP_PROBABILITY_DENSE;
		activePeriod = DEFAULT_ACTIVE_PERIOD;
		ageOfMaturity = DEFAULT_AGE_OF_MATURITY;
		resumeProbability = DEFAULT_RESUME_PROBABILITY;
		resumeProbabiltyDense = DEFAULT_RESUME_PROBABILITY_DENSE;
		resumeProbabiltyLone = DEFAULT_RESUME_PROBABILITY_ALONE;
		NutrientAbundance = make_float2 (DEFAULT_FOOD_QUANTITY, DEFAULT_FOOD_REGENERATION_TIME);
		foodExperiment = DEFAULT_FOOD_EXPERIMENT;
		biteSize = DEFAULT_BITE_SIZE;
		metabolicCostP = DEFAULT_METABOLIC_COST_P;
		metabolicCostC = DEFAULT_METABOLIC_COST_C;
		maxNumBodies = 0;
		longevityLandscape = DEFAULT_LONGEVITY_LANDCAPE;
		fecundityLandscape = DEFAULT_FECUNDITY_LANDCAPE;
		survivalAgeDependancy = 0;
		predationProbability = DEFAULT_PREDATION_PROBABILITY;
		intakeTargetStddv = DEFAULT_INTAKE_TARGET_STDDV;
		initialIntakeTarget = NO_INITIAL_INTAKE_TARGET;
		numberOfIterations = DEFAULT_NUM_ITERATIONS;
		seed = NO_SEED;
		initialDistribution = InitialDistribution::UNIFORM;
		circleLocationAndRadius = make_float3 (-1, -1, -1);
		socialExperiment = DEFAULT_SOCIAL_EXPERIMENT;
		reproductionExperiment = DEFAULT_REPRODUCTION_EXPERIMENT;
		nutritionMapFile = "";
		inputFile = DEFAULT_CONFIG_FILE;
		experimentName = DEFAULT_EXPERIMENT_NAME;
		foods = std::vector<float4>();
		recordAgentNutrition = DEFAULT_RECORD_AGENT_NUTRITION;
		nextAutoSave = DEFAULT_NEXT_AUTOSAVE;
		resetStatisticsOnAutoSave = DEFAULT_RESET_STATISTICS_ON_AUTOSAVE;
		outputStatisticsOnAutoSave = DEFAULT_OUTPUT_STATISTICS_ON_AUTOSAVE;
		numPopTags = 1;
}

void SimParams::setUpSimParams(int argc, char **argv)
{
	setDefaultParams();
	getCmdLineArgument(argc, argv, "inputFile", inputFile);
	loadSimulationParamsFromFile(inputFile);
	setParamsFromCommandLine(argc, argv);
}

void SimParams::setParamsFromCommandLine(int argc, char ** argv)
{
	getCmdLineArgument(argc, argv, "currentTime", currentTime);

	getCmdLineArgument(argc, argv, "experimentName", experimentName);

	getCmdLineArgument(argc, argv, "inputFile", inputFile);

	//base simulation
	getCmdLineArgument(argc, argv, "seed", seed);

	getCmdLineArgument(argc, argv, "numberOfIterations", numberOfIterations);
	if (getCmdLineArgument(argc, argv, "gridSize", gridSize.x))
	{
		gridSize.y = gridSize.x;
		numCells = gridSize.x * gridSize.y;
	}

	getCmdLineArgument(argc, argv, "cellSize", cellSize);
	
	getCmdLineArgument(argc, argv, "numAgents", numBodies);
	
	std::string distribution;
	if (getCmdLineArgument(argc, argv, "initialDistribution", distribution))
	{
		if (distribution == "uniform")
		{
			initialDistribution = InitialDistribution::UNIFORM;
		}
		else if (distribution == "circle")
		{
			initialDistribution = InitialDistribution::CIRCLE;
			getCmdLineArgument(argc, argv, "circleX", circleLocationAndRadius.x);
			getCmdLineArgument(argc, argv, "circleY", circleLocationAndRadius.y);
			getCmdLineArgument(argc, argv, "circleR", circleLocationAndRadius.z);
		}
		else
		{
			std::cout << distribution << " is an invalid value for initialDistribution" << std::endl;
		}
	}

	
	getCmdLineArgument(argc, argv, "nextAutoSave", nextAutoSave);
	setToBoolIfFlagFound(argc, argv, "resetStatisticsOnAutoSave", true, resetStatisticsOnAutoSave);
	setToBoolIfFlagFound(argc, argv, "outputStatisticsOnAutoSave", true, outputStatisticsOnAutoSave);


	//movement
	getCmdLineArgument(argc, argv, "speed", defSpeed);
	getCmdLineArgument(argc, argv, "inertia", inertia);
	getCmdLineArgument(argc, argv, "noiseIntensity", noiseIntensity);

	//social
	setToBoolIfFlagFound(argc, argv, "social", true, socialExperiment);
	if (socialExperiment)
	{
		getCmdLineArgument(argc, argv, "attractionRadius", attractionRadius);
		getCmdLineArgument(argc, argv, "alignmentRadius", alignmentRadius);
		getCmdLineArgument(argc, argv, "repulsionRadius", repulsionRadius);

		getCmdLineArgument(argc, argv, "attraction", attraction);
		getCmdLineArgument(argc, argv, "alignment", alignment);
		getCmdLineArgument(argc, argv, "repulsion", repulsion);

		getCmdLineArgument(argc, argv, "numOcclusion", numOcclusion);
	}
	//survival of the fittest
	getCmdLineArgument(argc, argv, "cullProportion", cullProportion);

	//locust specific
	getCmdLineArgument(argc, argv, "fastSpeed", fastSpeed);

	getCmdLineArgument(argc, argv, "jumpDistance", jumpDistance);
	getCmdLineArgument(argc, argv, "jumpProbability", jumpProbability);
	getCmdLineArgument(argc, argv, "jumpProbabilityDense", jumpProbabilityDense);

	getCmdLineArgument(argc, argv, "activePeriod", activePeriod);

	getCmdLineArgument(argc, argv, "pausePeriod", ageOfMaturity);
	getCmdLineArgument(argc, argv, "ageOfMaturity", ageOfMaturity);

	getCmdLineArgument(argc, argv, "resumeProbability", resumeProbability);
	getCmdLineArgument(argc, argv, "resumeProbabiltyDense", resumeProbabiltyDense);
	getCmdLineArgument(argc, argv, "resumeProbabiltyLone", resumeProbabiltyLone);

	//food things
	getCmdLineArgument(argc, argv, "foodQuantity", NutrientAbundance.x);
	getCmdLineArgument(argc, argv, "foodRegenerationTime", NutrientAbundance.y);
	setToBoolIfFlagFound(argc, argv, "foodExperiment", true, foodExperiment);
	std::string foodsString;
	getCmdLineArgument(argc, argv, "foods", foodsString);
	addFoods(foodsString);

	getCmdLineArgument(argc, argv, "nutritionMapFile", nutritionMapFile);
	getCmdLineArgument(argc, argv, "recordAgentNutrition", recordAgentNutrition);
	getCmdLineArgument(argc, argv, "biteSize", biteSize);
	getCmdLineArgument(argc, argv, "metabolicCostC", metabolicCostC);
	getCmdLineArgument(argc, argv, "metabolicCostP", metabolicCostP);
	
	getCmdLineArgument(argc, argv, "intakeTargetStddv", intakeTargetStddv);
	getCmdLineArgument(argc, argv, "initialIntakeTarget", initialIntakeTarget);

	//reproduction things
	setToBoolIfFlagFound(argc, argv, "reproduction", true, reproductionExperiment);
	if (reproductionExperiment)
	{
		getCmdLineArgument(argc, argv, "nMax", maxNumBodies);
		if (maxNumBodies == 0)
			maxNumBodies = numBodies * 2;
		
		getCmdLineArgument(argc, argv, "longevityLandscape", longevityLandscape);
		getCmdLineArgument(argc, argv, "fecundityLandscape", fecundityLandscape);

		getCmdLineArgument(argc, argv, "survivalAgeDependancy", survivalAgeDependancy);
		getCmdLineArgument(argc, argv, "predationProbability", predationProbability);
		getCmdLineArgument(argc, argv, "intakeTargetStddv", intakeTargetStddv);
	}
	else
	{
		maxNumBodies = numBodies;
	}

	getCmdLineArgument(argc, argv, "numPopTags", numPopTags);

}

void SimParams::saveToFile(const std::string & filename)
{
	std::ofstream file(filename);
	output(file);
}

//this function makes me weep, I wish there was a better way
bool equal(const GPUSimParams &p1,const GPUSimParams &p2)
{
	//we don't worry about float comparisons since the variables of one should have been 
	//initialized from the other (we're not checking for equality in floating point math)
	if (
		p1.currentTime == p2.currentTime &&
		p1.gridSize.x == p2.gridSize.x && p1.gridSize.y == p2.gridSize.y &&
		p1.numCells == p2.numCells &&
		p1.cellSize == p2.cellSize &&
		p1.numBodies == p2.numBodies &&
		p1.defSpeed == p2.defSpeed &&
		p1.noiseIntensity == p2.noiseIntensity &&
		p1.inertia == p2.inertia &&
		p1.repulsionRadius == p2.repulsionRadius &&
		p1.alignmentRadius == p2.alignmentRadius &&
		p1.attractionRadius == p2.attractionRadius &&
		p1.attraction == p2.attraction &&
		p1.alignment == p2.alignment &&
		p1.repulsion == p2.repulsion &&
		p1.numOcclusion == p2.numOcclusion &&
		p1.cullProportion == p2.cullProportion &&
		p1.fastSpeed == p2.fastSpeed &&
		p1.jumpDistance == p2.jumpDistance &&
		p1.jumpProbability == p2.jumpProbability &&
		p1.jumpProbabilityDense == p2.jumpProbabilityDense &&
		p1.activePeriod == p2.activePeriod &&
		p1.ageOfMaturity == p2.ageOfMaturity &&
		p1.resumeProbability == p2.resumeProbability &&
		p1.resumeProbabiltyDense == p2.resumeProbabiltyDense &&
		p1.resumeProbabiltyLone == p2.resumeProbabiltyLone &&
		p1.NutrientAbundance.x == p2.NutrientAbundance.x && p1.NutrientAbundance.y == p2.NutrientAbundance.y &&
		p1.foodExperiment == p2.foodExperiment &&
		p1.maxNumBodies == p2.maxNumBodies &&
		p1.longevityLandscape == p2.longevityLandscape &&
		p1.fecundityLandscape == p2.fecundityLandscape &&
		p1.survivalAgeDependancy == p2.survivalAgeDependancy &&
		p1.predationProbability == p2.predationProbability &&
		p1.intakeTargetStddv == p2.intakeTargetStddv
		)
	{
		return true;
	}

	return false;

}


bool SimParams::loadSimulationParamsFromFile(const std::string &filename)
{
	int number_of_lines = 0;
	std::string line;
	std::ifstream myfile(filename, std::ifstream::in);
	if (!myfile.is_open())
	{
		std::cout << "could not open " << filename << " for reading" << std::endl;
		return false;
	}
	std::vector<std::string> lines;
	while (std::getline(myfile, line))
	{
		++number_of_lines;
		lines.push_back(trim(line));
	}
	int argc = lines.size() + 1;
	char ** argv = new char*[argc];
	for (int i = 0; i < lines.size(); ++i)
	{
		argv[i + 1] = new char[lines.at(i).size() + 1];
		strcpy(argv[i + 1], lines.at(i).c_str());
	}
	setParamsFromCommandLine(argc, argv);
	for (int i = 0; i < lines.size(); ++i)
	{
		delete[] argv[i + 1];
	}
	return true;
}

void SimParams::addFoods(std::string foodsString)
{
	std::vector<std::string> foodStrings = split(foodsString, ";");
	for (std::string foodString : foodStrings)
	{
		std::vector<std::string> components = split(foodString, ",");
		if (components.size() != 4)
		{
			std::cout << "food " << foodString << "is badly formed" << std::endl;
			std::cout << "should be: ProteinConcentration,CarbConcentration,FractalDimension,Abundance all as floats seperated by a comma eg \"0.3,0.7,2.3,1\"" << std::endl;
			continue;
		}
		float4 food;
		food.x = stof(components[0]);
		food.y = stof(components[1]);
		food.z = stof(components[2]);
		food.w = stof(components[3]);
		foods.push_back(food);

	}

	if (foods.size() != 0)
	{
		foodExperiment = true;
	}
}

void SimParams::output(std::ostream &output) const
{
	output << "currentTime=" << currentTime << std::endl;
	output << "experimentName=" << experimentName << std::endl;
	if(!inputFile.empty())
		output << "inputFile=" << inputFile << std::endl;

	output << "gridSize=" << gridSize.x << std::endl;
	output << "gridCells=" << numCells << std::endl;
	output << "cellSize=" << cellSize << std::endl;
	output << "seed=" << seed << std::endl;
	output << "numAgents=" << numBodies << std::endl;

	if (initialDistribution == InitialDistribution::UNIFORM)
	{
		output << "initialDistribution=" << "uniform" << std::endl;
	}
	if (initialDistribution == InitialDistribution::CIRCLE)
	{
		output << "initialDistribution=" << "circle" << std::endl;
		output << "circleX=" << circleLocationAndRadius.x << std::endl;
		output << "circleY=" << circleLocationAndRadius.y << std::endl;
		output << "circleR=" << circleLocationAndRadius.z << std::endl;

	}

	output << "numberOfIterations=" << numberOfIterations << std::endl;
	output << "speed=" << defSpeed << std::endl;
	output << "inertia=" << inertia << std::endl;
	output << "noiseIntensity=" << noiseIntensity << std::endl;
	if (socialExperiment)
	{
		output << "social";
		output << "attractionRadius=" << attractionRadius << std::endl;
		output << "alignmentRadius=" << alignmentRadius << std::endl;
		output << "repulsionRadius=" << repulsionRadius << std::endl;
		output << "attractionStrength=" << attraction << std::endl;
		output << "alignmentStrength=" << alignment << std::endl;
		output << "repulsionStrength=" << repulsion << std::endl;
		output << "numOcclusion=" << numOcclusion << std::endl;
	}

	output << "fastSpeed=" << fastSpeed << std::endl;
	output << "jumpDistance=" << jumpDistance << std::endl;
	output << "jumpProbability=" << jumpProbability << std::endl;
	output << "jumpProbabilityDense=" << jumpProbabilityDense << std::endl;
	output << "activePeriod=" << activePeriod << std::endl;
	output << "ageOfMaturity=" << ageOfMaturity << std::endl;
	output << "resumeProbability=" << resumeProbability << std::endl;
	output << "resumeProbabiltyDense=" << resumeProbabiltyDense << std::endl;
	output << "resumeProbabiltyLone=" << resumeProbabiltyLone << std::endl;
	
	if (foodExperiment)
	{
		output << "foodExperiment" << std::endl;

		output << "foodQuantity=" << NutrientAbundance.x << std::endl;
		output << "foodRegenerationTime=" << NutrientAbundance.y << std::endl;
		if(nutritionMapFile!="")
			output << "nutritionMapFile=" << nutritionMapFile << std::endl;
		if(!foods.empty())
		{
			output << "foodSyntax={ProteinConcentration,CarbConcentration,FractalDimension,Abundance}" << std::endl;
			output << "foods=" ;
			bool first = true;
			for (float4 food: foods)
			{
				//the elements after the first are preceded by the element delimiter
				if(!first)
					output << ";";
				output << food.x << "," << food.y << "," << food.z << "," << food.w;
				first = false;
			}
			output << std::endl;
		}
		output << "recordAgentNutrition=" << recordAgentNutrition << std::endl;
		output << "biteSize=" << biteSize << std::endl;
		output << "metabolicCostC=" << metabolicCostC << std::endl;
		output << "metabolicCostP=" << metabolicCostP << std::endl;
		output << "intakeTargetStddv=" << intakeTargetStddv << std::endl;
		output << "initialIntakeTarget=" << initialIntakeTarget.x << "," << initialIntakeTarget.y << std::endl;

	}
	if (reproductionExperiment)
	{
		output << "reproduction" << std::endl;
		output << "nMax=" << maxNumBodies << std::endl;
		output << "cullProportion=" << cullProportion << std::endl;
		output << "landscapeSyntax(2Dgaussian)=max,x0,y0,x_stddv,y_stddv" << std::endl;
		output << "longevityLandscape=" << longevityLandscape << std::endl;
		output << "fecundityLandscape=" << fecundityLandscape << std::endl;
		output << "survivalAgeDependancy=" << survivalAgeDependancy << std::endl;
		output << "predationProbability=" << predationProbability << std::endl;
		output << "intakeTargetStddv=" << intakeTargetStddv << std::endl;

	}
	output << "nextAutoSave=" << nextAutoSave << std::endl;
	if(resetStatisticsOnAutoSave)
		output << "resetStatisticsOnAutoSave" << std::endl;
	if(outputStatisticsOnAutoSave)
		output << "outputStatisticsOnAutoSave" << std::endl;
	output << "numPopTags=" << numPopTags << std::endl;
}

