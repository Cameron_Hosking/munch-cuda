#pragma once
#include <cuda_runtime.h>
#include "vector_functions.h"
#include "DeviceArrays.cuh"
#include "helper.cuh"


typedef unsigned int uint;

#define CR /*const*/ __restrict__

namespace kernelImplementation {
	void syncHostAndDeviceSimParams(GPUSimParams hostParams);
	void syncDeviceArrays(DeviceArrays *newDeviceArrays);
}
__global__ void findFirstNoHash(uint * hashes, uint length, uint * result);

__device__ float randomNumber(uint4 &state);

__global__ void integrate();

__device__ int2 calcGridPos(float4 p);

__device__ uint calcGridHash(int2 gridPos);

__global__ void calcHashD();

__global__ void calcHashWithReproductionAndDeathD();

__global__ void findCellStartD();

__global__ void reorderDataD();

__global__ void collideD();

__global__ void eatD();

__global__ void forageD();

__global__ void reproductionLifeOrDeathD(uint * ageAtDeath, uint * nutritionalStatesByAge, uint * nutritionalStatesAtMaturity, uint * aggregateNutritionAtDeath, uint64_t * lifetimeReproductiveOutput, uint64_t * birthsAtAge, uint * intakeTargets);

__global__ void reproductionD();

__global__ void cullD();

__global__ void updateNutrientAbundanceD();

__global__ void calcAvailableNutrients();