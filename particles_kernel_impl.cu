
#ifndef CUDART_PI_F
#define CUDART_PI_F         3.141592654f
#endif

#ifndef CUDART_TWO_PI_F
#define CUDART_TWO_PI_F         2.0f*3.141592654f
#endif

#include <stdio.h>
#include <math.h>
#include "math_constants.h"
#include "simulationParameters.cuh"
#include "defaults.h"
#include "particles_kernel_impl.cuh"
#include "deviceArrays.cuh"
#include "nvidiaHelperFiles\helper_cuda.h"
#include "PRNG/PRNG.cuh"

//this whole annoying duplication and consistency checking shenanigan is because currently
//device constants are local to each module and therefore I need a seperate constant for each .cu file =(
//if this changes in a future CUDA release (currently CUDA 8.0rc) this workaround can be refactored away
__constant__ GPUSimParams params;
__constant__ DeviceArrays dev;
GPUSimParams paramsSnapshotB;
void kernelImplementation::syncHostAndDeviceSimParams(GPUSimParams hostParams)
{
	if (!equal(hostParams, paramsSnapshotB))
	{
		cudaDeviceSynchronize();
		// copy parameters to constant memory
		checkCudaErrors(cudaMemcpyToSymbol(params, &hostParams, sizeof(GPUSimParams)));
		paramsSnapshotB = hostParams;
	}

}
void kernelImplementation::syncDeviceArrays(DeviceArrays *newDeviceArrays)
{
	checkCudaErrors(cudaMemcpyToSymbol(dev, newDeviceArrays, sizeof(DeviceArrays)));
}
__global__ void findFirstNoHash(uint * hashes, uint length, uint * result)
{
	uint index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index >= length)
		return;
	if (hashes[index] == NO_HASH)
	{
		if (index == 0||hashes[index-1]!=NO_HASH)//if this value is no hash and the one before isn't then this is the first NO_HASH
		{//we rely on the short circuit evaluation to not check hashes[-1]
			*result = index;
		}
	}
	else if (index == length - 1)//if this is the last element and it's not NO_HASH we return length
	{
		*result = length;
	}
}


//change newVel to maxTheta from oldVel if newVel is greater than maxTheta from oldVel
__device__ inline void changeDirection(float2 &oldVel, float2 &newVel, float maxTheta)
{
	if (cosf(maxTheta) > dot(newVel, oldVel))//if the desired new velocity differs in direction by more than the possible turning angle
	{
		float oldVelTheta = atan2f(oldVel.y, oldVel.x);
		float phi = oldVelTheta + maxTheta;
		float2 newVel1 = make_float2(cosf(phi), sinf(phi));
		phi = oldVelTheta - maxTheta;
		float2 newVel2 = make_float2(cosf(phi), sinf(phi));
		if (dot(newVel, newVel1 - newVel2) > 0)
		{
			newVel = newVel1;
		}
		else
		{
			newVel = newVel2;
		}
	}
}

__inline__ __device__ void wrapD(float &valToWrap, float wrapTo)
{
	if (valToWrap > wrapTo){
		valToWrap -= wrapTo;
	}
	if (valToWrap < 0.0f){
		valToWrap += wrapTo;
	}
}

// integrate. Particles decisions about stopping/starting to move, their speed is determined according to timer, prob to jump, then their position is updated
__global__
void integrate()
{
	uint index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index >= params.numBodies)
		return;    // handle case when no. of particles not multiple of block size
	dev.timer[index] += 1.0;
	uint4 rngState = dev.RNGStatesForAgents[index];
	float4 posvel = dev.posVel[index];
	float2 oldVel = make_float2(dev.oldPosVel[index].z, dev.oldPosVel[index].w);
	float2 vel = make_float2(posvel.z, posvel.w);
	
	float2 pos = make_float2(posvel.x, posvel.y);
	unsigned short crowding = dev.crowding[index];
	float minNoise = 0.01f;
	float maxNoise = 0.5f;
	float nutritionalDist = length(dev.agentPC[index] - dev.intakeTargets[index]);
	float noise = maxNoise - nutritionalDist*0.25f;
	if (noise < minNoise)
		noise = minNoise;
	if (vel.x == 0 && vel.y == 0)//agent has no preferred velocity
	{
		vel = oldVel;
	}
	vel = vel * (1.0f - noise);

	//STEP 1 NOISE

	float rand_dir = randomNumber(rngState) * CUDART_TWO_PI_F;

	vel.x += (noise * __cosf(rand_dir));    //noise component of the new vel
	vel.y += (noise * __sinf(rand_dir));

	// INERTIA
	vel = normalize(vel);
	oldVel = normalize(oldVel);

	//if (deltaTheta < CUDART_PI_F) //if maximum turning angle is more than Pi than any direction is possible
	//{
	//	changeDirection(oldVel, vel, deltaTheta);
	//}
	
	
	//jump probabilty
	float JumpProba = params.jumpProbability;
	if (crowding >= 999.0f)
	{
		JumpProba += params.jumpProbabilityDense;
	}
	//jump
	if (randomNumber(rngState) < JumpProba)
	{
		pos += vel*params.jumpDistance;
	}

	//move
	pos += vel*params.defSpeed;


	//STEP 6 BOUNDARIES
	//periodic boundaries
	wrapD(pos.x, params.cellSize * params.gridSize.x);
	wrapD(pos.y, params.cellSize * params.gridSize.y);

	// store new position and velocity
	dev.posVel[index] = make_float4(pos.x, pos.y, vel.x, vel.y);
	dev.crowding[index] = 0;
	//store the updated random state
	dev.RNGStatesForAgents[index] = rngState;
}

// calculate position in uniform grid
__device__ int2 calcGridPos(float4 p) {
	int2 gridPos;
	gridPos.x = floor((p.x) / params.cellSize);
	gridPos.y = floor((p.y) / params.cellSize);

	return gridPos;
}

// calculate position in uniform grid
__device__ int2 calcGridPos(float2 p)
{
	int2 gridPos;
	gridPos.x = floor((p.x) / params.cellSize);
	gridPos.y = floor((p.y) / params.cellSize);

	return gridPos;
}

// calculate address in grid from position (clamping to edges)
__device__ uint calcGridHash(int2 gridPos) {
	gridPos.x = gridPos.x & (params.gridSize.x - 1); // wrap grid, assumes size is power of 2
	gridPos.y = gridPos.y & (params.gridSize.y - 1);
	return gridPos.y * params.gridSize.x + gridPos.x;
}

// calculate grid hash value for each particle
__global__
void calcHashD()
{
	uint index = blockIdx.x*blockDim.x + threadIdx.x;

	if (index >= params.numBodies)
		return;

	// get address in grid
	int2 gridPos = calcGridPos(dev.posVel[index]);
	uint hash = calcGridHash(gridPos);

	// store grid hash and particle index
	dev.gridParticleHash[index] = hash;
	dev.gridParticleIndex[index] = index;
}

__global__ void calcHashWithReproductionAndDeathD()
{
	uint index = blockIdx.x*blockDim.x + threadIdx.x;
	if (index >= params.maxNumBodies)
		return;
	//this makes sure that within the entire gridParticleHash array the number of values!=NO_HASH is exactly equal to the number of living agents
	if (index >= params.numBodies)
	{
		dev.gridParticleHash[2 * index] = NO_HASH;
		dev.gridParticleHash[2 * index + 1] = NO_HASH;
		return;
	}

	// get address in grid
	uint hash = calcGridHash(calcGridPos(dev.posVel[index]));
		
	char reproductionState = dev.reproductionStates[index];

	if (reproductionState == 0)
	{
		//dead agents are represented as NO_HASH
		//when the agents are subsequently sorted by hash value all the dead agents will end up at the end of the array.
		hash = NO_HASH;
	}

	//state of the parent
	dev.gridParticleHash[2*index] = hash;

	if (reproductionState == 1)
	{
		//agents who are not reproducing set their offspring hash values to NO_HASH 
		hash = NO_HASH;
	}
	//state of the offspring
	dev.gridParticleHash[2*index + 1] = hash;

	dev.gridParticleIndex[2*index] = index;
	
	//children are identified to their parents by having the same index as their parents
	//when we later stable sort by hash, children will be adjacent to their parents.
	//we can then identify children parent pairs by two adjacent agents with identical gridParticleIndex values.
	dev.gridParticleIndex[2*index+1] = index;

	//at the end of this kernel gridParticleHash is an array of hash values of length numAgents*2 where any 
	//value other than NO_HASH represents a living agent.
}


//find the start of each cell in the sorted hash array
__global__ void findCellStartD()
{
	extern __shared__ uint sharedHash[];    // blockSize + 1 elements
	uint index = blockIdx.x * blockDim.x + threadIdx.x;

	uint hash;
	// handle case when no. of particles not multiple of block size
	if (index < params.numBodies)
	{
		hash = dev.gridParticleHash[index];

		// Load hash data into shared memory so that we can look
		// at neighboring particle's hash value without loading
		// two hash values per thread
		sharedHash[threadIdx.x + 1] = hash;
		if (threadIdx.x == 0)
		{
			if (index > 0)
			{
				// first thread in block must load neighbor particle hash
				sharedHash[0] = dev.gridParticleHash[index - 1];
			}
			else
			{
				//there is no neighbor particle if this is the first particle
				sharedHash[0] = NO_HASH;
			}
		}

	}

	__syncthreads();

	if (index < params.numBodies)
	{
		// If this particle has a different cell index to the previous
		// particle then it must be the first particle in the cell,
		// so store the index of this particle in the cell.
		// As it isn't the first particle, it must also be the cell end of
		// the previous particle's cell

		if (hash != sharedHash[threadIdx.x])
		{
			dev.cellStart[hash] = index;

			if (index > 0)
				dev.cellEnd[sharedHash[threadIdx.x]] = index;
		}

		if (index == params.numBodies - 1)
		{
			dev.cellEnd[hash] = index + 1;
		}
	}
}


// rearrange particle data into sorted order 
__global__
void reorderDataD()
{
	uint index = blockIdx.x * blockDim.x + threadIdx.x;

	if (index < params.numBodies) {
		// Now use the sorted index to reorder the pos and vel 
		//note that offspring have the same sorted index as their parent, 
		//values that aren't cloned should be given new values in the reproduction function
		uint sortedIndex = dev.gridParticleIndex[index];
		dev.oldPosVel[index] = dev.posVel[sortedIndex];
		dev.crowdingAlt[index] = dev.crowding[sortedIndex];
		dev.timerAlt[index] = dev.timer[sortedIndex];
		dev.ageAtMaturityAlt[index] = dev.ageAtMaturity[sortedIndex];
		dev.densityAlt[index] = dev.density[sortedIndex];
		dev.eatingAlt[index] = dev.eating[sortedIndex];
		dev.agentPCAlt[index] = dev.agentPC[sortedIndex];
		dev.intakeTargetsAlt[index] = dev.intakeTargets[sortedIndex];
		dev.socialRangesAlt[index] = dev.socialRanges[sortedIndex];
		dev.socialStrengthsAlt[index] = dev.socialStrengths[sortedIndex];
		dev.aggregateNutritionAlt[index] = dev.aggregateNutrition[sortedIndex];
		dev.lifetimeReproductiveOutputAlt[index] = dev.lifetimeReproductiveOutput[sortedIndex];
		dev.populationTagAlt[index] = dev.populationTag[sortedIndex];

	}

}

__global__
void collideD() {
	uint index = blockIdx.x * blockDim.x + threadIdx.x;

	if (index >= params.numBodies)
		return;
	// read particle data from sorted arrays
	//float4 posvel = oldPosVel[index];
	float2 pos = make_float2(dev.oldPosVel[index].x, dev.oldPosVel[index].y);

	float3 socialRange = dev.socialRanges[index];
	float3 socialStrength = dev.socialStrengths[index];

	// get address in grid
	int2 gridPos = calcGridPos(pos);
	
	// examine neighbouring cells
	float2 force = make_float2(0,0);
	float2 attractionForce = make_float2(0, 0);

	uint gridHash;

	//float inertia=params.inertia;
	uint crowding = 0;
	uint density = 0;
	gridPos.x -= 1;
	gridPos.y -= 1;
	for (int y = -1; y <= 1; y++) {
		for (int x = -1; x <= 1; x++) {
			gridHash = calcGridHash(gridPos);
			// get start of bucket for this cell
			uint startIndex = dev.cellStart[gridHash];
			
			//take into account looping to the other side of the grid
			//although this looks like a lot of brancing, in almost all 
			//cases agents in the same warp will take the same branches
			float2 wrappedPos = pos;
			if (gridPos.x == params.gridSize.x)
				wrappedPos.x -= float(params.gridSize.x * params.cellSize);
			else if (gridPos.x == -1)
				wrappedPos.x += float(params.gridSize.x * params.cellSize);

			if (gridPos.y == params.gridSize.y)
				wrappedPos.y -= float(params.gridSize.y * params.cellSize);
			else if (gridPos.y == -1)
				wrappedPos.y += float(params.gridSize.y * params.cellSize);

			if (startIndex != 0xffffffff)          // cell is not empty
			{
				// iterate over particles in this cell
				uint endIndex = dev.cellEnd[gridHash];

				for (uint j = startIndex; j < endIndex; ++j) 
				{
					if (j == index) 
					{
						++j;
						if (j == endIndex)
							break;
					}
					float4 posvel2 = dev.oldPosVel[j];
					// interact with neighbours within range
					float dist = length(make_float2(posvel2.x - wrappedPos.x, posvel2.y - wrappedPos.y));
					if (dist < socialRange.x)
					{
						density += 1;
						//if crowding is greater than numOcclusion then agents further away than alignment range are ignored
						if (dist < socialRange.y || crowding < params.numOcclusion)
						{
							//if the distance between the two agents is zero we skip this collision since it will lead to division by zero and it makes no physical sense anyway
							if (dist == 0) continue;
							
							//use of tmp reduces duplication of computations
							//all forces scale inversly proportional to distance so that is the initial tmp value.
							float tmp = __fdividef(1, dist);

							//this looks like a lot of branching however usually all threads in a warp will progress down just one branch, or at most two.
							if (dist < socialRange.z)
							{
								//we divide by distance again to normalize the vector 
								tmp *= tmp*socialStrength.z;
								//note the direction of force is opposite to that of attraction
								force.x += tmp * (wrappedPos.x - posvel2.x);
								force.y += tmp * (wrappedPos.y - posvel2.y);
								crowding += 999;
							}
							else if (dist < socialRange.y)
							{	
								//if the other agent has zero velocity it is ignored
								if (posvel2.z != 0 || posvel2.w != 0)
								{
									force += tmp* socialStrength.y * normalize(make_float2(posvel2.z, posvel2.w));
									crowding += 1;
								}
							}
							else if (crowding < params.numOcclusion && dist<socialRange.x)
							{
								//we divide by distance again to normalize the vector 
								tmp *= tmp*socialStrength.x;
								attractionForce.x += tmp * (posvel2.x - wrappedPos.x);
								attractionForce.y += tmp * (posvel2.y - wrappedPos.y);
							}
						}
						
					}	
				}
			}
			gridPos.x += 1;
		}
		gridPos.y += 1;
		gridPos.x -= 3;
	}

	if (crowding <= params.numOcclusion) //attraction only occurs if near neighbours are not creating occlusion
	{
		force.x += socialStrength.x * attractionForce.x; // the attraction force is added on the top of repulsion/alignment
		force.y += socialStrength.x * attractionForce.y;
	}

	//there was a net force on the particle
	if (force.x != 0 || force.y != 0)
	{
		//normalize social vel
		force = normalize(force);
		
		// write new velocity back to original location
		dev.posVel[index] = make_float4(pos.x, pos.y, force.x, force.y);
		
	}
	
	dev.crowding[index] = crowding;
	//printf("app n %i\n",crowding);
	dev.density[index] = density;
}


__device__ float nutritionalScore(float2 food, float2 currentNutritionalState, float2 intakeTarget)
{
	float beta = 10.0/params.biteSize;

	float currentDistance = length(intakeTarget - currentNutritionalState);
	float2 delta = food*params.biteSize - make_float2(params.metabolicCostP, params.metabolicCostC);
	float newDistance = length(intakeTarget - (currentNutritionalState + delta));

	return __expf(beta * currentDistance * (currentDistance-newDistance));
}

__global__
void eatD()
{
	uint index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index >= params.numBodies)
		return;
	
	float2 agentPC = dev.agentPC[index];
	
	uint gridHash = calcGridHash(calcGridPos(dev.posVel[index]));
	float2 cellsPCcontent = make_float2(0, 0);
	if (dev.nutrientAbundance[gridHash].x>0)
		cellsPCcontent = dev.nutrientMap[gridHash];
	
	if (dev.eating[index])
	{
		agentPC.x += params.biteSize * cellsPCcontent.x;
		agentPC.y += params.biteSize * cellsPCcontent.y;
	}

	//simulating a moving target
	//PC values can't drop below 0
	//remove this later, perhaps agent with negative values dies.
	agentPC.x -= params.metabolicCostP;
	agentPC.y -= params.metabolicCostC;

	//if (agentPC.x < 0)
	//	agentPC.x = 0;
	//if(agentPC.y < 0)
	//	agentPC.y = 0;

		
	
	dev.agentPC[index] = agentPC;
}

__global__
void forageD() {
	uint index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index >= params.numBodies)
		return;
	float2 agentPC = dev.agentPC[index];
	float2 intakeTarget = dev.intakeTargets[index];

	uint4 rngState = dev.RNGStatesForAgents[index];


	float scoresSum = 0;
	float4 thisPosVel = dev.posVel[index];
	int2 gridPos = calcGridPos(thisPosVel);
	float xPos = thisPosVel.x - gridPos.x * params.cellSize;
	float yPos = thisPosVel.y - gridPos.y * params.cellSize;

	//the displacement of the center of the top left cell from the agent
	//float2 dir = make_float2(- params.cellSize, - params.cellSize );// make_float2(-0.5*params.cellSize - xPos, -0.5*params.cellSize - yPos);
	float2 averageDir;
	float2 chosenDirection = make_float2(-1, -1);// dir;
	for (int y = -1; y <= 1; y++) {
		for (int x = -1; x <= 1; x++) {
			uint gridHash = calcGridHash(gridPos + make_int2(x, y));
			float2 cellsPCcontent = make_float2(0, 0);
			if(dev.nutrientAbundance[gridHash].x>0)
				cellsPCcontent = dev.nutrientMap[gridHash];
			float score = nutritionalScore(cellsPCcontent, agentPC, intakeTarget);
			scoresSum += score;
			if (randomNumber(rngState) < score/scoresSum)
			{
				chosenDirection = make_float2(x,y);
			}

			//If this is the cell we are in, check if we want to eat by comparing the score to that of eating nothing.
			if (x == 0 && y == 0)
			{
				if (randomNumber(rngState) < score / (score + nutritionalScore(make_float2(0, 0), agentPC, intakeTarget)))
				{
					dev.eating[index] = true;
				}
				else
				{
					dev.eating[index] = false;
				}
				
			}
		}
	}
	if (chosenDirection.x == 0 && chosenDirection.y == 0)// this means it is in the desired square
	{
		chosenDirection = make_float2(0.5*params.cellSize - xPos, 0.5*params.cellSize - yPos);
	}

	//normalize the vector
	if(chosenDirection.x!=0 || chosenDirection.y!=0)
		chosenDirection = normalize(chosenDirection);
	float2 oldVel = make_float2(thisPosVel.z, thisPosVel.w);
	changeDirection(oldVel, chosenDirection, params.inertia);
	dev.posVel[index] = make_float4(thisPosVel.x, thisPosVel.y, chosenDirection.x, chosenDirection.y);

	dev.RNGStatesForAgents[index] = rngState;
		

}

__global__ void reproductionLifeOrDeathD(uint * ageAtDeath, uint * nutritionalStatesByAge, uint * nutritionalStatesAtMaturity, uint * aggregateNutritionAtDeath, uint64_t * lifetimeReproductiveOutput, uint64_t * birthsAtAge, uint * intakeTargets)
{
	uint index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index >= params.numBodies)
		return;
	float2 PC = dev.agentPC[index];
	uint4 rngState = dev.RNGStatesForAgents[index];
	char reproductionState = 1;
	float age = dev.timer[index];
	
	//only agents older than ageOfMaturity (currently defined as larval stage) can reproduce or die of old age. this decision is arbitrary
	if (age > dev.ageAtMaturity[index])
	{
		//add current PC to aggregate nutrition
		dev.aggregateNutrition[index] += PC;

		float probabilityOfDeath = 1.0f/params.longevityLandscape.eval(PC);
		float probabilityOfReproducing = params.fecundityLandscape.eval(PC);
		
		if (randomNumber(rngState) < probabilityOfDeath)
		{
			reproductionState = 0;
		}
		else if (randomNumber(rngState) < probabilityOfReproducing)
		{
			reproductionState = 2;
		}	
		//adult predation
		if (randomNumber(rngState) < params.predationProbability)
		{
			reproductionState = 0;
		}
	}
	else if (randomNumber(rngState) < 0.0004)	//juvenile predation
	{
		reproductionState = 0;
	}

	int nutritionIndex = 300 * __float2uint_rd(100.0f*(PC.x + 1.0f)) + __float2uint_rd(100.0f*(PC.y + 1.0f));
	bool nutritionIndexInBounds = PC.x >= -1 && PC.y >= -1 && PC.x < 2 && PC.y < 2;
	if (nutritionIndexInBounds)
	{
		int ageBracket = 32 - __clz(((int)age) >> 10);// ie brackets are <1024,<2048,<4096,<8192 etc
		//nutritionalScore is a 3D array where the first dimension is age/500, the second dimension is x NS+1 * 100 
		//(since it varies between -1 and 2 and we want 300 subdivisions) and the last dimension is y NS+1 * 100;
		
		atomicAdd(nutritionalStatesByAge + ageBracket * 90000 + nutritionIndex, 1);
		if (__float2int_rn(age) == __float2int_rn(dev.ageAtMaturity[index]))// record nutrition at maturity
		{
			atomicAdd(nutritionalStatesAtMaturity + nutritionIndex, 1);
		}
	}

	if (reproductionState == 0)
	{
		atomicAdd(ageAtDeath + __float2uint_rn(age), 1);
		if (age > dev.ageAtMaturity[index])
		{
			float2 avgPC = dev.aggregateNutrition[index] / (age - dev.ageAtMaturity[index]);
			atomicAdd(aggregateNutritionAtDeath + 300 * __float2uint_rd(100.0f*(avgPC.x + 1.0f)) + __float2uint_rd(100.0f*(avgPC.y + 1.0f)), 1);
			atomicAdd(lifetimeReproductiveOutput + dev.lifetimeReproductiveOutput[index], 1);
		}
		atomicAdd(intakeTargets + 300 * __float2uint_rd(100.0f*dev.intakeTargets[index].x) + __float2uint_rd(100.0f*dev.intakeTargets[index].y), age);
	}
	else if (reproductionState == 2)
	{
		//increase the counter saying how many offspring this agent had
		dev.lifetimeReproductiveOutput[index] += 1;
		atomicAdd(birthsAtAge + __float2uint_rn(age), 1);
	}
	dev.reproductionStates[index] = reproductionState;
	dev.RNGStatesForAgents[index] = rngState;
}

__global__ void reproductionD()
{
	uint index = blockIdx.x * blockDim.x + threadIdx.x;
	//this function works on parents which are at index child-1. This means that a parent cannot exist at the end of the array
	if (index >= params.numBodies-1)
		return;
	//if not a parent return
	if (dev.gridParticleIndex[index] != dev.gridParticleIndex[index + 1])
		return;
	uint parentIndex = index;
	uint childIndex = index + 1;
	uint4 rngState = dev.RNGStatesForAgents[parentIndex];
	//nutrients passed to child
	dev.agentPC[childIndex] = make_float2(0,0);

	//impart nutritional cost of reproduction on parent
	//agentPC[parentIndex] = agentPC[parentIndex] * 0.75f;

	//slight alteration in intakeTarget
	dev.intakeTargets[childIndex] = randomGuassian(dev.intakeTargets[parentIndex], make_float2(params.intakeTargetStddv, params.intakeTargetStddv), rngState);

	//let age of maturity vary normally with a stdv of 100
	//dev.ageAtMaturity[index + 1] = dev.ageAtMaturity[index] + 100.0f*randomGuassian(rngState).x;

	//set initial states that aren't cloned from parent (these are set during reorder step)
	dev.timer[childIndex] = 0;
	dev.aggregateNutrition[childIndex] = make_float2(0, 0);
	dev.lifetimeReproductiveOutput[childIndex] = 0;

	dev.RNGStatesForAgents[index] = rngState;
}

__global__
void cullD() {
	uint index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index >= params.maxNumBodies)
		return;
	uint4 rngState = dev.RNGStatesForAgents[index];
	if (randomNumber(rngState) < params.cullProportion)
	{
		dev.gridParticleHash[2*index] = NO_HASH;
	}
	if (randomNumber(rngState) < params.cullProportion)
	{
		dev.gridParticleHash[2*index+1] = NO_HASH;
	}
	dev.RNGStatesForAgents[index] = rngState;
}

__global__
void updateNutrientAbundanceD() {
	uint index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index >= params.numCells)
		return;
	float2 abundance = dev.nutrientAbundance[index];
	if (abundance.x <= 0) {
		abundance.y--;
		//the probabilty of the cell regenerating increases to 1 
		//as time with zero abundance approaches regeneration time
		//this leads to an equal probabilty of respawning at any timepoint between 0 and regeneration time.
		uint4 rngState = dev.RNGStatesForCells[index];
		if (1.0f/(float)abundance.y > randomNumber(rngState)) {
			abundance = params.NutrientAbundance;
		}
		dev.RNGStatesForCells[index] = rngState;

	}
	uint startIndex = dev.cellStart[index];
	if (startIndex != 0xffffffff) {
		uint endIndex = dev.cellEnd[index];
		for (uint currentIndex = startIndex; currentIndex < endIndex;
				++currentIndex) {
			abundance.x -= dev.eating[currentIndex];
		}
	}
	dev.nutrientAbundance[index] = abundance;
}

__global__ void calcAvailableNutrients()
{
	uint index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index >= params.numCells)
		return;
	float2 availableNutrients = make_float2(0.0, 0.0);
	if (dev.nutrientAbundance[index].x > 0)
		availableNutrients += dev.nutrientMap[index];
	((float2*)dev.scratchPadSpace)[index] = availableNutrients;
}
