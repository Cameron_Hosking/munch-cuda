#ifndef HELPER_H
#define HELPER_H
#include <iostream>
#include <string>
#include <sstream>
#include <cuda_runtime.h>
#include "nvidiaHelperFiles\helper_cuda.h"
#include "nvidiaHelperFiles\helper_math.h"
#include <stdio.h>
#include <memory>
#include <vector>

//for getting path of executable
#include <Shlwapi.h>

#ifdef _DEBUG	
#define checkKernelExecution           check ( (cudaDeviceSynchronize()), "cudaDeviceSynchronize()", __FILE__, __LINE__ );
#else
#define checkKernelExecution 
#endif
typedef unsigned int uint;

/**
/gaussian2D stores the parameters of a 2 dimensional gaussian curve.
/it has setters and getters for the variance factors since these are not stored as these values
/to optimise the speed of the evaluation function
**/
struct gaussian2D
{
	float max, x0, y0, xdenom, ydenom;

	void setXstddv(float xStddv) { xdenom = 1.0f / (2 * xStddv*xStddv); }
	void setYstddv(float yStddv) { ydenom = 1.0f / (2 * yStddv*yStddv); }
	float xStddv()const { return sqrt(1.0 / (2.0 * xdenom)); }
	float yStddv()const { return sqrt(1.0 / (2.0 * ydenom)); }
	bool operator==(const gaussian2D &g) const{ return max==g.max && x0==g.x0 && y0==g.y0 && xdenom==g.xdenom && ydenom==g.ydenom; }
	__device__ inline float eval(float x, float y)
	{
		return max*expf(-(xdenom*(x - x0)*(x - x0) + ydenom*(y - y0)*(y - y0)));
	}
	__device__ inline float eval(float2 f){	return eval(f.x,f.y);}

	__device__ inline float evalNormLog(float2 f)
	{
		return -(xdenom*(f.x - x0)*(f.x - x0) + ydenom*(f.y - y0)*(f.y - y0));
	}
};

std::ostream & operator<<(std::ostream &o, const gaussian2D &g);


inline bool operator==(const float2 &a, const float2 &b)
{
	return a.x == b.x && a.y == b.y;
}

inline float wrap(float toWrap, float wrapTo)
{
	return toWrap - wrapTo * floor(toWrap / wrapTo);
}
std::string trim(std::string s);
void viewDeviceArray(void * deviceArray,size_t size);

std::string getThisPath();
void resetStringstream(std::stringstream & ss);
bool outPutToFile(std::string filename, void * data, size_t length);
bool outPutToFileDeviceData(std::string filename, void * device_data, size_t length);
bool inputFromFile(std::string filename, void * data, size_t length);
bool inputFromFileDeviceData(std::string filename, void * device_data, size_t length);
void checkCUDAError(const char *msg);
char* getLocOfCmdLineArg(int argc, char **argv, const char * command);
bool getCmdLineArgument(int argc, char **argv, const char * command);
bool setToBoolIfFlagFound(int argc, char **argv, const char * command, bool setTo, bool &value);
bool getCmdLineArgument(int argc, char **argv, const char * command, char * value);
bool getCmdLineArgument(int argc, char **argv, const char * command, std::string &value);
bool getCmdLineArgument(int argc, char **argv, const char * command, float2 &value);
bool getCmdLineArgument(int argc, char **argv, const char * command, gaussian2D &value);

//simple getCmdLineArgument for types supported by iostream
template <typename T>
bool getCmdLineArgument(int argc, char **argv, const char * command, T &value)
{
	char* arg = getLocOfCmdLineArg(argc, argv, command);
	if (arg == nullptr)
		return false;
	else
	{
		std::stringstream ss;
		ss << arg;
		ss >> value;
		return true;
	}
}

std::vector<std::string> split(const std::string & stringToSplit,const std::string &delimiter);
std::vector<std::pair<std::string, std::string>> stringToListOfKeyValuePairs(const std::string &stringToParse,const std::string &keyValueDelimiter,const std::string &elementDelimiter);
//this is a direct copy of cudamemcpy except it can be called from a cpp file and has error handling
void allocateArray(void ** devPtr, size_t size);
void freeArray(void *devPtr);
void setBytesToValue(void * devicePtr, uint8_t value, size_t bytes);
void ratioOfFloat2ArryOnDevice(float2 *devArray, float *devResult, size_t size);
void threadSync();
//it would obviously have been nicer to template this code
//but these function are often called from a cpp file which can't compile the cuda code contained
void setAllToValue(bool * deviceArray, uint n, bool value);
void setAllToValue(char * deviceArray, uint n, char value);
void setAllToValue(int8_t * deviceArray, uint n, int8_t value);
void setAllToValue(int16_t * deviceArray, uint n, int16_t value);
void setAllToValue(uint8_t * deviceArray, uint n, uint8_t value);
void setAllToValue(uint16_t * deviceArray, uint n, uint16_t value);
void setAllToValue(uint64_t * deviceArray, uint n, uint64_t value);
void setAllToValue(int * deviceArray, uint n, int value);
void setAllToValue(int2 * deviceArray, uint n, int2 value);
void setAllToValue(int3 * deviceArray, uint n, int3 value);
void setAllToValue(int4 * deviceArray, uint n, int4 value);
void setAllToValue(uint * deviceArray, uint n, uint value);
void setAllToValue(uint2 * deviceArray, uint n, uint2 value);
void setAllToValue(uint3 * deviceArray, uint n, uint3 value);
void setAllToValue(uint4 * deviceArray, uint n, uint4 value);
void setAllToValue(float * deviceArray, uint n, float value);
void setAllToValue(float2 * deviceArray, uint n, float2 value);
void setAllToValue(float3 * deviceArray, uint n, float3 value);
void setAllToValue(float4 * deviceArray, uint n, float4 value);

float getAverage(float * deviceArray, uint n);
float getAverage(uint * deviceArray, uint n);

float2 getAverage(float2 * deviceArray, uint n);

//returns newly allocated host memory with as a copy of the device array, The function caller takes ownershiop of this memory
void * getDeviceArray(void * deviceArray, size_t size);
void copyData(void* dst, void* src, size_t size);

template <typename T>
void copyElements(T* dst, T* src, size_t numberOfElements)
{
	copyData(dst, src, numberOfElements * sizeof(T));
}
void copyArrayFromDevice(void* host, const void* device, int vbo, size_t size);
void copyArrayToDevice(void* device, const void* host, int offset, size_t size);
void computeGridSize(uint n, uint blockSize, uint &numBlocks, uint &numThreads);

template<typename T>
void changeDeviceArraySize(T * &data, size_t oldNumberOfElements, size_t newNumberOfElements)
{
	T * newArray;
	size_t newSize = sizeof(T)*newNumberOfElements;
	size_t oldSize = sizeof(T)*oldNumberOfElements;
	allocateArray((void**)&newArray, newSize);
	if (newSize > oldSize)
	{
		copyData(newArray, data, oldSize);
		setBytesToValue(&newArray[oldNumberOfElements], 0, newSize - oldSize);
	}
	else
	{
		copyData(newArray, data, newSize);
	}
	freeArray(data);
	data = newArray;
}

void randomUniformInitialization(float * values, float min, float range, uint number, uint4 * RNGStates);
void randomNormalInitialization(float * values, float mean, float stdv, uint number, uint4 * RNGStates);

void cudaInit(int argc, char **argv);

std::unique_ptr<float[]> sortArray(float* arrayToSort, int size);
float findMedian(float* deviceArray, int size);


#endif //HELPER_Hz