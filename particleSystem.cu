
// This file contains C wrappers around the some of the CUDA API and the
// kernel functions so that they can be called from "particleSystem.cpp"
//modified for CUSPP5
#if defined DEBUGGING
#define DEBUG(A) { A; }
#else
#define DEBUG(A) { }
#endif

#if defined(__APPLE__) || defined(MACOSX)
#include <GLUT/glut.h>
#else
#include <GL/freeglut.h>
#endif

#include <cstdlib>
#include <cstdio>
#include <string.h>

#include <cuda_gl_interop.h>

#include "nvidiaHelperFiles\helper_cuda.h"
#include "nvidiaHelperFiles\helper_cuda_gl.h"

#include "nvidiaHelperFiles\helper_functions.h"
#include "thrust/execution_policy.h"
#include "thrust/device_ptr.h"
#include "thrust/for_each.h"
#include "thrust/iterator/zip_iterator.h"
#include "thrust/sort.h"
#include "thrust/find.h"
#include "thrust/fill.h"
#include "cub/cub.cuh"

#include "defaults.h"
#include "particles_kernel_impl.cuh"
#include "simulationParameters.cuh"
#include "deviceArrays.cuh"
#include "particleSystem.h"


#include "helper.cuh"


void ParticleSystem::integrateSystem() {

	uint numThreads, numBlocks;
	computeGridSize(numAgents(), 1024, numBlocks, numThreads); 

	integrate<<<numBlocks, numThreads>>>();

checkKernelExecution

}
void ParticleSystem::calcHash() {
	uint numThreads, numBlocks;

	computeGridSize(numAgents(), 1024, numBlocks, numThreads);

	// execute the kernel
	calcHashD << <numBlocks, numThreads >> >();


checkKernelExecution
}

void ParticleSystem::calcHashWithReproductionAndDeath()
{
	uint numThreads, numBlocks;

	computeGridSize(numAgents(), 1024, numBlocks, numThreads);
	calcHashWithReproductionAndDeathD << <numBlocks, numThreads >> >();
	checkKernelExecution

}

void ParticleSystem::reproduction()
{
	uint numThreads, numBlocks;

	computeGridSize(numAgents(), 1024, numBlocks, numThreads);
	reproductionD << <numBlocks, numThreads >>>();
}


	
uint ParticleSystem::calculateNumberOfAgents()
{
	uint numThreads, numBlocks;
	uint n = min(numAgents() * 2, maxNumAgents());
	if (n == 0)
		return 0;
	computeGridSize(n, 1024, numBlocks, numThreads);
	findFirstNoHash << <numBlocks, numThreads >> > (dev.gridParticleHash, n, (uint *)dev.scratchPadSpace);
	copyData(&n, dev.scratchPadSpace, sizeof(uint));
	return n;
}


void ParticleSystem::reorderDataAndFindCellStart()
{
	uint numThreads, numBlocks;
	computeGridSize(numAgents(), 1024, numBlocks, numThreads);

	uint smemSize = sizeof(uint) * (numThreads + 1);
	findCellStartD <<<numBlocks, numThreads, smemSize >>>();
	checkKernelExecution

	reorderDataD<<<numBlocks, numThreads>>>();
	checkKernelExecution
}

void ParticleSystem::collide() {

	// thread per particle
	uint numThreads, numBlocks;

	/*Optimizing for the GTX1080 with small simulation sizes (10 is the maximum number of concurrently running blocks with default Blocksize)
	 * each block should have a multiple of 32 threads
	 * computeD takes 46 registers
	 * keep total register use per SM under 62000
	 * The GTX780 has 5 SMs
	 */
	int blockSize = BLOCK_SIZE;
	if (numAgents() / 10 < blockSize) {
		blockSize = 32;
		while (numAgents() / (5 * 62000 / (blockSize * 46)) > blockSize)
			blockSize += 32;
	}
	numThreads = min(blockSize, numAgents());
	numBlocks = (numAgents() % numThreads != 0) ? (numAgents() / numThreads + 1) : (numAgents() / numThreads);

	// execute the kernel
	collideD<<<numBlocks, numThreads>>>();

checkKernelExecution

 }

void ParticleSystem::sortParticles() {
	cub::DoubleBuffer<uint> dIndexes(dev.gridParticleIndex, dev.gridParticleIndexAlt);
	cub::DoubleBuffer<uint> dHashes(dev.gridParticleHash, dev.gridParticleHashAlt);

	size_t tmpStorageRequired = 0;

	cub::DeviceRadixSort::SortPairs(NULL, tmpStorageRequired, dHashes, dIndexes, numAgents() * 2);
	increaseScratchPadSize(tmpStorageRequired);
	int bitsToCheck;
	bitsToCheck = (int)log2(numCells()) + 1;
	cub::DeviceRadixSort::SortPairs(dev.scratchPadSpace, tmpStorageRequired, dHashes, dIndexes, numAgents() * 2,0,bitsToCheck);

	//use of the double buffers in the sort minimises space required but randomises which array is in a valid state
	dev.gridParticleHash = dHashes.Current();
	dev.gridParticleHashAlt = dHashes.Alternate();
	dev.gridParticleIndex = dIndexes.Current();
	dev.gridParticleIndexAlt = dIndexes.Alternate();

	kernelImplementation::syncDeviceArrays(&dev);

checkKernelExecution
}

void ParticleSystem::eat() {
	uint numThreads, numBlocks;
	computeGridSize(numAgents(), 1024, numBlocks, numThreads);
	eatD<<<numBlocks, numThreads>>>();
checkKernelExecution
}
void ParticleSystem::forage() {

	uint numThreads, numBlocks;
	computeGridSize(numAgents(), 128, numBlocks, numThreads);

	forageD<<<numBlocks, numThreads>>>();
checkKernelExecution

	// check if kernel invocation generated an error
	getLastCudaError("Kernel integrate failed");
}

void ParticleSystem::reproductionLifeOrDeath()
{
	uint numThreads, numBlocks;
	computeGridSize(numAgents(), 1024, numBlocks, numThreads);

	//execute the kernel
	reproductionLifeOrDeathD << <numBlocks, numThreads >> >(
		m_statistics.m_dDeaths, m_statistics.m_dNutritionalStatesByAge, 
		m_statistics.m_dNutritionalStatesAtMaturity, m_statistics.m_dAggregateNutrientsAtDeath, 
		m_statistics.m_dLifetimeReproductiveOutput,m_statistics.m_dBirthsAtAge, m_statistics.m_dIntakeTargets);
	checkKernelExecution
}


void ParticleSystem::cull() {
	uint numThreads, numBlocks;
	computeGridSize(maxNumAgents(), 1024, numBlocks, numThreads);
	cullD<<<numBlocks, numThreads>>>();
checkKernelExecution
}

void ParticleSystem::updateNutrientAbundance() {
	uint numThreads, numBlocks;
	numThreads = 1024;
	numBlocks = numCells() / 1024 + 1;
	updateNutrientAbundanceD<<<numBlocks, numThreads>>>();
checkKernelExecution
}

float2 ParticleSystem::calcAverageNutrientAbundance(){
	size_t tmpStorageRequired = 0;
	cub::DeviceReduce::Sum(NULL, tmpStorageRequired, (float2*)dev.scratchPadSpace, (float2*)NULL, numCells());
	
	//first numCells() of scratchPadSpace contain the values to reduce, next element contains the return value, 
	//last tmpStorageRequired is used by reduce algorithm.
	size_t requiredSpace = tmpStorageRequired + (1 + numCells()) * sizeof(float2);
	increaseScratchPadSize(requiredSpace);

	uint numThreads, numBlocks;
	numThreads = 1024;
	numBlocks = numCells() / 1024 + 1;
	calcAvailableNutrients<<<numBlocks, numThreads>>>();
	checkKernelExecution

	cub::DeviceReduce::Sum((float2*)dev.scratchPadSpace + numCells() + 1, tmpStorageRequired, (float2*)dev.scratchPadSpace, (float2*)dev.scratchPadSpace + numCells(), numCells());
	float2 averageAvailableNutrients;
	copyData(&averageAvailableNutrients, (float2*)dev.scratchPadSpace + numCells(), sizeof(float2));

	return averageAvailableNutrients/numCells();
}

void ParticleSystem::increaseScratchPadSize(size_t newSize)
{
	if (dev.scratchPadSize < newSize)
	{
		freeArray(dev.scratchPadSpace);
		dev.scratchPadSize = 2*newSize;
		allocateArray((void**)&dev.scratchPadSpace, dev.scratchPadSize);
		kernelImplementation::syncDeviceArrays(&dev);
	}
}
