
//modified for CUSPP 5
#ifndef __PARTICLESYSTEM_H__
#define __PARTICLESYSTEM_H__

#define DEBUG_GRID 0
#define DO_TIMING 0

#include "nvidiaHelperFiles\helper_functions.h"
#include "simulationParameters.cuh"
#include "DeviceArrays.cuh"
#include "vector_functions.h"
#include "Environment.h"
#include "Statistics.h"
#include <memory>

// Particle system class
class ParticleSystem {
public:
	ParticleSystem(std::string filename);
	ParticleSystem(const SimParams &params);
	~ParticleSystem();


	enum ParticleConfig {
		CONFIG_RANDOM, CONFIG_GRID, _NUM_CONFIGS
	};

	enum ParticleArray {
		POSITION, VELOCITY,
	};
	void loadEnvironment(std::string filepath);
	void loadPopulations(std::vector<std::pair<std::string, std::string> > populations);
	void saveState(std::string fileName);
	bool loadState(std::string fileName);
	void initialize();
	void update(float deltaTime);
	void reset();

	void setArray(const void *hdata, void *ddata, int start,
			std::size_t typeSize, int count);
	void increaseScratchPadSize(size_t newSize);
	void dumpSpecialization(const std::string filename);
	void dumpNutritionalStatesHist(const std::string filename);
	void dumpDataHist(float * data, size_t n, float binSize, const std::string filename);
	void dumpData(uint * data, size_t n, const std::string filename);
	void resetStatistics();
	void outputAllStatistics();
	const SimParams & getParams() { return hostParams; }

	std::vector<uint16_t> countNumberOfTags();

	Statistics & getStatistics()
	{
		return m_statistics;
	}

	inline
		void randomizeEnvironment() {
		environment->generateFractalFoodMap(rng);
		reset();
	}
	inline
	void saveEnvironment() {
		environment->saveEnvironment("nutrientMap.png");
	}
	inline
	void loadEnvironment() {
		environment->loadEnvironment("nutrientMap.png");
		reset();
	}
	int numAgents() { return hostParams.numBodies; }
	int maxNumAgents() { return hostParams.maxNumBodies; }
	int numCells() { return hostParams.numCells; }

	DeviceArrays dev;

protected:
	// methods
	//uint createVBO(uint size);

	void initializeSpatialDistributionAndVelocity();
	void initializeIntakeTargets();
	void finalize();
	void copySorted();
	void initializeAgentRNGstates();
	void initializeCellRNGstates();
	void allocateMemory();
	void initializeSimulation();
	void initializeAgents();
	void setEnvironment();
protected:
	// data
	bool m_bInitialized;
	fastPRNG::PRNG rng;
	SimParams originalParams;
	SimParams hostParams;
	// CPU data
	Environment* environment;
	Statistics m_statistics;

private:

	void integrateSystem();

	void calcHash();
	void reproductionLifeOrDeath();
	void calcHashWithReproductionAndDeath();

	void reproduction();

	uint calculateNumberOfAgents();

	void reorderDataAndFindCellStart();

	void collide();

	void sortParticles();

	void eat();

	void forage();

	void cull();

	void updateNutrientAbundance();

	float2 calcAverageNutrientAbundance();

	bool successfullyLoaded;

};

#endif // __PARTICLESYSTEM_H__
