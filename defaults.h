/*
 * defaults.h
 *
 *  Created on: 30/08/2016
 *      Author: cameron
 */

#ifndef DEFAULTS_H_
#define DEFAULTS_H_
#define NO_HASH 0xFFFFFFFF
#define NO_SEED 0xFFFFFFFF
#define BLOCK_SIZE		96
#define BLOCK_SIZE_APPROXIMATING		640
#define SAVES_DIR "/saveData"
#define OUTPUT_DIR "/output"
#define DEFAULT_EXPERIMENT_NAME "experiment"
#define DEFAULT_CONFIG_FILE "config.ini"
#define DEFAULT_NUM_AGENTS 131072 
#define DEFAULT_GRID_DIMENSION 256 //the x and y dimensions of the grid (currently only squares are supported)
								   //if there is social behavior the size of each cell is the largest radius of that behaviour
#define DEFAULT_CELL_SIZE 0.25f
#define DEFAULT_NUM_ITERATIONS 0 //this is the default number of iterations after which the simulation will end.
								 //if the number is 0 then it does not end.
#define DENSITY
#define DEFAULT_ACCURACY 512
#define DEFAULT_SOCIAL_EXPERIMENT false
#define DEFAULT_REPRODUCTION_EXPERIMENT false
#define DEFAULT_LONGEVITY_LANDCAPE {10000.f,0.f,1.f,1.f,1.f}
#define DEFAULT_FECUNDITY_LANDCAPE {0.01f,1.f,0.f,1.f,1.f}
#define DEFAULT_INTAKE_TARGET_STDDV 0.01f
#define DEFAULT_ATTRACTION_RADIUS 0.25f
#define DEFAULT_ALIGNMENT_RADIUS 0.125f
#define DEFAULT_REPULSION_RADIUS 0.03125f
#define DEFAULT_ATTRACTION_STRENGTH 0.1f
#define DEFAULT_ALIGNMENT_STRENGTH 0.5f
#define DEFAULT_REPULSION_STRENGTH 1.5f
#define DEFAULT_OCCLUSION_NUMBER 100 //number of agents within allignment range at which point attraction is ignored
#define DEFAULT_FOOD_EXPERIMENT false
#define DEFAULT_RECORD_AGENT_NUTRITION 1000 //iterations between recording a summary
#define DEFAULT_FOOD_QUANTITY 1000.0f //nutrients per square
#define DEFAULT_FOOD_REGENERATION_TIME 1000.0f //time between nutrient regeneration
#define DEFAULT_BITE_SIZE 0.01f //amount of food eaten per iteration
#define DEFAULT_METABOLIC_COST_C 0.0001f //decrease in carbohydrates per iteration
#define DEFAULT_METABOLIC_COST_P 0.0001f //decrease in protein per iteration
#define DEFAULT_EATING_SPECIALIZATION -1 // slope of rule of compromise as a simple line passing through NT.
#define DEFAULT_CULL_PROPORTION 0.9f
#define DEFAULT_SPEED 0.0025f //movement per tick
#define DEFAULT_FAST_SPEED 0.000f //additional movement per tick when moving quickly
#define DEFAULT_JUMP_DISTANCE 0.1f 
#define DEFAULT_JUMP_PROBABILITY 0.01f //chance of jumping per tick
#define DEFAULT_JUMP_PROBABILITY_DENSE 0.1f //chance of jumping per tick when in there are other agents in repulsion zone
#define DEFAULT_INERTIA 0.6f
#define DEFAULT_NOISE_INTESITY 0.05f
#define DEFAULT_ACTIVE_PERIOD 8100
#define DEFAULT_AGE_OF_MATURITY 4000
#define DEFAULT_RESUME_PROBABILITY 0.0011f //base chance of resuming movement
#define DEFAULT_RESUME_PROBABILITY_ALONE 0.11f //additional chance when alone
#define DEFAULT_RESUME_PROBABILITY_DENSE 0.5f //additional chance when within repulsion range of others
#define DEFAULT_PREDATION_PROBABILITY 0.0f
#define DEFAULT_NEXT_AUTOSAVE 64000
#define DEFAULT_RESET_STATISTICS_ON_AUTOSAVE false
#define DEFAULT_OUTPUT_STATISTICS_ON_AUTOSAVE false
#define NO_INITIAL_INTAKE_TARGET make_float2(-1.0f,-1.0f)




enum cellType{REPULSION,ALIGNMENT,ATTRACTION};

#endif /* DEFAULTS_H_ */
