//modified for CUDA 8
#ifndef DEVICE_ARRAYS_H
#define DEVICE_ARRAYS_H

#include "vector_types.h"
#include <vector>
#include "helper.cuh"
#include "simulationParameters.cuh"


typedef unsigned int uint;

struct DeviceArrays
{
	/**
	requires params.numBodies, params.maxNumBodies, and params.numCells to be set
	**/
	void allocateMemory(SimParams & params);
	void freeMemory();

	float4 *posVel;
	uint16_t *crowding;
	float *timer;
	float * ageAtMaturity;
	uint *density;
	float2 *agentPC;
	bool * eating;
	float * scratchPadSpace;
	size_t scratchPadSize;
	float2* intakeTargets;
	float3 * socialRanges;
	float3 * socialStrengths;
	float2 * aggregateNutrition;
	uint8_t * lifetimeReproductiveOutput;
	uint16_t * populationTag;

	float4 *oldPosVel;
	uint16_t *crowdingAlt;
	float *timerAlt;
	float * ageAtMaturityAlt;
	uint *densityAlt;
	float2 *agentPCAlt;
	bool * eatingAlt;
	float2* intakeTargetsAlt;
	float3 * socialRangesAlt;
	float3 * socialStrengthsAlt;
	float2 * aggregateNutritionAlt;
	uint8_t * lifetimeReproductiveOutputAlt;
	uint16_t * populationTagAlt;

	float2 *nutrientMap;
	float2 *nutrientAbundance;

	uint4 * RNGStatesForAgents;
	uint4 * RNGStatesForCells;

	//displayData
	uchar4 * agentColours;

	// grid data for sorting method Attraction
	uint * gridParticleHash; // grid hash value for each particle
	uint * gridParticleHashAlt; // alternate storage used for sort

	uint * gridParticleIndex; // particle index for each particle
	uint * gridParticleIndexAlt;  // alternate storage used for sort

	char * reproductionStates; // whether an agent will die, do nothing, or reproduce {0,1,2} this number represents how many agents an agent will carry forward to the next iteration
	uint * cellStart;        // index of start of each cell in sorted list
	uint * cellEnd;          // index of end of cell

	void resizeEnvironment(uint newSize);
	
};


#endif //DEVICE_ARRAYS_H
