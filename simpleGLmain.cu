// simpleGLmain.cpp (Rob Farber)

#include "simpleGLmain.cuh"
#include "simulationParameters.cuh"
#include "nvidiaHelperFiles\helper_math.h"
#include <sstream>

enum class ViewType
{
	LOCATION_PHYSICAL, LOCATION_NUTRITIONAL_STATE, LOCATION_INTAKE_TARGET, COLOUR_DENSITY, COLOUR_NUTRITIONAL_STATE, COLOUR_INTAKE_TARGET, COLOUR_WHITE, FITNESS_LANDSCAPE,
	FECUNDITY_LANDSCAPE, LONGEVITY_LANDSCAPE, TRADEOFF_LANDSCAPE
};
#define DUMP_AGES_MENU_ITEM 251245
#define OUTPUT_DEATHRATE_MENU_ITEM 3129541
#define RESET_STATISTICS_MENU_ITEM 1258460
#define SAVE_STATE_MENU_ITEM 87350
#define OUTPUT_INTAKE_TARGETS_MENU_ITEM 12736491
#define OUTPUT_NUTRITIONAL_STATES_MENU_ITEM 85848
#define OUTPUT_NUTRITIONAL_STATES_BY_AGE_MENU_ITEM 4596510
#define OUTPUT_NUTRITIONAL_STATES_SINCE_RESET_MENU_ITEM 5987205
#define OUTPUT_MATURITY_AGES_MENU_ITEM 34589845
#define OUTPUT_AGES_SINCE_RESET_MENU_ITEM 9876349
#define OUTPUT_ALL_STATISTICS 345898145


int displayLocation;
int displayColour;
unsigned int window_width = 1024; //GRID_SIZE;
unsigned int window_height = 1024; //GRID_SIZE;

unsigned int iterationsRemaining;
bool timeOut = true;

int animFlag = 1;
float animTime = 0.0f;
float animInc = 0.1f;
bool paused = false;
int zoomLevel = 1;
int2 lastMovement;
int2 mouseDragVector;
float left = 0;
float top = 0;
int iterationsPerFrame = 32;
bool panning = false;
bool displayNutrientMap = true;
bool displayAgentNutrition = false;
bool displayFitnessLandscape = false;
bool showGrid = false;
float foodTransparency = 0;
float agentTransparency = 0;
ViewType location = ViewType::LOCATION_PHYSICAL;
ViewType colour = ViewType::COLOUR_DENSITY;


GLuint pbo = (GLuint) NULL;
GLuint textureID = (GLuint) NULL;
struct cudaGraphicsResource *cuda_pbo_resource;

StopWatchInterface* timer = 0;

ParticleSystem* psystem;

//this whole annoying duplication and consistency checking shenanigan is because currently
//device constants are local to each module and therefore I need a seperate constant for each .cu file =(
//if this changes in a future CUDA release (currently CUDA 8.0rc) this workaround can be refactored away
__constant__ GPUSimParams params;
GPUSimParams paramsSnapshotA;

__device__ uchar4 rgbaFromVal(float minimum, float maximum, float val)
{
	uint8_t r = 0;
	uint8_t g = 0;
	uint8_t b = 0;
	//val is now between 0 and max
	maximum -= minimum;
	val -= minimum;
	float center = maximum / 2.0f;
	float midpoint = center / 2.0f;

	//green is 255 at the center and decreases to 0 at a distance center/2 from the center
	g = __max(0, 255 * (1 - 2 * (fabsf(val - center) / center)));

	if (val < center)
	{
		//blue is 255 at the midpoint and decreases to 0 at minimum and at center
		b = __max(0, 255 * (1 - (fabsf(val - midpoint) / midpoint)));
	}
	else
	{
		val -= center;
		//red is 255 at the midpoint and decreases to 0 at maximum and at center
		r = __max(0, 255 * (1 - (fabsf(val - midpoint) / midpoint)));
	}

	return make_uchar4(r, g, b, 0);
}

__global__ void setAgentColours(uchar4* agentColours, ViewType colouring ,uint* densities, float2* nutritionalStates, float2* intakeTargets)
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index >= params.numBodies)
		return;
	unsigned char r, g, b;
	if (colouring == ViewType::COLOUR_WHITE)
	{
		r = 255;
		g = 255;
		b = 255;
	}
	else if (colouring == ViewType::COLOUR_DENSITY)
	{
		float density = (float)densities[index];
		b = (unsigned char)(255 * (float)(1 - density / 50));
		r = (unsigned char)(255	* (float)(__log10f(density) / __log10f(50) - 1));
		g = 255 - b - r;
	}
	else if (colouring == ViewType::COLOUR_NUTRITIONAL_STATE)
	{
		r = 255 * (nutritionalStates[index].x + 1) / 3;
		g = 255 * (nutritionalStates[index].y + 1) / 3;
		b = 255 * 1 / 3;
	}
	else if (colouring == ViewType::COLOUR_INTAKE_TARGET)
	{
		r = 255 * (intakeTargets[index].x + 1) / 3;
		g = 255 * (intakeTargets[index].y + 1) / 3;
		b = 255 * 1 / 3;
	}
	agentColours[index] = make_uchar4(r, g, b, 0);
}

void kernelGL::syncHostAndDeviceSimParams(GPUSimParams hostParams)
{
	if (!equal(hostParams, paramsSnapshotA))
	{
		cudaDeviceSynchronize();
		// copy parameters to constant memory
		checkCudaErrors(cudaMemcpyToSymbol(params, &hostParams, sizeof(GPUSimParams)));
		paramsSnapshotA = hostParams;
	}
}

__inline__ __device__ uint getCellIndex(float x, float y)
{
	int gridX = (int)x % params.gridSize.x;
	int gridY = (int)y % params.gridSize.y;

	return gridY * params.gridSize.x + gridX;
}

__global__ void clearTextureKernel(uchar4* pos, unsigned int width,
		unsigned int height) {
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index < width * height) {
		pos[index].w = 0;
		pos[index].x = 0;
		pos[index].y = 0;
		pos[index].z = 0;
	}
}
__global__ void showNutritionMap(uchar4* pos, unsigned int width,
		unsigned int height, unsigned int windowWidth, unsigned int windowHeight,
		float2* nutritionMap, float2* nutrientAbundance, bool displayNutrientMap,
		float foodTransparency) {
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int x = index % windowWidth;
	int y = index / windowWidth;
	int effectiveIndex = x + y * width;
	if (x < width && y < height) {
		if (effectiveIndex < windowWidth * windowHeight) {
			// Each thread writes one pixel location in the texture (textel)
			pos[index].w = 255 * foodTransparency;
			pos[index].x = displayNutrientMap * 255
					* nutritionMap[effectiveIndex].x
					* (nutrientAbundance[effectiveIndex].x > 0)
					* (1 - foodTransparency);
			pos[index].y = displayNutrientMap * 255
					* nutritionMap[effectiveIndex].y
					* (nutrientAbundance[effectiveIndex].x > 0)
					* (1 - foodTransparency);
			pos[index].z = 0;
		}
	}
}

__inline__ __device__ float2 indexToFloat2(int index, unsigned int windowWidth, unsigned int windowHeight, float xScaling, float yScaling)
{
	int x = index % windowWidth;
	int y = index / windowWidth;
	return make_float2(xScaling*(float)x / (float)windowWidth, yScaling*(float)(windowHeight - y) / (float)windowHeight);
}

__global__ void showNutritionalStates(uchar4* outputTexture, float2* agentPCValues, uchar4 * agentColours, unsigned int windowWidth, unsigned int windowHeight)
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index >= params.numBodies)
		return;
	float2 PC = agentPCValues[index];
	int x = windowWidth * (PC.x / 2.0);
	int y = windowHeight * (PC.y / 2.0);

	//if the point is within bounds to be displayed on the window
	if (x >= 0 && x < windowWidth && y >= 0 && y < windowHeight)
	{
		uint outputPixel = x + (windowHeight-y-1)*windowWidth;
		outputTexture[outputPixel] = agentColours[index];
	}
}

__global__ void showLandscape(ViewType landscape, uchar4* outputTexture, unsigned int windowWidth, unsigned int windowHeight)
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index >= windowWidth*windowHeight)
		return;
	float2 PC = indexToFloat2(index,windowWidth,windowHeight,2.0,2.0);
	
	float value, max, min;
	if (landscape == ViewType::FECUNDITY_LANDSCAPE)
	{
		value = params.fecundityLandscape.eval(PC);
		max = params.fecundityLandscape.max;
		min = 0;
	}
	if (landscape == ViewType::LONGEVITY_LANDSCAPE)
	{
		value = params.longevityLandscape.eval(PC);
		max = params.longevityLandscape.max;
		min = 0;
	}
	if (landscape == ViewType::TRADEOFF_LANDSCAPE)
	{
		min = -4;
		max = 5;
		value = params.fecundityLandscape.evalNormLog(PC) - params.longevityLandscape.evalNormLog(PC);
	}
	
	uchar4 outputColour = rgbaFromVal(min, max, value);

	//contours
	if (fabs(fmodf(value, 0.1f*(max-min))) < 0.002f*(max - min))
	{
		outputColour = make_uchar4(0, 0, 0, 0);
	}

	outputTexture[index] = outputColour;
}

__global__ void outputGridlines(uchar4* outputTexture, unsigned int windowWidth, unsigned int windowHeight)
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index >= windowWidth*windowHeight)
		return;
	float2 PC = indexToFloat2(index, windowWidth, windowHeight, 2.0, 2.0);
	
	if (abs(fmodf(PC.x, 0.5)) < 0.003 || abs(fmodf(PC.y, 0.5)) < 0.003)
	{
		outputTexture[index] = make_uchar4(192, 192, 192, 0);
	}
	else if (abs(fmodf(PC.x, 0.1)) < 0.0018 || abs(fmodf(PC.y, 0.1)) < 0.0018)
	{
		outputTexture[index] = make_uchar4(128, 128, 128, 0);

	}
}

__global__ void showAverageLifetimeFitness(uchar4* outputTexture, unsigned int windowWidth, unsigned int windowHeight)
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index >= windowWidth*windowHeight)
		return;
	float2 PC = indexToFloat2(index, windowWidth, windowHeight, 2.0, 2.0);

	
	float meanLifetime = params.longevityLandscape.eval(PC);
	//add in predation
	float probabilityOfDeath = 1 / meanLifetime;
	probabilityOfDeath = 1 - (1 - probabilityOfDeath)*(1 - params.predationProbability);
	meanLifetime = 1 / probabilityOfDeath;
	
	float probabilityOfReproducing = params.fecundityLandscape.eval(PC);

	float expectedNumberOfOffspring = probabilityOfReproducing * meanLifetime;
	uchar4 outputColour = rgbaFromVal(0, 20, expectedNumberOfOffspring);
	
	//contour lines of fitness landscape
	if (fmodf(expectedNumberOfOffspring, 1.0f) < 0.02)
	{
		outputColour = make_uchar4(0, 0, 0, 0);
	}

	outputTexture[index] = outputColour;
}

__global__ void zoomedKernel(uint* cellStarts, uint* cellEnds,
	uchar4* outputTexture, float4 *posvel, uchar4* agentColours,
	unsigned int windowWidth, unsigned int windowHeight, float startX,
	float startY, int zoomLevel, float agentTransparency) {
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int x = index % params.gridSize.x;
	int y = index / params.gridSize.x;

	//check if the cell is inside the viewing area
	if (x > (int)startX + windowWidth / zoomLevel)
		return;
	if (x < (int)startX && x + params.gridSize.x > (int)startX + windowWidth / zoomLevel)
	{
		return;
	}

	if (y > (int)startY + windowHeight / zoomLevel)
		return;
	if (y < (int)startY && y + params.gridSize.y >(int)startY + windowHeight / zoomLevel)
	{
		return;
	}

	int currentIndex = cellStarts[index];
	int endIndex = cellEnds[index];
	if (currentIndex != 0xffffffff) {
		while (currentIndex != endIndex) {

			uchar4 colour = agentColours[currentIndex];
			float4 pos = posvel[currentIndex];
			int2 location;
			location.x = (int) (zoomLevel
					* (pos.x / params.cellSize - startX));
			location.y = (int) (zoomLevel
					* (pos.y / params.cellSize - startY));
			if (location.x < 0)
				location.x += params.gridSize.x*zoomLevel;
			if (location.y < 0)
				location.y += params.gridSize.y*zoomLevel;
			currentIndex++;
			if (location.x >= windowWidth || location.y >= windowHeight)
				continue;
			int zoomedIndex = windowWidth * location.y + location.x;

			outputTexture[zoomedIndex].x = outputTexture[zoomedIndex].x * agentTransparency + colour.x*(1 - agentTransparency);
			outputTexture[zoomedIndex].y = outputTexture[zoomedIndex].y * agentTransparency + colour.y*(1 - agentTransparency);
			outputTexture[zoomedIndex].z = outputTexture[zoomedIndex].z * agentTransparency + colour.z*(1 - agentTransparency);
			outputTexture[zoomedIndex].w = 255 - ((255 - outputTexture[zoomedIndex].w) * agentTransparency);

		}
	}
}

__global__ void showNutritionMapZoomed(float2* nutritionMap,
		float2* nutrientAbundance, uchar4* outputTexture, unsigned int width,
		unsigned int height, unsigned int windowWidth, unsigned int windowHeight,
		float startX, float startY, int zoomLevel, bool displayNutrientMap,
		float foodTransparency) {
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int x = index % windowWidth;
	int y = index / windowHeight;
	if (x / zoomLevel > params.gridSize.x || y / zoomLevel > params.gridSize.y)
		return;
	uint effectiveIndex = getCellIndex((float)x / (float)zoomLevel + startX, (float)y / (float)zoomLevel + startY);
	if (effectiveIndex < windowWidth * windowHeight) {
		// Each thread writes one pixel location in the texture (textel)
		outputTexture[index].w = 0; //*foodTransparency;
		if (displayNutrientMap && nutrientAbundance[effectiveIndex].x > 0)
		{
			outputTexture[index].x = 255 * nutritionMap[effectiveIndex].x * (1 - foodTransparency);
			outputTexture[index].y = 255 * nutritionMap[effectiveIndex].y * (1 - foodTransparency);
		}
		else
		{
			outputTexture[index].x = 0;
			outputTexture[index].y = 0;
		}
		outputTexture[index].z = 0;
	}
}
// Wrapper for the __global__ call that sets up the kernel call
void launch_kernel(uchar4* pos)
{
	// execute the kernel
	uint nThreads, nBlocks;
	computeGridSize(window_height * window_width, 512, nBlocks, nThreads);
	kernelGL::syncHostAndDeviceSimParams(psystem->getParams());
	clearTextureKernel << <nBlocks, nThreads >> > (pos, window_width, window_height);
	computeGridSize(psystem->getParams().numBodies, 512, nBlocks, nThreads);
	setAgentColours <<< nBlocks, nThreads >> > (psystem->dev.agentColours, colour, psystem->dev.density, psystem->dev.agentPC, psystem->dev.intakeTargets);
	checkKernelExecution
	switch (location)
	{
	case ViewType::LOCATION_PHYSICAL:
		computeGridSize(window_height * window_width, 512, nBlocks, nThreads);
		showNutritionMapZoomed << <nBlocks, nThreads >> > (psystem->dev.nutrientMap,
			psystem->dev.nutrientAbundance, pos, psystem->getParams().gridSize.x,
			psystem->getParams().gridSize.y, window_width, window_height,
			left, top, zoomLevel, displayNutrientMap,
			foodTransparency);
		checkKernelExecution
			
		computeGridSize(psystem->getParams().gridSize.x*psystem->getParams().gridSize.y, 128, nBlocks, nThreads);
		zoomedKernel << <nBlocks, nThreads >> > (psystem->dev.cellStart,
			psystem->dev.cellEnd, pos, psystem->dev.posVel,
			psystem->dev.agentColours, window_width, window_height,
			left, top, zoomLevel, agentTransparency);
		checkKernelExecution
		break;
	
	case ViewType::LOCATION_NUTRITIONAL_STATE:
		computeGridSize(psystem->getParams().numBodies, 512, nBlocks, nThreads);
		showNutritionalStates << <nBlocks, nThreads >> > (pos, psystem->dev.agentPC, psystem->dev.agentColours, window_width, window_height);
		checkKernelExecution
		break;
	
	case ViewType::LOCATION_INTAKE_TARGET:
		computeGridSize(psystem->getParams().numBodies, 512, nBlocks, nThreads);
		showNutritionalStates << <nBlocks, nThreads >> > (pos, psystem->dev.intakeTargets, psystem->dev.agentColours, window_width, window_height);
		checkKernelExecution
		break;

	case ViewType::FITNESS_LANDSCAPE:
		computeGridSize(window_height * window_width, 512, nBlocks, nThreads);
		showAverageLifetimeFitness << <nBlocks, nThreads >> > (pos, window_width, window_height);
		checkKernelExecution
		break;
	case ViewType::LONGEVITY_LANDSCAPE:
	case ViewType::FECUNDITY_LANDSCAPE:
	case ViewType::TRADEOFF_LANDSCAPE:
		computeGridSize(window_height * window_width, 512, nBlocks, nThreads);
		showLandscape << <nBlocks, nThreads >> > (location,pos, window_width, window_height);
		break;
	}
	if (location != ViewType::LOCATION_PHYSICAL&&showGrid)
	{
		computeGridSize(window_height * window_width, 512, nBlocks, nThreads);
		outputGridlines << <nBlocks, nThreads >> > (pos, window_width, window_height);
	}
}
	
//end kernelPBO.cu

// callbackPBO.cpp

void display() {
	if (!iterationsRemaining&&timeOut)
		exit(0);
	// run CUDA kernel
	runCuda();
	// Create a texture from the buffer
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, pbo);

	// bind texture from PBO
	glBindTexture(GL_TEXTURE_2D, textureID);

	// Note: glTexSubImage2D will perform a format conversion if the
	// buffer is a different format from the texture. We created the
	// texture with format GL_RGBA8. In glTexSubImage2D we specified
	// GL_BGRA and GL_UNSIGNED_INT. This is a fast-path combination

	// Note: NULL indicates the data resides in device memory
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, window_width, window_height, GL_RGBA,
			GL_UNSIGNED_BYTE, NULL);

	// Draw a single Quad with texture coordinates for each vertex.

	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 0.0f);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, 0.0f, 0.0f);
	glEnd();

	// Don't forget to swap the buffers!
	glutSwapBuffers();

	// if animFlag is true, then indicate the display needs to be redrawn
	if (animFlag) {
		glutPostRedisplay();
		animTime += animInc;
	}
}
void menu(int num)
{
	std::string outputFile = std::string(psystem->getParams().experimentName) + OUTPUT_DIR + "/" + std::to_string(psystem->getParams().currentTime) + "_";
	if (num == OUTPUT_ALL_STATISTICS)
	{
		psystem->outputAllStatistics();
	}
	else if (num == DUMP_AGES_MENU_ITEM)
	{
		psystem->dumpDataHist(psystem->dev.timer, psystem->getParams().numBodies, 100.0f, outputFile + "ages");
	}
	else if (num == OUTPUT_DEATHRATE_MENU_ITEM)
	{
		psystem->getStatistics().outputDeathRates(outputFile+ "death_rates");
	}
	else if (num == RESET_STATISTICS_MENU_ITEM)
	{
		psystem->resetStatistics();
	}
	else if (num == SAVE_STATE_MENU_ITEM)
	{
		std::stringstream ss;
		ss << psystem->getParams().experimentName << SAVES_DIR << "/" << psystem->getParams().currentTime;;
		psystem->saveState(ss.str());
	}
	else if (num == OUTPUT_INTAKE_TARGETS_MENU_ITEM)
	{
		psystem->dumpSpecialization(outputFile + "IT_hist");
	}
	else if (num == OUTPUT_NUTRITIONAL_STATES_MENU_ITEM)
	{
		psystem->dumpNutritionalStatesHist(outputFile + "NS_hist");
	}
	else if (num == OUTPUT_NUTRITIONAL_STATES_SINCE_RESET_MENU_ITEM)
	{
		psystem->getStatistics().outputNutritionalStates2Histograms(outputFile + "NS_hist_since_reset");
	}
	else if (num == OUTPUT_NUTRITIONAL_STATES_BY_AGE_MENU_ITEM)
	{
		psystem->getStatistics().outputNutritionalStatesByAge(outputFile + "NS_dist");
		psystem->getStatistics().outputNutritionalStatesAtMaturity(outputFile + "NS_dist_at_maturity");

	}
	else if (num == OUTPUT_MATURITY_AGES_MENU_ITEM)
	{
		psystem->dumpDataHist(psystem->dev.ageAtMaturity, psystem->getParams().numBodies, 10, outputFile + "maturity_ages");
	}
	else if (num == OUTPUT_AGES_SINCE_RESET_MENU_ITEM)
	{
		psystem->getStatistics().outputAgesEver(outputFile + "ages_since_reset");
	}

	switch ((ViewType)num)
	{
	case ViewType::LOCATION_PHYSICAL:
		location = ViewType::LOCATION_PHYSICAL;
		break;
	case ViewType::LOCATION_NUTRITIONAL_STATE:
		location = ViewType::LOCATION_NUTRITIONAL_STATE;
		break;
	case ViewType::LOCATION_INTAKE_TARGET:
		location = ViewType::LOCATION_INTAKE_TARGET;
		break;
	case ViewType::COLOUR_DENSITY:
		colour = ViewType::COLOUR_DENSITY;
		break;
	case ViewType::COLOUR_NUTRITIONAL_STATE:
		colour = ViewType::COLOUR_NUTRITIONAL_STATE;
		break;
	case ViewType::COLOUR_INTAKE_TARGET:
		colour = ViewType::COLOUR_INTAKE_TARGET;
		break;	
	case ViewType::COLOUR_WHITE:
		colour = ViewType::COLOUR_WHITE;
		break;
	case ViewType::FITNESS_LANDSCAPE:
		location = ViewType::FITNESS_LANDSCAPE;
		break;
	case ViewType::FECUNDITY_LANDSCAPE:
		location = ViewType::FECUNDITY_LANDSCAPE;
		break;
	case ViewType::LONGEVITY_LANDSCAPE:
		location = ViewType::LONGEVITY_LANDSCAPE;
		break;
	case ViewType::TRADEOFF_LANDSCAPE:
		location = ViewType::TRADEOFF_LANDSCAPE;
		break;
		
	}
	glutPostRedisplay();
}
void createMenu()
{
	int submenuLocation_id = glutCreateMenu(menu);
	glutAddMenuEntry("Physical", (int)ViewType::LOCATION_PHYSICAL);
	glutAddMenuEntry("Nutritional State", (int)ViewType::LOCATION_NUTRITIONAL_STATE);
	glutAddMenuEntry("Intake Target", (int)ViewType::LOCATION_INTAKE_TARGET);
	glutAddMenuEntry("Fecundity Landscape", (int)ViewType::FECUNDITY_LANDSCAPE);
	glutAddMenuEntry("Longevity Landscape", (int)ViewType::LONGEVITY_LANDSCAPE);
	glutAddMenuEntry("Fitness Landscape", (int)ViewType::FITNESS_LANDSCAPE);
	glutAddMenuEntry("Tradeoff Landscape", (int)ViewType::TRADEOFF_LANDSCAPE);
	int submenuColour_id = glutCreateMenu(menu);
	glutAddMenuEntry("Density", (int)ViewType::COLOUR_DENSITY);
	glutAddMenuEntry("Nutritional State", (int)ViewType::COLOUR_NUTRITIONAL_STATE);
	glutAddMenuEntry("Intake Target", (int)ViewType::COLOUR_INTAKE_TARGET);
	glutAddMenuEntry("White", (int)ViewType::COLOUR_WHITE);
	int submenuOutput_id = glutCreateMenu(menu);
	glutAddMenuEntry("All Statistics", OUTPUT_ALL_STATISTICS);
	glutAddMenuEntry("Save State", SAVE_STATE_MENU_ITEM);
	glutAddMenuEntry("Ages", DUMP_AGES_MENU_ITEM);
	glutAddMenuEntry("Death Rates", OUTPUT_DEATHRATE_MENU_ITEM);
	glutAddMenuEntry("Intake Targets", OUTPUT_INTAKE_TARGETS_MENU_ITEM);
	glutAddMenuEntry("Current Nutritional States", OUTPUT_NUTRITIONAL_STATES_MENU_ITEM);
	glutAddMenuEntry("Nutritional States Since Reset", OUTPUT_NUTRITIONAL_STATES_SINCE_RESET_MENU_ITEM);
	glutAddMenuEntry("Nutritional State distributions by age", OUTPUT_NUTRITIONAL_STATES_BY_AGE_MENU_ITEM);
	glutAddMenuEntry("Maturity Ages", OUTPUT_MATURITY_AGES_MENU_ITEM);
	glutAddMenuEntry("Ages since reset", OUTPUT_AGES_SINCE_RESET_MENU_ITEM);
	glutAddMenuEntry("ResetStatistics", RESET_STATISTICS_MENU_ITEM);
	int menu_id = glutCreateMenu(menu);
	glutAddSubMenu("Agent Location", submenuLocation_id);
	glutAddSubMenu("Agent Colour", submenuColour_id);
	glutAddSubMenu("Output", submenuOutput_id);
	glutAttachMenu(GLUT_RIGHT_BUTTON);
}

//! Keyboard events handler for GLUT
void keyboard(unsigned char key, int x, int y) {
	int mod = glutGetModifiers();
	switch (key) {
		case (27):
			exit(0);
			break;
		case '_':
		case '-': // decrease the number of update calls per display call
			if (iterationsPerFrame > 1)
				iterationsPerFrame/=2;
			break;
		case '=':
		case '+': // increase the number of update calls per display call
				iterationsPerFrame *= 2;
			break;
		case 'r': // reset the time increment
			psystem->reset();
			break;
		case 'p':
			paused = !paused;
			break;
		case 'd':

			break;
		case 'b':
		case 'B':
			if (mod == GLUT_ACTIVE_SHIFT) {
				foodTransparency -= 0.1;
				if (foodTransparency < 0)
					foodTransparency = 0;
			} else if (mod == GLUT_ACTIVE_ALT) {
				foodTransparency += 0.1;
				if (foodTransparency > 1)
					foodTransparency = 1;

			} else {
				displayNutrientMap = !displayNutrientMap;
			}
			break;
		case 'c':
		case 'C':
			if (mod == GLUT_ACTIVE_SHIFT) {
				agentTransparency -= 0.1;
				if (agentTransparency < 0)
					agentTransparency = 0;
			} else if (mod == GLUT_ACTIVE_ALT) {
				agentTransparency += 0.1;
				if (agentTransparency > 1)
					agentTransparency = 1;
			}
			break;
		case 'e':
			psystem->randomizeEnvironment();
			break;
		case 'g': showGrid = !showGrid; break;
		case 's':
			psystem->saveEnvironment();
			break;
		case 'l':
			psystem->loadEnvironment();
			break;
		case 'u':
			//loadSimulationParamsFromFile(psystem->getParams(), psystem->getParams().inputFile);
			break;
		case 'n':
			location = (location != ViewType::LOCATION_PHYSICAL) ? ViewType::LOCATION_PHYSICAL : ViewType::COLOUR_NUTRITIONAL_STATE;

	}

	// indicate the display must be redrawn
	glutPostRedisplay();
}

void wrapIfneccessary()
{
	while (left < 0)
	{
		left += (float) psystem->getParams().gridSize.x;
	}
	if (left > psystem->getParams().gridSize.x)
	{
		left = fmodf(left, (float)psystem->getParams().gridSize.x);
	}

	while (top < 0)
	{
		top += (float)psystem->getParams().gridSize.y;
	}
	if (top > psystem->getParams().gridSize.y)
	{
		top = fmodf(top, (float)psystem->getParams().gridSize.y);
	}
}

void mouse(int button, int state, int x, int y) {
	//doubles the zoom level by scrolling up
	if (button == 3 && state == GLUT_DOWN) {
		int oldZoomLevel = zoomLevel;
		zoomLevel *= 2;

		//the new value of the top left hand corner
		top += 0.5f*y / (float)oldZoomLevel;
		left += 0.5f*x / (float)oldZoomLevel;
		if (zoomLevel * psystem->getParams().gridSize.x <= window_width)
			left = 0;
		if (zoomLevel * psystem->getParams().gridSize.y <= window_height)
			top = 0;
	}
	//halves the zoom when scrolling down
	if (button == 4 && state == GLUT_DOWN) {
		int oldZoomLevel = zoomLevel;
		zoomLevel /= 2;
		if (zoomLevel <= 1) {
			zoomLevel = 1;
			top = 0;
			left = 0;
		} else {
			top -= y / (float)oldZoomLevel;
			left -= x / (float)oldZoomLevel;
		}
		if (zoomLevel * psystem->getParams().gridSize.x <= window_width)
			left = 0;
		if (zoomLevel * psystem->getParams().gridSize.y <= window_height)
			top = 0;
		/*if(left<=0)
		 left = window_width - left;
		 if(top<=0)
		 top = window_height - top;*/
	}
	
	//initialize the start of the drag motion by clicking the left mouse button
	if (button == 0&&state== GLUT_DOWN) {
		lastMovement.x = x;
		lastMovement.y = y;
		panning = true;
	}
	if (button == 0 && state == GLUT_UP)
	{
		panning = false;
	}
	wrapIfneccessary();
}

void motion(int x, int y) {
	if (!panning)
		return;
	if (!(zoomLevel * psystem->getParams().gridSize.x <= window_width
					&& zoomLevel * psystem->getParams().gridSize.y <= window_height)) {
		left += (float) (lastMovement.x - x) / (float) zoomLevel;
		top += (float) (lastMovement.y - y) / (float) zoomLevel;
		lastMovement.x = x;
		lastMovement.y = y;
	}
	wrapIfneccessary();
}

//end callbacksPBO.cpp

//simplePBO.cpp

// variables

void createPBO() {
	if (!pbo) {
		// set up vertex data parameter
		int num_texels = window_width * window_height;
		int num_values = num_texels * 4;
		int size_tex_data = sizeof(GLubyte) * num_values;
		cudaDeviceSynchronize();
		// Generate a buffer ID called a PBO (Pixel Buffer Object)
		glGenBuffers(1, &pbo);
		// Make this the current UNPACK buffer (OpenGL is state-based)
		glBindBuffer(GL_PIXEL_UNPACK_BUFFER, pbo);
		// Allocate data for the buffer. 4-channel 8-bit image
		glBufferData(GL_PIXEL_UNPACK_BUFFER, size_tex_data, NULL,
				GL_DYNAMIC_COPY);
		checkCudaErrors(
				cudaGraphicsGLRegisterBuffer(&cuda_pbo_resource, pbo,
						cudaGraphicsMapFlagsWriteDiscard));
	}
}

void deletePBO() {
	if (pbo) {
		// unregister this buffer object with CUDA
		checkCudaErrors(cudaGraphicsUnregisterResource(cuda_pbo_resource));

		glBindBuffer(GL_ARRAY_BUFFER, pbo);
		glDeleteBuffers(1, &pbo);

		pbo = (unsigned int) NULL;
	}
}

void createTexture(GLuint* textureID, unsigned int size_x,
		unsigned int size_y) {
	// Enable Texturing
	glEnable(GL_TEXTURE_2D);

	// Generate a texture identifier
	glGenTextures(1, textureID);

	// Make this the current texture (remember that GL is state-based)
	glBindTexture(GL_TEXTURE_2D, *textureID);

	// Allocate the texture memory. The last parameter is NULL since we only
	// want to allocate memory, not initialize it
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, window_width, window_height, 0,
			GL_BGRA, GL_UNSIGNED_BYTE, NULL);

	// Must set the filter mode, GL_LINEAR enables interpolation when scaling
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	// Note: GL_TEXTURE_RECTANGLE_ARB may be used instead of
	// GL_TEXTURE_2D for improved performance if linear interpolation is
	// not desired. Replace GL_LINEAR with GL_NEAREST in the
	// glTexParameteri() call

}

void deleteTexture(GLuint* tex) {
	glDeleteTextures(1, tex);

	*tex = (unsigned int) NULL;
}

void cleanupCuda() {
	if (pbo)
		deletePBO();
	if (textureID)
		deleteTexture(&textureID);
}

// Run the Cuda part of the computation
void runCuda() {
	uchar4 *dptr;
	// map OpenGL buffer object for writing from CUDA on a single GPU
	// no data is moved (Win & Linux). When mapped to CUDA, OpenGL
	// should not use this buffer
	//cudaGLMapBufferObject((void**)&dptr, pbo);
	checkCudaErrors(cudaGraphicsMapResources(1, &cuda_pbo_resource, 0));
	size_t num_bytes;
	checkCudaErrors(
			cudaGraphicsResourceGetMappedPointer((void **) &dptr, &num_bytes,
					cuda_pbo_resource));
	//printf("bytes allocated %lud",num_bytes);
	// execute the kernel
	launch_kernel(dptr);
	for (int i = 0; i < iterationsPerFrame && !paused && (iterationsRemaining||!timeOut); ++i) {
		psystem->update(1.0f);
		iterationsRemaining--;
	}
	// unmap buffer object
	checkCudaErrors(cudaGraphicsUnmapResources(1, &cuda_pbo_resource, 0));
}

void initCuda(int argc, char** argv) {
	createPBO();
	createTexture(&textureID, window_width, window_height);

	// Clean up on program exit
	atexit(cleanupCuda);

	runCuda();
}
//end simplePBO.cpp
// a timer for FPS calculations

// Simple method to display the Frames Per Second in the window title
void computeFPS() {
	static int fpsCount = 0;
	static int fpsLimit = 10;

	fpsCount++;
	if (fpsCount == fpsLimit) {
		char fps[256];
		float ifps = 1.f / (sdkGetAverageTimerValue(&timer) / 1000.f);
		checkKernelExecution
		sprintf(fps,
				"n = %d, %3.1d ipf , density = %1.2f, Ips = %3.1f, I = %d, %3.1f m",
				psystem->getParams().numBodies, iterationsPerFrame,
				getAverage(psystem->dev.density,psystem->getParams().numBodies),
				ifps * iterationsPerFrame * (int) (!paused),
				psystem->getParams().currentTime,
				psystem->getParams().cellSize * window_width / zoomLevel);
		checkKernelExecution
		glutSetWindowTitle(fps);
		fpsCount = 0;
		sdkResetTimer(&timer);

	}
}
void resizeEvent(int width, int height) {
	
	if (!(width <= 0 || height <= 0))
	{
		//this is because registering a zero sized PBO causes cuda to crash
		window_width = width;
		window_height = height;
	}
	glViewport(0, 0, width, height);
	cleanupCuda();
	initCuda(0, NULL);
}
void fpsDisplay() {
	sdkStartTimer(&timer);

	display();

	sdkStopTimer(&timer);
	computeFPS();
}

bool initGL(int argc, char **argv) {
	//Steps 1-2: create a window and GL context (also register callbacks)
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(window_width, window_height);
	glutCreateWindow("Locust Simulation");
	glutDisplayFunc(fpsDisplay);
	glutKeyboardFunc(keyboard);
	glutMotionFunc(motion);
	glutReshapeFunc(resizeEvent);

	// check for necessary OpenGL extensions
	glewInit();
	if (!glewIsSupported("GL_VERSION_2_0 ")) {
		fprintf(stderr,
				"ERROR: Support for necessary OpenGL extensions missing.");
		return false;
	}

	// Step 3: Setup our viewport and viewing modes
	glViewport(0, 0, window_width, window_height);

	glClearColor(0.0, 0.0, 0.0, 1.0);
	glDisable(GL_DEPTH_TEST);

	// set view matrix
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f);

	return true;
}

void setPaused(bool pause)
{
	paused = pause;
}

int GUI(ParticleSystem * p,int argc, char** argv) {
	psystem = p;
	if (psystem->getParams().numberOfIterations == 0)
		timeOut = false;
	iterationsRemaining = psystem->getParams().numberOfIterations;
	sdkCreateTimer(&timer);

	initGL(argc, argv);
	initCuda(argc, argv);
	createMenu();
	// register callbacks
	glutDisplayFunc(fpsDisplay);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutMotionFunc(motion);

	// start rendering mainloop
	glutMainLoop();

	// clean up
	cudaThreadExit();
	return 0;
}
