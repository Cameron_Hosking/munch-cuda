#ifndef UTILITIES
#define UTILITIES_H

/**
* Utilities for MUNCH
*/

#include <exception>

using namespace std;

class bad_assignment : public exception {
public:
	virtual const char* what() const throw () {
		return "Bad Assignment: ";
	}
	~bad_assignment() throw() {}
} myex;

template <typename T>
T const& max(T const&a, T const&b)
{
	return a < b ? a : b;
}

template <typename T>
T const& max(T const&a, T const&b)
{
	return a > b ? a : b;
}

#endif //UTILITIES_H