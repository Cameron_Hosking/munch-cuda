#include "helper.cuh"
#include <stdio.h>
#include <cuda_runtime.h>
#include <string>
#include <sstream>
#include "thrust/execution_policy.h"
#include "thrust/device_ptr.h"
#include "thrust/for_each.h"
#include "thrust/iterator/zip_iterator.h"
#include "thrust/sort.h"
#include "thrust/find.h"
#include "thrust/fill.h"
#include "PRNG/PRNG.cuh"
#include <vector>

void checkCUDAError(const char *msg)
{
	cudaError_t err = cudaGetLastError();
	if (cudaSuccess != err)
	{
		fprintf(stderr, "Cuda error: %s: %s.\n", msg, cudaGetErrorString(err));
		exit(EXIT_FAILURE);
	}
}

std::ostream & operator<<(std::ostream & o, const gaussian2D & g)
{
	return o << g.max << "," << g.x0 << "," << g.y0 << "," << g.xStddv() << "," << g.yStddv();
}

std::string trim(std::string s)
{
	size_t first = s.find_first_not_of(" \n\r\t");
	if (first == std::string::npos)
		return "";
	size_t last = s.find_last_not_of(" \n\r\t");
	return s.substr(first, (last - first + 1));
}

void viewDeviceArray(void * deviceArray, size_t size)
{
	char * hostArray = new char[size];
	cudaMemcpy(hostArray, deviceArray, size, cudaMemcpyDefault);
	std::cout << hostArray[size - 1] << std::endl;
	delete[] hostArray;
}

std::string getThisPath()
{
	char buffer[MAX_PATH];
	GetModuleFileName(NULL, buffer, MAX_PATH);
	return std::string(buffer);
}

void resetStringstream(std::stringstream & ss)
{
	ss.clear();
	ss.str(std::string());
}

bool outPutToFile(std::string filename, void * data, size_t length)
{
	FILE * outputFile;
	size_t dataWritten = 0;
	outputFile = fopen(filename.c_str(), "wb");
	if (outputFile == NULL)
	{
		std::cout << "failed to open " << filename << std::endl;
		return false;
	}
	size_t chunkSize = 8 * 1024 * 1024; //1MB writes are most efficient
	for (; dataWritten + chunkSize < length; dataWritten += chunkSize)
	{
		fwrite((char *)data + dataWritten, 1, chunkSize, outputFile);	
		if (ferror(outputFile))
		{
			std::cout << "error writing to " << filename << std::endl;
			return false;
		}

	}
	fwrite((char *)data + dataWritten, 1, length - dataWritten, outputFile);
	if (ferror(outputFile))
	{
		std::cout << "error writing to " << filename << std::endl;
		return false;
	}
	if (fclose(outputFile))
	{
		std::cout << "error closing " << filename << std::endl;
		return false;
	}
	return true;
}

bool outPutToFileDeviceData(std::string filename, void * device_data, size_t length)
{
	std::unique_ptr<char> hostData(new char[length]);
	cudaMemcpy(hostData.get(), device_data, length, cudaMemcpyDefault);
	return outPutToFile(filename, hostData.get(), length);
}

bool inputFromFile(std::string filename, void * data, size_t length)
{
	if (length == 0)
	{
		return true;
	}
	FILE * inputFile;
	inputFile = fopen(filename.c_str(), "rb");
	if (inputFile == NULL)
	{
		std::cout << "failed to open " << filename << std::endl;
		return false;
	}
	if (!fread(data, length, 1, inputFile))
	{
		if (ferror(inputFile))
		{
			std::cout << "error reading from " << filename << std::endl;
		}
		else
		{
			std::cout << "EOF reached before required data was read in " << filename << std::endl;
		}
		return false;
	}
	fclose(inputFile);
	if (ferror(inputFile))
	{
		std::cout << "Something very strange has happened, failed to close " << filename << " which was opened read only. Continueing on" << std::endl;
	}
	return true;
}

bool inputFromFileDeviceData(std::string filename, void * device_data, size_t length)
{
	std::unique_ptr<char> hostData(new char[length]);
	if (!inputFromFile(filename, hostData.get(), length))
		return false;
	cudaMemcpy(device_data, hostData.get(), length, cudaMemcpyDefault);
	return true;
}

char * getLocOfCmdLineArg(int argc, char ** argv, const char * command)
{
	if (argc >= 1)
	{
		for (int i = 1; i < argc; i++)
		{
			//ignores leading dashes
			size_t loc = 0;
			while (argv[i][loc] == '-')
			{
				loc++;
			}
			char * arg = argv[i]+loc;

			size_t index = 0;
			bool mismatchFound = false;
			while (command[index] != '\0'&&arg[index] != '\0')
			{
				if (command[index] != arg[index])
				{
					mismatchFound = true;
				}
				index++;
			}
			//the characters don't match or the argument is shorter than the command
			if (mismatchFound || index < strlen(command))
				continue;
			if (arg[index] == '=')
				index++;
			else if(arg[index]!='\0')
			{
				//if this is a flag without a value (no = ) this must be the end of the argument
				//otherwise this just happens to be an argument where the first characters match the flag
				continue;
			}
			return arg + index;
		}
	}
	return nullptr;
}

bool getCmdLineArgument(int argc, char ** argv, const char * command)
{
	return getLocOfCmdLineArg(argc,argv,command) != nullptr;
}

bool setToBoolIfFlagFound(int argc, char ** argv, const char * command, bool setTo, bool & value)
{
	if (getCmdLineArgument(argc, argv, command))
	{
		value = setTo;
		return true;
	}
	return false;
}

bool getCmdLineArgument(int argc, char ** argv, const char * command, char * value)
{
	char* arg = getLocOfCmdLineArg(argc, argv, command);
	if (arg == nullptr)
		return false;
	else
	{
		strcpy(value, arg);
		return true;
	}
}

bool getCmdLineArgument(int argc, char ** argv, const char * command, std::string & value)
{
	char* arg = getLocOfCmdLineArg(argc, argv, command);
	if (arg == nullptr)
		return false;
	else
	{
		value = std::string(arg);
		return true;
	}
}

bool getCmdLineArgument(int argc, char ** argv, const char * command, float2 & value)
{
	std::string asString;
	if(!getCmdLineArgument(argc, argv, command, asString))
		return false;
	std::vector<std::string> words = split(asString, ",");
	float2 tmp;
	try
	{
		tmp.x = std::stof(words.at(0));
		tmp.y = std::stof(words.at(1));
	}
	catch (std::out_of_range e) { return false; }
	catch (std::invalid_argument e) { return false; }
	value = tmp;
	return true;
	
}

bool getCmdLineArgument(int argc, char ** argv, const char * command, gaussian2D & value)
{
	std::string asString;
	if (!getCmdLineArgument(argc, argv, command, asString))
		return false;
	std::vector<std::string> words = split(asString, ",");
	gaussian2D tmp;
	try
	{
		tmp.max = std::stof(words.at(0));
		tmp.x0 = std::stof(words.at(1));
		tmp.y0 = std::stof(words.at(2));
		tmp.setXstddv(std::stof(words.at(3)));
		tmp.setYstddv(std::stof(words.at(4)));	
	}
	catch (std::out_of_range e) { return false; }
	catch (std::invalid_argument e) { return false; }
	value = tmp;
	return true;
}

std::vector<std::string> split(const std::string & stringToSplit,const std::string & delimiter)
{
	//return the empty vector if the string is empty
	if (stringToSplit.empty())
		return std::vector<std::string>();

	std::vector<std::string> results;
	size_t start = 0;
	size_t end = stringToSplit.find(delimiter);
	while (end != std::string::npos)
	{
		results.push_back(stringToSplit.substr(start, end - start));
		start = end + delimiter.size();
		end = stringToSplit.find(delimiter,start);
	}

	results.push_back(stringToSplit.substr(start));
	return results;
}

std::vector<std::pair<std::string, std::string>> stringToListOfKeyValuePairs(const std::string &stringToParse,const std::string &keyValueDelimiter,const std::string &elementDelimiter)
{
	
	std::vector<std::string> elements = split(stringToParse, elementDelimiter);
	
	std::vector<std::pair<std::string, std::string>> keyValuePairs;

	for (std::string element : elements)
	{
		size_t split = element.find(keyValueDelimiter);
		if (split == std::string::npos)
		{
			std::cout << "element \"" << element << "\" is  missing the key value delimiter \"" << elementDelimiter << "\" skipping" << std::endl;
		}
		else
		{
			keyValuePairs.push_back(std::make_pair(element.substr(0, split), element.substr(split + elementDelimiter.size())));
		}
	}
	return keyValuePairs;
}

void setBytesToValue(void * devicePtr, uint8_t value, size_t bytes)
{
	checkCudaErrors(cudaMemset(devicePtr , value, bytes));
}

void setAllToValue(bool * deviceArray, uint n, bool value)
{
	thrust::fill_n(thrust::device_ptr<bool>(deviceArray), n, value);
}
void setAllToValue(char * deviceArray, uint n, char value)
{
	thrust::fill_n(thrust::device_ptr<char>(deviceArray), n, value);
}
void setAllToValue(int8_t * deviceArray, uint n, int8_t value)
{
	thrust::fill_n(thrust::device_ptr<int8_t>(deviceArray), n, value);
}
void setAllToValue(int16_t * deviceArray, uint n, int16_t value)
{
	thrust::fill_n(thrust::device_ptr<int16_t>(deviceArray), n, value);
}
void setAllToValue(uint8_t * deviceArray, uint n, uint8_t value)
{
	thrust::fill_n(thrust::device_ptr<uint8_t>(deviceArray), n, value);
}
void setAllToValue(uint16_t * deviceArray, uint n, uint16_t value)
{
	thrust::fill_n(thrust::device_ptr<uint16_t>(deviceArray), n, value);
}
void setAllToValue(uint64_t * deviceArray, uint n, uint64_t value)
{
	thrust::fill_n(thrust::device_ptr<uint64_t>(deviceArray), n, value);
}
void setAllToValue(int * deviceArray, uint n, int value)
{
	thrust::fill_n(thrust::device_ptr<int>(deviceArray), n, value);
}
void setAllToValue(int2 * deviceArray, uint n, int2 value)
{
	thrust::fill_n(thrust::device_ptr<int2>(deviceArray), n, value);
}
void setAllToValue(int3 * deviceArray, uint n, int3 value)
{
	thrust::fill_n(thrust::device_ptr<int3>(deviceArray), n, value);
}
void setAllToValue(int4 * deviceArray, uint n, int4 value)
{
	thrust::fill_n(thrust::device_ptr<int4>(deviceArray), n, value);
}
void setAllToValue(uint * deviceArray, uint n, uint value)
{
	thrust::fill_n(thrust::device_ptr<uint>(deviceArray), n, value);
}
void setAllToValue(uint2 * deviceArray, uint n, uint2 value)
{
	thrust::fill_n(thrust::device_ptr<uint2>(deviceArray), n, value);
}
void setAllToValue(uint3 * deviceArray, uint n, uint3 value)
{
	thrust::fill_n(thrust::device_ptr<uint3>(deviceArray), n, value);
}
void setAllToValue(uint4 * deviceArray, uint n, uint4 value)
{
	thrust::fill_n(thrust::device_ptr<uint4>(deviceArray), n, value);
}
void setAllToValue(float * deviceArray, uint n, float value)
{
	thrust::fill_n(thrust::device_ptr<float>(deviceArray), n, value);
}
void setAllToValue(float2 * deviceArray, uint n, float2 value)
{
	thrust::fill_n(thrust::device_ptr<float2>(deviceArray), n, value);
}
void setAllToValue(float3 * deviceArray, uint n, float3 value)
{
	thrust::fill_n(thrust::device_ptr<float3>(deviceArray), n, value);
}
void setAllToValue(float4 * deviceArray, uint n, float4 value)
{
	thrust::fill_n(thrust::device_ptr<float4>(deviceArray), n, value);
}

float getAverage(float * deviceArray, uint n)
{
	float sum = thrust::reduce(thrust::device_ptr<float>(deviceArray), thrust::device_ptr<float>(deviceArray + n), 0.0f, thrust::plus<float>());
	return sum / (float)n;
}

float getAverage(uint * deviceArray, uint n)
{
	float sum = thrust::reduce(thrust::device_ptr<uint>(deviceArray), thrust::device_ptr<uint>(deviceArray + n), 0.0f, thrust::plus<uint>());
	return sum / (float)n;
}

struct addFloat2
{
	__host__ __device__ float2 operator()(const float2& x, const float2& y) const
	{
		return make_float2(x.x + y.x, x.y + y.y);
	}
};

float2 getAverage(float2 * deviceArray, uint n)
{
	float2 sum = thrust::reduce(thrust::device_ptr<float2>(deviceArray), thrust::device_ptr<float2>(deviceArray + n), make_float2(0, 0), addFloat2());
	return make_float2(sum.x / (float)n, sum.y / (float)n);
}


__global__ void ratioOfFloat2ArryOnDeviceD(float2 * devArray, float * devResult, size_t size)
{
	uint index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index >= size)
		return;
	devResult[index] = devArray[index].x / devArray[index].y;

}

//This gives the log of the ratio x/y
__global__ void parametriseFloat2(float2 * devArray, float * devResult, size_t size)
{
	uint index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index >= size)
		return;
	devResult[index] = log10f(devArray[index].x / devArray[index].y);
}

void ratioOfFloat2ArryOnDevice(float2 * devArray, float * devResult, size_t size)
{
	uint numThreads, numBlocks;
	computeGridSize(size, 1024, numBlocks, numThreads);
	parametriseFloat2 << <numBlocks, numThreads >> >(devArray, devResult, size);
	checkKernelExecution
}

void allocateArray(void **devPtr, size_t size)
{
	checkCudaErrors(cudaMalloc(devPtr, size));
}

void freeArray(void *devPtr)
{
	checkCudaErrors(cudaFree(devPtr));
}

void threadSync()
{
	checkCudaErrors(cudaDeviceSynchronize());
}

void copyArrayToDevice(void *device, const void *host, int offset, size_t size)
{
	checkCudaErrors(
		cudaMemcpy((char *)device + offset, host, size,
			cudaMemcpyHostToDevice));
}

void * getDeviceArray(void * deviceArray, size_t size)
{
	void * hostData = new char[size];
	copyData(hostData, deviceArray, size);
	return hostData;
}

void copyData(void * dst, void * src, size_t size)
{
	checkCudaErrors(cudaMemcpy(dst, src, size, cudaMemcpyDefault));
}

void copyArrayFromDevice(void* host, const void* device, int offset, size_t size)
{

	checkCudaErrors(cudaMemcpy(host, device, size, cudaMemcpyDeviceToHost));
}

// Round a / b to nearest higher integer value
uint iDivUp(uint a, uint b)
{
	return (a % b != 0) ? (a / b + 1) : (a / b);
}

// compute grid and thread block size for a given number of elements
void computeGridSize(uint n, uint blockSize, uint &numBlocks, uint &numThreads)
{
	if (n == 0)
	{
		numBlocks = 1;
		numThreads = 1;
		return;
	}
	numThreads = min(blockSize, n);
	numBlocks = iDivUp(n, numThreads);
}

__global__ void DrandomUniformInitialization(float * values, float min, float range, uint number, uint4 * RNGStates)
{
	uint index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index >= number)
		return;
	uint4 RNGState = RNGStates[index];
	values[index] = min + randomNumber(RNGState)*range;
	RNGStates[index] = RNGState;
}

void randomUniformInitialization(float * values, float min, float range, uint number, uint4 * RNGStates)
{
	uint numThreads, numBlocks;
	computeGridSize(number, 1024, numBlocks, numThreads);
	DrandomUniformInitialization << <numBlocks, numThreads >> >(values, min, range, number, RNGStates);
	checkKernelExecution
}

__global__ void DrandomNormalInitialization(float * values, float mean, float stddv, uint number, uint4 * RNGStates)
{
	uint index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index >= number)
		return;
	uint4 RNGState = RNGStates[index];
	values[index] = mean + randomGuassian(RNGState).x*stddv;
	RNGStates[index] = RNGState;
}

void randomNormalInitialization(float * values, float mean, float stdv, uint number, uint4 * RNGStates)
{
	uint numThreads, numBlocks;
	computeGridSize(number, 1024, numBlocks, numThreads);
	DrandomNormalInitialization << <numBlocks, numThreads >> >(values, mean, stdv, number, RNGStates);
	checkKernelExecution
}

void cudaInit(int argc, char **argv)
{
	int devID;
	cudaDeviceReset();
	if (checkCmdLineFlag(argc, (const char **)argv, "passiveWaitCPU"))
	{
		cudaSetDeviceFlags(cudaDeviceScheduleBlockingSync);
	}
	cudaDeviceSynchronize();
	// use command-line specified CUDA device, otherwise use device with highest Gflops/s
	devID = findCudaDevice(argc, (const char **)argv);
	//checkCudaErrors(cudaGLSetGLDevice(devID));
	if (devID < 0)
	{
		printf("No CUDA Capable devices found, exiting...\n");
		exit(EXIT_SUCCESS);
	}
}

float findMedian(float* deviceArray, int size)
{
	if (size == 0) return 0;
	float* temp;
	float median;
	allocateArray((void**)&temp, sizeof(float) * size);
	checkCudaErrors(
		cudaMemcpy(temp, deviceArray, sizeof(float) * size,
			cudaMemcpyDeviceToDevice));
	thrust::sort(thrust::device_ptr<float>(temp),
		thrust::device_ptr<float>(temp) + size);
	checkKernelExecution
		cudaMemcpy(&median, temp + size / 2, sizeof(float),
			cudaMemcpyDeviceToHost);
	freeArray(temp);
	return median;
}

/**
* Invoke the thrust::sort of hostSortedArray
* by copying it onto an array on the GPU, using thrust::sort, and then copying the result back.
*/
std::unique_ptr<float[]> sortArray(float* arrayToSort, int size)
{
	// allocate memory on the gpu for the sorted array
	float* temp;
	allocateArray((void**)&temp, sizeof(float) * size);
	// copy array from gpu into sorting array:
	checkCudaErrors(cudaMemcpy(temp, arrayToSort, sizeof(float) * size, cudaMemcpyDefault));

	// sort the data in the temporary array:
	thrust::sort(thrust::device_ptr<float>(temp), thrust::device_ptr<float>(temp) + size);
	checkKernelExecution

		std::unique_ptr<float[]> hostSortedArray(new float[size]);

	// copy the sorted array back to the host:
	cudaMemcpy(hostSortedArray.get(), temp, sizeof(float) * size, cudaMemcpyDefault);

	// free the gpu allocated memory
	freeArray(temp);

	return hostSortedArray;
}
