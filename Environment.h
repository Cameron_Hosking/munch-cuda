/*
 * Environment.h
 *
 *  Created on: 23/09/2014
 *      Author: cameron
 */

#ifndef ENVIRONMENT_H_
#define ENVIRONMENT_H_
#include "vector_types.h"
#include "vector_functions.h"
#include <vector>
#include <math.h>
#include <stdlib.h>
#include "lodepng.h"
#include <iostream>
#include <fstream>

#include "PRNG/PRNG.h"


typedef unsigned int uint;
class Environment {
public:
	std::vector<float4> foods;
	uint2 gridSize;
	float2* nutrientMap;
	Environment();
	Environment(uint2 size);
	Environment(uint2 size,float2* nutrients);
	Environment(std::string fileName);
	~Environment(){delete nutrientMap;}
	void	setFoods(std::vector <float4> &newFoods);
	void	addFood(float protein, float carb, float fractalDimension, float totalAbundance);
	void	generateFractalFoodMap(fastPRNG::PRNG &rng);
	void	clearNutrients();
	float2*	nutrientAt(uint x, uint y) { return &(nutrientMap[x+y*gridSize.x]); }
	float	calcThreshold(float **grid, float targetTotal);
	bool	isNumberAboveThreshold(float **landscape, float threshold, float total);
	void	doSquareAndDiamond(float **grid, const uint2 &a,const uint2 &b, const uint2 &c, const uint2 &d,float noise, float h, fastPRNG::PRNG & rng);
	uint2	getMidpoint(uint2 a,uint2 b)	{ return make_uint2((a.x + b.x)/2,(a.y + b.y)/2); }
	void	putNoisyMidpoints(float **grid, const uint2 &a,const uint2 &b, const uint2 &c, const uint2 &d,	float noise, fastPRNG::PRNG & rng);
	bool	equal(uint2 a, uint2 b){return a.x==b.x&&a.y==b.y;}
	float	max(float a, float b){return (a>b)?a:b;}
	float	min(float a, float b){return (a<b)?a:b;}
	void saveStateToFile(std::string fileName);
	bool loadStateFromFile(std::string fileName);
	void	saveEnvironment(std::string fileName);
	void	loadEnvironment(std::string fileName);
};

#endif /* ENVIRONMENT_H_ */
