
//modified for CUSPP 5 J BUHL
// Modified in 2014 for CUDA 6.0 by Cameron Hosking
// Modified in 2015 by Mike Charleston (MAC)

// Includes
#include <stdlib.h>
#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include <stdint.h>
#include <cstdio>
#include <numeric>
#ifndef _WIN32
	#include <sys/time.h>
#endif // !_WIN32
#include <direct.h>
#include <sstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <map>
#include <set>

#include "defaults.h"
#include "particleSystem.h"

#define GUI_ENABLED

#ifdef GUI_ENABLED
#include "simpleGLmain.cuh"
#endif // GUI_ENABLED



#define MAX_EPSILON_ERROR 5.00f
#define THRESHOLD         0.30f

//Should there be an emulation option here?
	// don't think so -- MAC

//enum { M_VIEW = 0, M_MOVE };
bool gui = true;
//some more global here for parameters from command line

// simulation parameters

ParticleSystem *pSystem = 0;


using namespace std;

ofstream statsOutput;

map<string, string> args;

/**
 * This is code now being managed by Mike Charleston for the MUNCH project.
 * It is based on my original code, but has gone through Jerome Buhl's hands
 * and then through Cameron Hosking's to get it to where it is in March 2015.
 * Now I have got it on an Assembla subversion repository and am managing it.
 * MAC, March 2015, UTas.
 */

void cudaInit(int argc, char **argv);

void cleanup() {
	/**
	 * Nothing to do in the cleanup.
	 */
}

/// Helper method to supply [0,1] uniform-distributed random variate.
inline float frand() {
	return rand() / (float) RAND_MAX;
}

//void addCommand(const char * name, const char *alternate) {
//	if (args.find(name) != args.end()) {
//		throw
//	}
//}
//void setupCommands() {
//	addCommand("-o", "-outfile");
//}

void runNogui(ParticleSystem * psystem) {
	//there is only a timeout if the current time is less than the number of Iterations
	//otherwse either the number of iterations is zero (no timeout) or we've loaded a
	//simulation that has passed the timeout or exited because of timeout
	bool timeOut = psystem->getParams().currentTime <  psystem->getParams().numberOfIterations;
	uint iterationsRemaining = psystem->getParams().numberOfIterations;
	if (timeOut)
	{
		for (int i = 0; i <  psystem->getParams().numberOfIterations; ++i)
		{
			psystem->update(1.0f);
		}
	}
	else
	{
		while (true)
		{
			psystem->update(1.0f);
		}
	}

}

void setupSaveDirectory()
{
	std::string s = pSystem->getParams().experimentName;
	_mkdir(s.c_str());

	//this prevents overwriting the original simulation parameters
	if (!std::ifstream((s + "/SimulationParameters").c_str()).is_open())
	{
		std::ofstream output((s + "/SimulationParameters").c_str());
		pSystem->getParams().output(output);
	}

	_mkdir((s + OUTPUT_DIR).c_str());
	_mkdir((s + SAVES_DIR).c_str());
}

////////////////////////////////////////////////////////////////////////////////
// Program main
////////////////////////////////////////////////////////////////////////////////
int main(int argc, char **argv) {
	//printf("%s Starting...\n\n", sSDKsample);
	cudaInit(argc, argv);
	setToBoolIfFlagFound(argc, argv, "nogui", false, gui);
	std::string filename;
	if (getCmdLineArgument(argc, argv, "load", filename))
	{
		pSystem = new ParticleSystem(filename);
		pSystem->getParams().output(std::cout);

#ifdef GUI_ENABLED
		setPaused(true);
#endif // GUI_ENABLED
	}
	else
	{
		SimParams hostParams;
		hostParams.setUpSimParams(argc, argv);
		hostParams.output(std::cout);
		pSystem = new ParticleSystem(hostParams);
		pSystem->initialize();
		std::string environmentToLoad;
		if (getCmdLineArgument(argc, argv, "loadEnvironment", environmentToLoad))
		{
			pSystem->loadEnvironment(environmentToLoad);
		}

		std::string populationsToload;
		if (getCmdLineArgument(argc, argv, "loadPopulations", populationsToload))
		{
			pSystem->loadPopulations(stringToListOfKeyValuePairs(populationsToload, ",", ";"));
		}
	}

	setupSaveDirectory();

	cudaDeviceSynchronize();
#ifdef GUI_ENABLED
	if (gui) 
		GUI(pSystem, argc, argv);
	else 
#endif // GUI_ENABLED
		runNogui(pSystem);
	
	cudaDeviceSynchronize();
	if (pSystem) {
		delete pSystem;
	}

	cudaDeviceReset();
}
