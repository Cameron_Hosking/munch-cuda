/*
 * Environment.cpp
 *
 *  Created on: 23/09/2014
 *      Author: cameron
 */

#include "Environment.h"
#include "helper.cuh"
Environment::Environment()
{
}
Environment::Environment(uint2 gridSize) :
		foods(), gridSize(gridSize), nutrientMap(
				new float2[gridSize.x * gridSize.y]) {
	clearNutrients();
}

Environment::Environment(uint2 size, float2 * nutrients)
	:foods(), gridSize(gridSize), 
	nutrientMap(new float2[gridSize.x * gridSize.y])
{
	memcpy(nutrientMap, nutrients, gridSize.x * gridSize.y);
}

/**
 * Constructor for the Environment.
 *
 * Load the file provided, which should be a .png file.
 * Use lodepng code to decode that binary image into a vector of unsigned char,
 * then store the char values in nutrientMap
 * XXX WHY?
 */
Environment::Environment(std::string fileName) :
		foods() {
	std::vector<unsigned char> image; //the raw pixels
	unsigned width, height;

	//decode
	unsigned error = lodepng::decode(image, width, height, fileName, LCT_RGB);
	gridSize.x = width;
	gridSize.y = height;
	nutrientMap = new float2[gridSize.x * gridSize.y];

	//if there's an error, display it:
	if (error)
		std::cout << "decoder error " << error << ": "
				<< lodepng_error_text(error) << std::endl;

	int size = gridSize.x * gridSize.y;

	for (int i = 0; i < size; ++i) {
		nutrientMap[i].x = (float) image.at(3 * i) / (float) 255;
		nutrientMap[i].y = (float) image.at(3 * i + 1) / (float) 255;
	}
}

void Environment::setFoods(std::vector<float4>& newFoods)
{
	foods = newFoods;
}


// Add a food to the list of foods in the environment
void Environment::addFood(float protein, float carb, float fractalDimension,
		float totalAbundance) {
	foods.push_back(
			make_float4(protein, carb, fractalDimension, totalAbundance));
}

/**
 * Use the midpoint displacement technique to create a grid of the
 * desired fractal dimension.
 * Pseudocode:
 * let maxtries be the number of times to try generating the landscape
 *
 * @param targetFractalDimension
 */
void Environment::generateFractalFoodMap(fastPRNG::PRNG &rng)
{
	clearNutrients();
	float c1, c2, c3, c4;
	c1 = rng.get0to1float();
	c2 = rng.get0to1float();
	c3 = rng.get0to1float();
	c4 = rng.get0to1float();
	uint2 a = make_uint2(0, 0);
	uint2 b = make_uint2(gridSize.x - 1, 0);
	uint2 c = make_uint2(gridSize.x - 1, gridSize.y - 1);
	uint2 d = make_uint2(0, gridSize.y - 1);
	uint2 m = make_uint2((gridSize.x - 1) / 2, (gridSize.y - 1) / 2);
	float **landscape = new float*[gridSize.x];
	for (unsigned int i = 0; i < gridSize.x; ++i) {
		landscape[i] = new float[gridSize.y];
	}

	//this array is used for calculating overlaps
	//the probability of a new food being placed in a cell is thisScore/(thisScore+oldScore)
	//and then the new score = thisScore+oldScore
	float **scores = new float*[gridSize.x];
	for (unsigned int i = 0; i < gridSize.x; ++i)
	{
		scores[i] = new float[gridSize.y];
		for (int j = 0; j < gridSize.y; ++j)
		{
			scores[i][j] = 0;
		}
	}

	/*
	 * For each food type,
	 * 	generate the fractal landscape according to its target fractal dimension
	 * 	add the food nutritional components together
	 * 	take the minimum of the regrowth value
	 */

	for (std::vector<float4>::iterator iter = foods.begin();
			iter != foods.end(); ++iter) {
		float4 food = *iter;

		for (unsigned int i = 0; i < gridSize.x; ++i) {
			for (unsigned int j = 0; j < gridSize.y; ++j) {
				landscape[i][j] = -1000000;
			}
		}
		landscape[a.x][a.y] = rng.get0to1float();
		landscape[b.x][b.y] = rng.get0to1float();
		landscape[c.x][c.y] = rng.get0to1float();
		landscape[d.x][d.y] = rng.get0to1float();
		landscape[m.x][m.y] = rng.get0to1float();
		/*
		 * The maximum height will start at 1 for the moment.
		 * Once the landscape is made there will be a binary search on a
		 * threshold value to add foods depending on how many cells should
		 * be occupied with each food type.
		 */
		doSquareAndDiamond(landscape, a, b, c, d, 10.0, food.z, rng);
		float minVal = 10000000.0;
		float maxVal = -10000000.0;
		for (unsigned int i = 0; i < gridSize.x; ++i) {
			for (unsigned int j = 0; j < gridSize.y; ++j) {
				maxVal = max(maxVal, landscape[i][j]);
				minVal = min(minVal, landscape[i][j]);
			}
		}
		for (unsigned int i = 0; i < gridSize.x; ++i) {
			for (unsigned int j = 0; j < gridSize.y; ++j) {
				landscape[i][j] = (landscape[i][j] - minVal)
						/ (maxVal - minVal);
			}
		}
		//		string fname = "testingLandscape.csv";
		//		DEBUG(saveCSVLandscape(landscape, fname.c_str()));
		float targetOccupiedCells = food.w * gridSize.x * gridSize.y;
		//		DEBUG (cout << "desired number of cells with food = "
		//				<< targetOccupiedCells << endl);
		float threshold = calcThreshold(landscape, targetOccupiedCells);

		for (unsigned int i = 0; i < gridSize.x; ++i) {
			for (unsigned int j = 0; j < gridSize.y; ++j) {
				if (landscape[i][j] > threshold) {
					float2 *c = nutrientAt(i, j);
					/*
					 *The probability of placing the food in the cell is the inverse of it's abundance
					 *This may seem counter intuitive, but it only applies if another food was already placed
					 *in this cell, if that is the case then the least abundant food is more likely to remain
					 *since this will lead to a final proportion of foods closest to that desired
					 */
					float thisScore = 1.0f / food.w;
					scores[i][j] += thisScore;
					if (thisScore / scores[i][j] > rng.get0to1float())
					{
						*nutrientAt(i, j) = make_float2(food.x, food.y);
					}
				}
			}
		}
	}

	// don't forget to delete things:
	for (unsigned int i = 0; i < gridSize.x; ++i) {
		delete[] landscape[i];
		delete[] scores[i];
	}
	delete[] landscape;
	delete[] scores;


}

void Environment::clearNutrients() {
	float2 empty = make_float2(0.0, 0.0);
	for (int i = 0; i < gridSize.x * gridSize.y; ++i) {
		nutrientMap[i] = empty;
	}
}

/*
 * Calculate and return the threshold, above which a given
 */
float Environment::calcThreshold(float **landscape, float targetTotal) {
	/**
	 * Perform a binary search on the space of possible thresholds for
	 */
	float minVal = 1000000.0;
	float maxVal = 0.0;
	for (unsigned int i = 0; i < gridSize.x; ++i) {
		for (unsigned int j = 0; j < gridSize.y; ++j) {
			maxVal = max(maxVal, landscape[i][j]);
			minVal = min(minVal, landscape[i][j]);
		}
	}
	float middle = (maxVal + minVal) / 2.0;	// this might change later
	// now the binary search:
	// Just do a few iterations.
	float lower = minVal;
	float upper = maxVal;
	while (upper - lower > 0.001) {
		//		DEBUG (
		//				cout << "Number above threshold (" << middle << ")"
		//					<< " = " << calcNumberAboveThreshold(landscape, middle) << endl
		//		);

		if (isNumberAboveThreshold(landscape, middle, targetTotal)) {
			lower = middle;
		} else {
			upper = middle;
		}
		middle = (upper + lower) / 2.0;
	}
	//	DEBUG (cout << "estimated threshold to get correct mass of food: "
	//			<< middle << std::endl);
	return middle;
}

/*
 * Returns true iff the number of pixels in the environment above a given threshold is
 * strictly greater than the total.
 * XXX Why is total a float?
 */
bool Environment::isNumberAboveThreshold(float **landscape, float threshold,
		float total) {
	int num = 0;
	for (unsigned int i = 0; i < gridSize.x; ++i) {
		for (unsigned int j = 0; j < gridSize.y; ++j) {
			num += (landscape[i][j] > threshold) ? 1 : 0;
			if (num > total) {
				return true;
			}
		}
	}
	return false;
}

void Environment::doSquareAndDiamond(float** landscape, const uint2 &a,
		const uint2 &b, const uint2 &c, const uint2 &d, float noise,
		float targetFD, fastPRNG::PRNG &rng) {
	/**
	 * Do the Square-and-Diamond method of fractal landscape generation.
	 * The input is a size x size grid of floats passed as a handle, four
	 * corners a, b, c, and d of a square, the current noise level, and
	 * the target fractal dimension, h.

	 * @pre: landscape is defined and is size x size doubles. The coordinates are
	 * corners of the square.  The corners have values in landscape. Noise is a
	 * positive real value. h is the target fractal dimension and is in the
	 * range [1,2]
	 */
	if (abs((int) (a.x - b.x)) < 2) {
		return;
	}
	putNoisyMidpoints(landscape, a, b, c, d, noise, rng);
	noise /= pow(1.5f, targetFD);
	uint2 w = getMidpoint(a, b);
	uint2 x = getMidpoint(b, c);
	uint2 y = getMidpoint(c, d);
	uint2 z = getMidpoint(d, a);
	uint2 m = getMidpoint(a, c);
	putNoisyMidpoints(landscape, w, x, y, z, noise, rng);
	noise /= pow(1.5f, targetFD);
	doSquareAndDiamond(landscape, a, w, m, z, noise, targetFD, rng);
	doSquareAndDiamond(landscape, w, b, x, m, noise, targetFD, rng);
	doSquareAndDiamond(landscape, m, x, c, y, noise, targetFD, rng);
	doSquareAndDiamond(landscape, z, m, y, d, noise, targetFD, rng);
}

void Environment::putNoisyMidpoints(float **landscape, const uint2 &a,
		const uint2 &b, const uint2 &c, const uint2 &d, float noise, fastPRNG::PRNG &rng) {
	/**
	 * Put midpoints between the coordinates given.  We assume they're provided
	 * in counter-clockwise order.
	 * Add noise by drawing random variates from a Gaussian distribution.
	 */
	// First, derive the midpoints:
	uint2 w, x, y, z;
	w = getMidpoint(a, b);
	if (equal(w, a) || equal(w, b)) {
		return;	// the midpoint is the same as one of the ends
	}
	x = getMidpoint(b, c);
	if (equal(x, b) || equal(x, c)) {
		return;	// the midpoint is the same as one of the ends
	}
	y = getMidpoint(c, d);
	if (equal(y, c) || equal(y, d)) {
		return;	// the midpoint is the same as one of the ends
	}
	z = getMidpoint(d, a);
	if (equal(z, d) || equal(z, a)) {
		return;	// the midpoint is the same as one of the ends
	}

	// Now calculate the averages and add noise:
	if (landscape[w.x][w.y] == -1000000) {
		landscape[w.x][w.y] = (landscape[a.x][a.y] + landscape[b.x][b.y]) / 2.0
				+ noise * (rng.get0to1float() - 0.5);
	}

	if (landscape[x.x][x.y] == -1000000) {
		landscape[x.x][x.y] = (landscape[b.x][b.y] + landscape[c.x][c.y]) / 2.0
				+ noise * (rng.get0to1float() - 0.5);

	}
	if (landscape[y.x][y.y] == -1000000) {
		landscape[y.x][y.y] = (landscape[c.x][c.y] + landscape[d.x][d.y]) / 2.0
				+ noise * (rng.get0to1float() - 0.5);

	}
	if (landscape[z.x][z.y] == -1000000) {
		landscape[z.x][z.y] = (landscape[d.x][d.y] + landscape[a.x][a.y]) / 2.0
				+ noise * (rng.get0to1float() - 0.5);
	}
}

void Environment::saveStateToFile(std::string filename)
{
	outPutToFile(filename + "GridSize.bin", &gridSize, sizeof(uint2));
	outPutToFile(filename + "NutrientMap.bin", nutrientMap, gridSize.x*gridSize.y*sizeof(uint2));
	size_t numFoods = foods.size();
	outPutToFile(filename + "NumberOfFoods", &numFoods, sizeof(size_t));
	if (numFoods != 0)
	{
		outPutToFile(filename + "Foods", &foods[0], foods.size() * sizeof(float4));
	}
}

bool Environment::loadStateFromFile(std::string filename)
{
	if (!inputFromFile(filename + "GridSize.bin", &gridSize, sizeof(uint2))) return false;
	nutrientMap = new float2[gridSize.x * gridSize.y];
	if (!inputFromFile(filename + "NutrientMap.bin", nutrientMap, gridSize.x*gridSize.y * sizeof(uint2))) return false;
	size_t numFoods;
	if (!inputFromFile(filename + "NumberOfFoods", &numFoods, sizeof(size_t))) return false;
	if (numFoods != 0)
	{
		foods = std::vector<float4>(numFoods);
		if (!inputFromFile(filename + "Foods", &foods[0], numFoods * sizeof(float4))) return false;
	}
	return true;
}

void Environment::saveEnvironment(std::string fileName) {
	std::vector<unsigned char> image;
	int size = gridSize.x * gridSize.y;
	for (int i = 0; i < size; ++i) {
		image.push_back(nutrientMap[i].x * 255);
		image.push_back(nutrientMap[i].y * 255);
		image.push_back(0);
		//image.push_back(255);
	}
	//Encode the image
	unsigned error = lodepng::encode(fileName, image, gridSize.x, gridSize.y,
			LCT_RGB);

	//if there's an error, display it
	if (error)
		std::cout << "encoder error " << error << ": "
				<< lodepng_error_text(error) << std::endl;
}
void Environment::loadEnvironment(std::string fileName) {
	std::vector<unsigned char> image; //the raw pixels
	unsigned width, height;

	//decode
	unsigned error = lodepng::decode(image, width, height, fileName, LCT_RGB);
	if (gridSize.x != width || gridSize.y != height) {
		std::cout << "new map incompatible size: " << height << "x" << width
				<< " current gridSize is: " << gridSize.y << "x" << gridSize.x
				<< std::endl;
		return;
	}

	//if there's an error, display it
	if (error)
		std::cout << "decoder error " << error << ": "
				<< lodepng_error_text(error) << std::endl;

	int size = gridSize.x * gridSize.y;
	for (int i = 0; i < size; ++i) {
		nutrientMap[i].x = (float) image.at(3 * i) / (float) 255;
		nutrientMap[i].y = (float) image.at(3 * i + 1) / (float) 255;
	}
}

