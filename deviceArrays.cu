#include "DeviceArrays.cuh"
void DeviceArrays::allocateMemory(SimParams & params)
{
	uint n = params.numBodies;
	uint nMax = params.maxNumBodies;
	uint g = params.numCells;

	// allocate device storage
	unsigned int memSize4 = sizeof(float4) * nMax;

	allocateArray((void**)&posVel, memSize4);
	allocateArray((void**)&crowding, sizeof(unsigned short) * nMax);
	allocateArray((void**)&timer, sizeof(float) * nMax);
	allocateArray((void**)&ageAtMaturity, sizeof(float) * nMax);
	allocateArray((void**)&density, sizeof(uint) * nMax);
	allocateArray((void**)&agentPC, sizeof(float2) * nMax);
	allocateArray((void**)&eating, sizeof(bool) * nMax);
	allocateArray((void**)&scratchPadSpace, sizeof(float) * nMax);
	scratchPadSize = sizeof(float) * nMax;
	allocateArray((void**)&intakeTargets, sizeof(float2) * nMax);
	allocateArray((void**)&socialRanges, sizeof(float3) * nMax);
	allocateArray((void**)&socialStrengths, sizeof(float3) * nMax);
	allocateArray((void**)&aggregateNutrition, sizeof(float2) * nMax);
	allocateArray((void**)&lifetimeReproductiveOutput, sizeof(uint8_t) * nMax);
	allocateArray((void**)&populationTag, sizeof(uint16_t) * nMax);


	allocateArray((void**)&oldPosVel, memSize4);
	allocateArray((void**)&crowdingAlt, sizeof(unsigned short) * nMax);
	allocateArray((void**)&timerAlt, sizeof(float) * nMax);
	allocateArray((void**)&ageAtMaturityAlt, sizeof(float) * nMax);
	allocateArray((void**)&densityAlt, sizeof(uint) * nMax);
	allocateArray((void**)&agentPCAlt, sizeof(float2) * nMax);
	allocateArray((void**)&eatingAlt, sizeof(bool) * nMax);
	allocateArray((void**)&intakeTargetsAlt, sizeof(float2) * nMax);
	allocateArray((void**)&socialRangesAlt, sizeof(float3) * nMax);
	allocateArray((void**)&socialStrengthsAlt, sizeof(float3) * nMax);
	allocateArray((void**)&aggregateNutritionAlt, sizeof(float2) * nMax);
	allocateArray((void**)&lifetimeReproductiveOutputAlt, sizeof(uint8_t) * nMax);
	allocateArray((void**)&populationTagAlt, sizeof(uint16_t) * nMax);

	allocateArray((void**)&reproductionStates, nMax * sizeof(char));

	allocateArray((void**)&gridParticleHash, 2 * nMax * sizeof(uint));
	allocateArray((void**)&gridParticleIndex, 2 * nMax * sizeof(uint));
	allocateArray((void**)&gridParticleHashAlt, 2 * nMax * sizeof(uint));
	allocateArray((void**)&gridParticleIndexAlt, 2 * nMax * sizeof(uint));

	allocateArray((void**)&cellStart, g * sizeof(uint));
	allocateArray((void**)&cellEnd, g * sizeof(uint));

	allocateArray((void**)&nutrientMap, g * sizeof(float2));
	allocateArray((void**)&nutrientAbundance, g * sizeof(float2));

	allocateArray((void**)&RNGStatesForAgents, nMax * sizeof(uint4));
	allocateArray((void**)&RNGStatesForCells, g * sizeof(uint4));


	allocateArray((void**)&agentColours, nMax * sizeof(uchar4));
}
void DeviceArrays::freeMemory()
{
	freeArray(posVel);
	freeArray(crowding);
	freeArray(timer);
	freeArray(ageAtMaturity);
	freeArray(density);
	freeArray(agentPC);
	freeArray(eating);
	freeArray(scratchPadSpace);
	freeArray(intakeTargets);
	freeArray(socialRanges);
	freeArray(socialStrengths);
	freeArray(aggregateNutrition);
	freeArray(lifetimeReproductiveOutput);
	freeArray(populationTag);

	freeArray(oldPosVel);
	freeArray(crowdingAlt);
	freeArray(timerAlt);
	freeArray(ageAtMaturityAlt);
	freeArray(densityAlt);
	freeArray(agentPCAlt);
	freeArray(eatingAlt);
	freeArray(intakeTargetsAlt);
	freeArray(socialRangesAlt);
	freeArray(socialStrengthsAlt);
	freeArray(aggregateNutritionAlt);
	freeArray(lifetimeReproductiveOutputAlt);
	freeArray(populationTagAlt);

	freeArray(gridParticleHash);
	freeArray(gridParticleIndex);
	freeArray(gridParticleHashAlt);
	freeArray(gridParticleIndexAlt);

	freeArray(cellStart);
	freeArray(cellEnd);

	freeArray(nutrientMap);
	freeArray(nutrientAbundance);

	freeArray(RNGStatesForAgents);
	freeArray(RNGStatesForCells);

	freeArray(agentColours);
}

void DeviceArrays::resizeEnvironment(uint newSize)
{
	freeArray(nutrientMap);
	freeArray(nutrientAbundance);

	freeArray(RNGStatesForCells);

	freeArray(cellStart);
	freeArray(cellEnd);

	allocateArray((void**)&cellStart, newSize * sizeof(uint));
	allocateArray((void**)&cellEnd, newSize * sizeof(uint));

	allocateArray((void**)&nutrientMap, newSize * sizeof(float2));
	allocateArray((void**)&nutrientAbundance, newSize * sizeof(float2));
	allocateArray((void**)&RNGStatesForCells, newSize * sizeof(uint4));

}
