#pragma once
#include <stdint.h>
#include "deviceArrays.cuh"

class ParticleSystem;

class Statistics
{
public:
	Statistics(uint32_t maxAge = 10000);
	void resize(uint32_t newMaxAge);
	void reset();
	void save(std::string filename);
	bool load(std::string filename);
	/**This function should only ever be called after the integrate step,
	hence the minimum age will be 1, because we increment the age in the update method
	we decriment the provided ages by 1*/
	void setCurrentAges(float * ages, uint32_t popSize, ParticleSystem & p);
	/**
	This may modify the device array
	*/
	void update(uint32_t oldPop, uint32_t newPop, ParticleSystem& dev);
	void outputAgesEver(const std::string &filename) const;
	void outputDeathRates(const std::string &filename) const;
	void outputAgesAtDeath(const std::string &filename) const;
	void outputNutritionalStatesByAge(const std::string &filename) const;
	void outputNutritionalStatesAtMaturity(const std::string &filename) const;
	void outputNutritionalStatesHistogram(const std::string &filename) const;
	void outputNutritionalStates2Histograms(const std::string &filename) const;
	void outputAggregateNutritionalStates(const std::string &filename) const;
	void outputLifetimeReproductiveOutput(const std::string &filename) const;
	void outputIntakeTargets(const std::string &filename) const;
	void outputBirthsAtAge(const std::string &filename) const;

	~Statistics();
	float2 m_averageAvailableNutrients;
	uint32_t m_maxAge;
	uint64_t * m_dAgesAtDeath;
	uint32_t * m_dAggregateNutrientsAtDeath;
	uint64_t * m_dLifetimeReproductiveOutput;
	uint64_t * m_dBirthsAtAge;
	uint32_t * m_dCurrentAges;
	uint32_t * m_dCurrentAgesAlt;

	//this counts the number of agents that ever reached this age.
	uint64_t * m_dAgesEver;

	uint32_t * m_dDeaths;
	uint32_t * m_dNutritionalStatesByAge;
	uint32_t * m_dNutritionalStatesAtMaturity;
	uint32_t * m_dIntakeTargets;
};

