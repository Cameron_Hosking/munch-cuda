
//modified for CUSPP 5
#include "particleSystem.h"
#include "PRNG/PRNG.h"

#include <cuda_runtime.h>

//#include <helper_functions.h>
#include "nvidiaHelperFiles\helper_cuda.h"

#include <assert.h>
#include <math.h>
#include <memory.h>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <GL/glew.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include "thrust/sort.h"
#include "thrust/device_ptr.h"
#include "particles_kernel_impl.cuh"
#include "defaults.h"

#ifndef CUDART_PI_F
#define CUDART_PI_F         3.141592654f
#endif

ParticleSystem::ParticleSystem(std::string filename)
{
	successfullyLoaded = loadState(filename);
	originalParams = hostParams;
}

ParticleSystem::ParticleSystem(const SimParams & params) :
		m_bInitialized(false), rng(), m_statistics(5000),originalParams(params),hostParams(params)
{
	if (hostParams.seed != NO_SEED)
	{
		rng = fastPRNG::PRNG(hostParams.seed);
	}
}

ParticleSystem::~ParticleSystem() {
	finalize();
}

void ParticleSystem::loadEnvironment(std::string filepath)
{
	ParticleSystem otherSystem(filepath);
	int n = otherSystem.getParams().numCells;
	if (n != getParams().numCells)
	{
		dev.resizeEnvironment(n);
		initializeCellRNGstates();
		initializeSpatialDistributionAndVelocity();
	}
	copyElements(dev.nutrientAbundance, otherSystem.dev.nutrientAbundance, n);
	copyElements(dev.nutrientMap, otherSystem.dev.nutrientMap, n);

	hostParams.gridSize = otherSystem.hostParams.gridSize;
	hostParams.numCells = otherSystem.hostParams.numCells;
}

void ParticleSystem::loadPopulations(std::vector<std::pair<std::string, std::string>> populations)
{
	int currentPopOffset = 0;
	for (auto population = populations.begin();population!=populations.end();++population)
	{
		int agentsRequested = stof(population->second)*hostParams.numBodies;
		ParticleSystem otherSystem(population->first);
		if (!otherSystem.successfullyLoaded || otherSystem.numAgents() == 0) continue;
		while (agentsRequested > 0)
		{
			int agentsToAdd = std::min(otherSystem.numAgents(), agentsRequested);
			//if the simulation hasn't got enough agents to satisfy the quota
			//alter the number required to the remainder
			agentsRequested -= agentsToAdd;

			//set intake target to average
			setAllToValue(otherSystem.dev.intakeTargets, agentsToAdd, getAverage(otherSystem.dev.intakeTargets, otherSystem.numAgents()));

			//tag these pops with the pop tag.
			setAllToValue(dev.populationTag + currentPopOffset, agentsToAdd, hostParams.numPopTags);

			//copy across agents into the main experiment
			copyElements(dev.ageAtMaturity + currentPopOffset, otherSystem.dev.ageAtMaturity, agentsToAdd);
			copyElements(dev.agentPC + currentPopOffset, otherSystem.dev.agentPC, agentsToAdd);
			copyElements(dev.intakeTargets + currentPopOffset, otherSystem.dev.intakeTargets, agentsToAdd);
			copyElements(dev.timer + currentPopOffset, otherSystem.dev.timer, agentsToAdd);
			copyElements(dev.socialRanges + currentPopOffset, otherSystem.dev.socialRanges, agentsToAdd);
			copyElements(dev.socialStrengths + currentPopOffset, otherSystem.dev.socialStrengths, agentsToAdd);
			
			currentPopOffset += agentsToAdd;
		}
		hostParams.numPopTags++;

	}
	hostParams.numBodies = currentPopOffset;

	//resync constant memory since the creation of the other particle systems will have used it
	kernelImplementation::syncDeviceArrays(&dev);
	kernelImplementation::syncHostAndDeviceSimParams(hostParams);
	//since all the statistics need to now take into account these new populations
	resetStatistics();

}

void ParticleSystem::saveState(std::string filename)
{
	std::stringstream ss;
	ss << hostParams.experimentName << "/" << "Iteration" <<hostParams.currentTime <<"save"<< ".cmd";
	std::string scriptFilename = ss.str();
	resetStringstream(ss);
	ss << "cd .." << std::endl <<"\""<< getThisPath() << "\" -load=" << filename << std::endl;
	outPutToFile(scriptFilename, (void *)(ss.str().c_str()), ss.str().length());

	//save params
	hostParams.saveToFile(filename + "Parameters");

	//save statistics
	m_statistics.save(filename + "Statistics");

	//save data
	size_t n = hostParams.numBodies;
	outPutToFileDeviceData(filename + "PosVel.bin", dev.posVel, n * sizeof(float4));
	outPutToFileDeviceData(filename + "Crowding.bin", dev.crowding, n * sizeof(uint16_t));
	outPutToFileDeviceData(filename + "Timers.bin", dev.timer, n * sizeof(float));
	outPutToFileDeviceData(filename + "AgeAtMaturity.bin", dev.ageAtMaturity, n * sizeof(float));
	outPutToFileDeviceData(filename + "Density.bin", dev.density, n * sizeof(uint));
	outPutToFileDeviceData(filename + "AgentPC.bin", dev.agentPC, n * sizeof(float2));
	outPutToFileDeviceData(filename + "Eating.bin", dev.eating, n * sizeof(bool));
	outPutToFileDeviceData(filename + "IntakeTargets.bin", dev.intakeTargets, n * sizeof(float2));
	outPutToFileDeviceData(filename + "SocialRanges.bin", dev.socialRanges, n * sizeof(float3));
	outPutToFileDeviceData(filename + "SocialStrengths.bin", dev.socialStrengths, n * sizeof(float3));
	outPutToFileDeviceData(filename + "ReproductionStates.bin", dev.reproductionStates, n * sizeof(char));
	outPutToFileDeviceData(filename + "AggregateNutrition.bin", dev.aggregateNutrition, n * sizeof(float2));
	outPutToFileDeviceData(filename + "LifetimeReproductiveOutput.bin", dev.lifetimeReproductiveOutput, n * sizeof(uint8_t));
	outPutToFileDeviceData(filename + "PopulationTag.bin", dev.populationTag, n * sizeof(uint16_t));

	outPutToFileDeviceData(filename + "GridParticleHash.bin", dev.gridParticleHash, n * sizeof(uint));
	outPutToFileDeviceData(filename + "GridParticleIndex.bin", dev.gridParticleIndex, n * sizeof(uint));
	outPutToFileDeviceData(filename + "CellStart.bin", dev.cellStart, hostParams.numCells * sizeof(uint));
	outPutToFileDeviceData(filename + "CellEnd.bin", dev.cellEnd, hostParams.numCells * sizeof(uint));

	outPutToFileDeviceData(filename + "RNGStatesForAgents.bin", dev.RNGStatesForAgents, hostParams.maxNumBodies * sizeof(uint4));
	outPutToFileDeviceData(filename + "RNGStatesForCells.bin", dev.RNGStatesForCells, hostParams.numCells * sizeof(uint4));
	outPutToFile(filename + "RNG.bin", &rng, sizeof(fastPRNG::PRNG));

	outPutToFileDeviceData(filename + "NutrientMap.bin", dev.nutrientMap, hostParams.numCells * sizeof(float2));
	outPutToFileDeviceData(filename + "NutrientAbundance.bin", dev.nutrientAbundance, hostParams.numCells * sizeof(float2));

	environment->saveStateToFile(filename + "Environment");

}

bool ParticleSystem::loadState(std::string filename)
{
	hostParams.setDefaultParams();
	if (!hostParams.loadSimulationParamsFromFile(filename + "Parameters")) return false;
	allocateMemory();
	//load statistics
	m_statistics.load(filename + "Statistics");

	//load data
	size_t n = hostParams.numBodies;
	if (!inputFromFileDeviceData(filename + "PosVel.bin", dev.posVel, n * sizeof(float4))) return false;
	if (!inputFromFileDeviceData(filename + "Crowding.bin", dev.crowding, n * sizeof(uint16_t))) return false;
	if (!inputFromFileDeviceData(filename + "Timers.bin", dev.timer, n * sizeof(float))) return false;
	if (!inputFromFileDeviceData(filename + "AgeAtMaturity.bin", dev.ageAtMaturity, n * sizeof(float)))
	{
		setAllToValue(dev.ageAtMaturity, hostParams.numBodies, hostParams.ageOfMaturity);
	}

	if (!inputFromFileDeviceData(filename + "Density.bin", dev.density, n * sizeof(uint))) return false;
	if (!inputFromFileDeviceData(filename + "AgentPC.bin", dev.agentPC, n * sizeof(float2))) return false;
	if (!inputFromFileDeviceData(filename + "Eating.bin", dev.eating, n * sizeof(bool))) return false;
	if (!inputFromFileDeviceData(filename + "IntakeTargets.bin", dev.intakeTargets, n * sizeof(float2))) return false;
	if (!inputFromFileDeviceData(filename + "SocialRanges.bin", dev.socialRanges, n * sizeof(float3))) return false;
	if (!inputFromFileDeviceData(filename + "SocialStrengths.bin", dev.socialStrengths, n * sizeof(float3))) return false;
	if (!inputFromFileDeviceData(filename + "ReproductionStates.bin", dev.reproductionStates, n * sizeof(char))) return false;
	if (!inputFromFileDeviceData(filename + "AggregateNutrition.bin", dev.aggregateNutrition, n * sizeof(float2))) return false;
	if (!inputFromFileDeviceData(filename + "LifetimeReproductiveOutput.bin", dev.lifetimeReproductiveOutput, n * sizeof(uint8_t))) return false;
	if (!inputFromFileDeviceData(filename + "PopulationTag.bin", dev.populationTag, n * sizeof(uint16_t)))
	{
		setAllToValue(dev.populationTag, hostParams.numBodies, 0);
	}

	if (!inputFromFileDeviceData(filename + "GridParticleHash.bin", dev.gridParticleHash, n * sizeof(uint))) return false;
	if (!inputFromFileDeviceData(filename + "GridParticleIndex.bin", dev.gridParticleIndex, n * sizeof(uint))) return false;
	if (!inputFromFileDeviceData(filename + "CellStart.bin", dev.cellStart, hostParams.numCells * sizeof(uint))) return false;
	if (!inputFromFileDeviceData(filename + "CellEnd.bin", dev.cellEnd, hostParams.numCells * sizeof(uint))) return false;

	if (!inputFromFileDeviceData(filename + "RNGStatesForAgents.bin", dev.RNGStatesForAgents, hostParams.maxNumBodies * sizeof(uint4))) return false;
	if (!inputFromFileDeviceData(filename + "RNGStatesForCells.bin", dev.RNGStatesForCells, hostParams.numCells * sizeof(uint4))) return false;

	if (!inputFromFile(filename + "RNG.bin", &rng, sizeof(fastPRNG::PRNG))) return false;

	if (!inputFromFileDeviceData(filename + "NutrientMap.bin", dev.nutrientMap, hostParams.numCells * sizeof(float2))) return false;
	if (!inputFromFileDeviceData(filename + "NutrientAbundance.bin", dev.nutrientAbundance, hostParams.numCells * sizeof(float2))) return false;

	environment = new Environment();
	if (!environment->loadStateFromFile(filename + "Environment")) return false;
	return true;
}

void ParticleSystem::initialize() {
	assert(!m_bInitialized);
	setEnvironment();
	kernelImplementation::syncHostAndDeviceSimParams(hostParams);
	allocateMemory();
	initializeSimulation();
	initializeAgents();

	kernelImplementation::syncDeviceArrays(&dev);
}



void ParticleSystem::finalize() {
	assert(m_bInitialized);
	m_bInitialized = false;
	delete environment;
	dev.freeMemory();


}

void ParticleSystem::copySorted()
{
	uint n = hostParams.numBodies;
	checkCudaErrors(cudaMemcpy(dev.posVel, dev.oldPosVel, n*sizeof(float4),cudaMemcpyDeviceToDevice));
	std::swap(dev.crowding, dev.crowdingAlt);
	std::swap(dev.timer, dev.timerAlt);
	std::swap(dev.ageAtMaturity, dev.ageAtMaturityAlt);
	std::swap(dev.density, dev.densityAlt);
	std::swap(dev.agentPC, dev.agentPCAlt);
	std::swap(dev.eating, dev.eatingAlt);
	std::swap(dev.intakeTargets, dev.intakeTargetsAlt);
	std::swap(dev.socialRanges, dev.socialRangesAlt);
	std::swap(dev.socialStrengths, dev.socialStrengthsAlt);
	std::swap(dev.aggregateNutrition, dev.aggregateNutritionAlt);
	std::swap(dev.lifetimeReproductiveOutput, dev.lifetimeReproductiveOutputAlt);
	std::swap(dev.populationTag, dev.populationTagAlt);
	kernelImplementation::syncDeviceArrays(&dev);
}

void ParticleSystem::initializeAgentRNGstates()
{
	std::unique_ptr<uint4[]> rngStates(new uint4[hostParams.maxNumBodies]);
	for (int i = 0; i < hostParams.maxNumBodies; ++i)
	{
		rng.jump();
		fastPRNG::State s = rng.getState();
		rngStates[i].x = s.bits0_63 >> 32;
		rngStates[i].y = s.bits0_63;
		rngStates[i].z = s.bits64_127 >> 32;
		rngStates[i].w = s.bits64_127;
	}

	copyData(dev.RNGStatesForAgents, rngStates.get(), sizeof(uint4)*hostParams.maxNumBodies);
}

void ParticleSystem::initializeCellRNGstates()
{
	std::unique_ptr<uint4[]> rngStates(new uint4[hostParams.numCells]);

	for (int i = 0; i < hostParams.numCells; ++i)
	{
		rng.jump();
		fastPRNG::State s = rng.getState();
		rngStates[i].x = s.bits0_63 >> 32;
		rngStates[i].y = s.bits0_63;
		rngStates[i].z = s.bits64_127 >> 32;
		rngStates[i].w = s.bits64_127;
	}

	copyData(dev.RNGStatesForCells, rngStates.get(), sizeof(uint4)*hostParams.numCells);

}

void ParticleSystem::allocateMemory()
{
	dev.allocateMemory(hostParams);
	kernelImplementation::syncDeviceArrays(&dev);
	m_bInitialized = true;
}

void ParticleSystem::initializeSimulation()
{
	initializeAgentRNGstates();
	initializeCellRNGstates();
	copyData(dev.nutrientMap, environment->nutrientMap, sizeof(float2)*hostParams.numCells);
	hostParams.currentTime = 0;
	setAllToValue(dev.nutrientAbundance, hostParams.numCells, hostParams.NutrientAbundance);
}

void ParticleSystem::initializeAgents()
{
	initializeSpatialDistributionAndVelocity();
	float3 socialRanges = make_float3(hostParams.attractionRadius,
		hostParams.alignmentRadius, hostParams.repulsionRadius);
	float3 socialStrengths = make_float3(hostParams.attraction,
		hostParams.alignment, hostParams.repulsion);

	//set initial social strengths and ranges
	setAllToValue(dev.socialRanges, hostParams.numBodies, socialRanges);
	setAllToValue(dev.socialStrengths, hostParams.numBodies, socialStrengths);
	setAllToValue(dev.crowding, hostParams.numBodies, 0);
	setAllToValue(dev.timer, hostParams.numBodies, 0);
	setAllToValue(dev.ageAtMaturity, hostParams.numBodies, hostParams.ageOfMaturity);
	setAllToValue(dev.aggregateNutrition, hostParams.numBodies, make_float2(0, 0));
	setAllToValue(dev.lifetimeReproductiveOutput, hostParams.numBodies, 0);
	setAllToValue(dev.populationTag, hostParams.numBodies, 0);

	//randomInitialization(dev.ageAtMaturity, 0, 10000, hostParams.numBodies, dev.RNGStatesForAgents);
	
	//initialize all values to 1, this means on the next iteration there will be no deaths (zeros) and no reproduction (twos)
	setAllToValue(dev.reproductionStates, hostParams.numBodies, 1);
	initializeIntakeTargets();

	setAllToValue(dev.agentPC, hostParams.numBodies, make_float2(0.0, 0.0));
	//copyData(dev.agentPC, dev.intakeTargets, sizeof(float2)*hostParams.numBodies);
}

void ParticleSystem::setEnvironment()
{
	bool mapLoaded = false;

	// check for the presence of an input map of nutrients:
	std::string filename = hostParams.nutritionMapFile;
	if (!filename.empty())
	{
		std::ifstream infile(filename);
		if (infile.good())
		{
			infile.close();
			std::string filename(hostParams.nutritionMapFile);
			environment = new Environment(filename);
			hostParams.foodExperiment = true;
			hostParams.gridSize = environment->gridSize;
			hostParams.numCells = hostParams.gridSize.x*hostParams.gridSize.y;
			mapLoaded = true;
		}
		else
		{
			std::cout << filename << " could not be opened, using default environment\n";
		}
	}

	/*
	* If we haven't loaded any map then we must create one using a fractal distribution:
	*/
	if (!mapLoaded)
	{
		environment = new Environment(hostParams.gridSize);
		environment->setFoods(hostParams.foods);
		environment->generateFractalFoodMap(rng);
	}
}

// step the simulation
void ParticleSystem::update(float deltaTime) {
	
	kernelImplementation::syncHostAndDeviceSimParams(hostParams);
	// calculate grid hash
	if (hostParams.reproductionExperiment)
	{
		//at this point m_dReproductionStates must contain sensible values
		calcHashWithReproductionAndDeath();
	}
	else
	{
		calcHash();
	}
	// sort particles based on hash
	sortParticles();
	bool culled = false;
	//calculate and set the new number of agents
	if (hostParams.reproductionExperiment)
	{
		uint oldNumAgents = hostParams.numBodies;
		hostParams.numBodies = calculateNumberOfAgents();
		if(hostParams.numBodies == hostParams.maxNumBodies)
		{
			culled = true;
			std::stringstream ss;
			ss << hostParams.experimentName << OUTPUT_DIR<<"/";
			dumpDataHist(dev.timer, oldNumAgents, 1, ss.str()+"Ages");
			dumpSpecialization(ss.str()+"ITHist");
			dumpNutritionalStatesHist(ss.str()+"NSHist");

			cull();
			// sort particles based on hash
			sortParticles();
			hostParams.numBodies = calculateNumberOfAgents();
		}
		else
		{
			m_statistics.update(oldNumAgents, hostParams.numBodies,*this);
		}
	}
	hostParams.currentTime++;
	kernelImplementation::syncHostAndDeviceSimParams(hostParams);
	

	// reorder particle arrays into sorted order and
	// find start and end of each cell
	setAllToValue(dev.cellStart, hostParams.numCells, NO_HASH);
	reorderDataAndFindCellStart();

	copySorted(); 
	reproduction();
	//social behaviors
	if (hostParams.socialExperiment)
	{
		collide();
	}

	if (hostParams.foodExperiment) {
		forage();
		eat();
		m_statistics.m_averageAvailableNutrients += calcAverageNutrientAbundance()/hostParams.recordAgentNutrition;
		updateNutrientAbundance();
	}

	// integrate
	integrateSystem();

	if (culled)
	{
		m_statistics.setCurrentAges(dev.timer, hostParams.numBodies, *this);
	}

	if (hostParams.reproductionExperiment)
	{	
		setAllToValue(m_statistics.m_dDeaths, m_statistics.m_maxAge, 0.0f);
		reproductionLifeOrDeath();
	}
	else
	{
		setAllToValue(dev.reproductionStates, hostParams.numBodies, 1);
	}

	if (hostParams.recordAgentNutrition != 0)
	{
		if (hostParams.currentTime % hostParams.recordAgentNutrition == 0)
		{
			std::stringstream summaryString;
			summaryString.precision(4);
			std::stringstream popString;
			//this is the first time we are outputting the summary, add the headers
			if (hostParams.currentTime == hostParams.recordAgentNutrition)
			{
				summaryString << "iteration\tNSx\tNSy\tITx\tITy\tAge\tpopulation\tEnvironment_x\tEnvironment_y\n";
				popString << "iteration";
				for (int i = 0; i < hostParams.numPopTags; ++i)
				{
					popString << "\tpopulation_" << i;
				}
				popString << "\n";
			}
			float2 averageNutrition = getAverage(dev.agentPC, hostParams.numBodies);
			float2 averageIntakeTarget = getAverage(dev.intakeTargets, hostParams.numBodies);
			float averageAge = getAverage(dev.timer, hostParams.numBodies);
			summaryString << hostParams.currentTime << '\t' << averageNutrition.x << '\t' << averageNutrition.y << '\t' <<
				averageIntakeTarget.x << '\t' << averageIntakeTarget.y << '\t' << int(averageAge) << '\t' <<hostParams.numBodies << '\t'
				<< m_statistics.m_averageAvailableNutrients.x << '\t' << m_statistics.m_averageAvailableNutrients.y << std::endl;
			std::cout << summaryString.str();
			m_statistics.m_averageAvailableNutrients = make_float2(0, 0);
			std::fstream fs;
			fs.open(std::string(hostParams.experimentName) + "/summary.tsv", std::ios::app | std::ios::out);
			fs << summaryString.str();
			fs.close();
			

			popString << hostParams.currentTime;
			for (auto count : countNumberOfTags())
			{
				popString << "\t" << count;
			}
			popString << std::endl;
			
			fs.open(std::string(hostParams.experimentName) + "/populations.tsv", std::ios::app | std::ios::out);
			fs << popString.str();
			fs.close();
			std::cout << popString.str();


		}
		


	}

	if (hostParams.currentTime == 1) //reset the statistics after the first iteration since only now are all arrays in a valid state
	{
		resetStatistics();
	}
	if (hostParams.currentTime == hostParams.nextAutoSave)
	{
		hostParams.nextAutoSave *= 2;
		std::stringstream ss;
		ss << hostParams.experimentName << SAVES_DIR << "/" << hostParams.currentTime;
		saveState(ss.str());
		if (hostParams.outputStatisticsOnAutoSave)
		{
			outputAllStatistics();
		}
		if (hostParams.resetStatisticsOnAutoSave)
		{
			resetStatistics();
		}
	}

}

/**
 * Dump to a file the specialisation statistics.
 * The nutritional focus value for each creature is binned into intervals
 *
 */
void ParticleSystem::dumpSpecialization(const std::string filename) {
	ratioOfFloat2ArryOnDevice(dev.intakeTargets, dev.scratchPadSpace, hostParams.numBodies);
	dumpDataHist(dev.scratchPadSpace, hostParams.numBodies, 0.005,filename);
}

void ParticleSystem::dumpNutritionalStatesHist(const std::string filename)
{
	ratioOfFloat2ArryOnDevice(dev.agentPC, dev.scratchPadSpace, hostParams.numBodies);
	dumpDataHist(dev.scratchPadSpace, hostParams.numBodies, 0.005, filename);
}

void ParticleSystem::dumpDataHist(float * dataDev, size_t n, float binSize, const std::string filename)
{
	if (n == 0) return;

	std::ofstream outfile;
	
	outfile.open(filename);
	std::unique_ptr<float[]> sortedData = sortArray(dataDev, n);	// O(n log n)
	
	float current = sortedData[0];
	float step = current;
	int count = 0;
	
	int i = 1;
	//while (current < 0)
	//{
	//	current = sortedData[i];
	//	i++;
	//}
	for (; i < n; ++i)
	{
		float next = sortedData[i];
		//if (next > 10) break;
		while (step + binSize < next)
		{
			outfile << step << "\t" << (float)count / (float)n << "\n";
			step += binSize;
			count = 0;
		}
		count++;
	}
	outfile << step << "\t" << (float)count / (float)n << "\n";
	outfile.close();
}

void ParticleSystem::dumpData(uint * data, size_t n, const std::string filename)
{
	if (n == 0) return;

	std::stringstream fileNameString;
	fileNameString << filename << hostParams.currentTime;
	std::ofstream outfile;

	outfile.open(fileNameString.str().c_str());
	std::unique_ptr<uint[]> hostData(new uint[n]);
	checkCudaErrors(cudaMemcpy(hostData.get(), data, sizeof(uint) * n, cudaMemcpyDefault));
	uint64_t sum = 0;
	for (int i = 0; i < n; ++i)
	{
		sum += hostData[i];
	}
	for (int i = 0; i < n; ++i)
	{
		outfile << i << "\t" << (double)hostData[i]/(double)sum << "\n";
	}
	outfile.close();
}

void ParticleSystem::resetStatistics()
{
	m_statistics.reset();
	m_statistics.setCurrentAges(dev.timer, hostParams.numBodies,*this);
}

void ParticleSystem::outputAllStatistics()
{
	std::string outputFile = std::string(hostParams.experimentName) + OUTPUT_DIR + "/" + std::to_string(hostParams.currentTime) + "_";
	dumpDataHist(dev.timer, hostParams.numBodies, 100.0f, outputFile + "ages");
	m_statistics.outputDeathRates(outputFile + "death_rates");
	dumpSpecialization(outputFile + "IT_hist");
	dumpNutritionalStatesHist(outputFile + "NS_hist");
	m_statistics.outputNutritionalStates2Histograms(outputFile + "NS_hist_since_reset");
	m_statistics.outputNutritionalStatesByAge(outputFile + "NS_dist");
	m_statistics.outputNutritionalStatesAtMaturity(outputFile + "NS_dist_at_maturity");
	dumpDataHist(dev.ageAtMaturity, hostParams.numBodies, 10, outputFile + "maturity_ages");
	m_statistics.outputAgesEver(outputFile + "ages_since_reset");
	m_statistics.outputAgesAtDeath(outputFile + "ages_at_death");
	m_statistics.outputAggregateNutritionalStates(outputFile + "aggregate_nutritional_states_after_maturity");
	m_statistics.outputLifetimeReproductiveOutput(outputFile + "lifetime_reproductive_output");
	m_statistics.outputIntakeTargets(outputFile + "IT_dist");
	m_statistics.outputBirthsAtAge(outputFile + "births_at_age");

}

std::vector<uint16_t> ParticleSystem::countNumberOfTags()
{
	std::unique_ptr<uint16_t[]> poptags = std::unique_ptr<uint16_t[]>(new uint16_t[hostParams.numBodies]);
	copyArrayFromDevice(poptags.get(), dev.populationTag,0, hostParams.numBodies * sizeof(uint16_t));
	std::vector<uint16_t> popTagCounts = std::vector<uint16_t>(hostParams.numPopTags, 0);
	//loop through entire list of pop tags count the number of occurences of each unique tag
	//no tag values should be greater than numTags-1;
	for (int i = 0; i < hostParams.numBodies; ++i)
	{
		popTagCounts[poptags[i]] += 1;
	}
	return popTagCounts;
}

void ParticleSystem::setArray(const void *hdata, void *ddata, int start,
		std::size_t typeSize, int count) {
	assert(m_bInitialized);
	copyArrayToDevice(ddata, hdata, start * typeSize, count * typeSize);
}


void ParticleSystem::reset() 
{
	cudaDeviceSynchronize();
	finalize();
	hostParams = originalParams;
	initialize();
}

void ParticleSystem::initializeSpatialDistributionAndVelocity()
{
	std::unique_ptr<float4[]> hPosVel(new float4[hostParams.numBodies]);

	//initialize random initial directions of movement
	for (uint i = 0; i < hostParams.numBodies; i++)
	{
		float direction = rng.get0to1float()*2.0f*CUDART_PI_F;
		hPosVel[i].z = cosf(direction);
		hPosVel[i].w = sinf(direction);
	}

	float2 exSize = make_float2(hostParams.gridSize.x*hostParams.cellSize, hostParams.gridSize.y*hostParams.cellSize);
	switch (hostParams.initialDistribution)
	{
	case InitialDistribution::UNIFORM:
		for (uint i = 0; i < hostParams.numBodies; i++)
		{
			hPosVel[i].x = rng.get0to1float() * exSize.x;
			hPosVel[i].y = rng.get0to1float() * exSize.y;
		}
		break;

	case InitialDistribution::CIRCLE:
		float x = hostParams.circleLocationAndRadius.x;
		float y = hostParams.circleLocationAndRadius.y;
		float r = hostParams.circleLocationAndRadius.z;

		//default values are used if provided values are negative
		if (x < 0)	x = exSize.x / 2;
		if (y < 0)	y = exSize.y / 2;
		if (r < 0)	r = exSize.x / 4;

		for (uint i = 0; i < hostParams.numBodies; i++)
		{

			//if we want to do this on the gpu use polar coordinates
			//theta = random(0,2pi] and r = R*sqrt(random(0,1])
			//x = r*cos(theta), y = r*sin(theta)
			//this avoids any branching

			//not on the gpu so use rejection sampling
			float tempX;
			float tempY;
			do
			{
				tempX = 2 * rng.get0to1float() - 1;
				tempY = 2 * rng.get0to1float() - 1;
				//if the point does not lie within the circle randomise the point again
			} while (tempX*tempX + tempY*tempY > 1);

			//scale to the actual values
			hPosVel[i].x = tempX*r + x;
			hPosVel[i].y = tempY*r + y;

			//loop around agents that are outside of the boundary
			hPosVel[i].x = wrap(hPosVel[i].x, exSize.x);
			hPosVel[i].y = wrap(hPosVel[i].y, exSize.y);
		}
		break;
	}
	copyData(dev.posVel, hPosVel.get(), hostParams.numBodies * sizeof(float4));
}

void ParticleSystem::initializeIntakeTargets()
{
	if (hostParams.initialIntakeTarget == NO_INITIAL_INTAKE_TARGET)
	{
		std::unique_ptr<float2[]> hIntakeTargets(new float2[hostParams.numBodies]);
		for (uint i = 0; i < hostParams.numBodies; i++)
		{
			hIntakeTargets[i] = make_float2(rng.get0to1float() * 2, rng.get0to1float() * 2);
		}
		copyData(dev.intakeTargets, hIntakeTargets.get(), sizeof(float2)*hostParams.numBodies);
	}
	else
	{
		setAllToValue(dev.intakeTargets, hostParams.numBodies, hostParams.initialIntakeTarget);
	}
	
}
