//modified for CUDA 8
#ifndef SIMULATION_PARAMETERS_H
#define SIMULATION_PARAMETERS_H

#include "vector_types.h"
#include <vector>
#include "helper.cuh"

#define MAX_FILENAME_LENGTH 256
#define MAX_NUM_FOODS 16

typedef unsigned int uint;
enum class InitialDistribution 
{
	CIRCLE,UNIFORM
};
struct GPUSimParams
{
	//base simulation
	uint currentTime;
	uint2 gridSize;
	uint numCells;
	float cellSize;

	uint numBodies;

	//movement
	float defSpeed;
	float noiseIntensity;
	float inertia;

	//social
	float repulsionRadius;
	float alignmentRadius;
	float attractionRadius;
	float attraction;
	float alignment;
	float repulsion;
	uint numOcclusion;

	//survival of the fittest
	float cullProportion;

	//locust specific
	float fastSpeed;

	float jumpDistance;
	float jumpProbability;
	float jumpProbabilityDense;

	float activePeriod;
	float ageOfMaturity;

	float resumeProbability;
	float resumeProbabiltyDense;
	float resumeProbabiltyLone;


	//food things
	float2 NutrientAbundance;
	bool foodExperiment;
	float biteSize;
	float metabolicCostP;
	float metabolicCostC;

	//reproduction things
	uint maxNumBodies;
	gaussian2D longevityLandscape;
	gaussian2D fecundityLandscape;
	float survivalAgeDependancy;
	float predationProbability;
	float intakeTargetStddv;



};

//simulation parameters
struct SimParams : GPUSimParams
{
	uint numberOfIterations;

	uint seed;

	InitialDistribution initialDistribution;

	float3 circleLocationAndRadius;

	bool socialExperiment;
	bool reproductionExperiment;
	float2 initialIntakeTarget;

	std::string nutritionMapFile;
	std::string inputFile;
	std::string experimentName;

	std::vector<float4> foods;

	uint recordAgentNutrition;

	uint nextAutoSave;
	bool resetStatisticsOnAutoSave;
	bool outputStatisticsOnAutoSave;

	uint16_t numPopTags;

	void setUpSimParams(int argc, char **argv);
	void setDefaultParams();
	void setParamsFromCommandLine(int argc, char **argv);
	void saveToFile(const std::string &filename);
	bool loadSimulationParamsFromFile(const std::string &filename);
	void addFoods(std::string foodsString);
	void output(std::ostream &output) const;

};

bool equal(const GPUSimParams &p1,const GPUSimParams &p2);
#endif //SIMULATION_PARAMETERS_H
