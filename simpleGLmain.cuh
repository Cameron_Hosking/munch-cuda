#ifndef SIMPLEGLMAIN_CUH
#define SIMPLEGLMAIN_CUH

#ifdef _WIN32
#include <Windows.h>
#endif

#include "GL/glew.h"

#if defined(__APPLE__) || defined(MACOSX)
#include <GLUT/glut.h>
#else
#include <GL/freeglut.h>
#endif
#include <stdio.h>
#include <cuda_runtime.h>
#include "nvidiaHelperFiles\helper_string.h"
#include "nvidiaHelperFiles\helper_cuda.h"
#include "nvidiaHelperFiles\helper_cuda_gl.h"
#include "nvidiaHelperFiles\helper_timer.h"
#include <cuda_gl_interop.h>
#include "defaults.h"
#include "particleSystem.h"
#include "helper.cuh"
#include <iostream>

namespace kernelGL {
	void syncHostAndDeviceSimParams(GPUSimParams hostParams);
}
__global__ void clearTextureKernel(uchar4* pos, unsigned int width, unsigned int height);
__global__ void zoomedKernel(uint* cellStarts,uint* cellEnds,uchar4* pos, float4 *posvel, uchar4* agentColours,unsigned int windowWidth, unsigned int windowHeight,float startX,float startY,int zoomLevel, float agentTransparency);
__global__ void showNutritionalStates(uchar4* outputTexture, float2* agentPCValues, uchar4 * agentColours, unsigned int windowWidth, unsigned int windowHeight);
__global__ void showAverageLifetimeFitness(uchar4* outputTexture, unsigned int windowWidth, unsigned int windowHeight);
void launch_kernel(uchar4* pos);
void display();
void menu(int num);
void createMenu();
void keyboard(unsigned char key, int x, int y);
void mouse(int button, int state, int x, int y);
void motion(int x, int y);
void resizeEvent(int width, int height);
void createPBO();
void deletePBO();
void createTexture(GLuint* textureID, unsigned int size_x, unsigned int size_y);
void deleteTexture(GLuint* tex);
void cleanupCuda();
void runCuda();
void initCuda(int argc, char** argv);
void computeFPS();
void fpsDisplay();
bool initGL(int argc, char **argv);
void setPaused(bool pause);
int GUI(ParticleSystem * psystem,int argc, char **argv);

#endif
