#include "helper.cuh"
#include "Statistics.h"
#include <fstream>
#include <sstream>
#include <cuda_runtime.h>
#include "device_launch_parameters.h"
#include "device_atomic_functions.h"
#include "vector_functions.h"
#include "cub/cub.cuh"
#include "particleSystem.h"

uint32_t mostSignificantBit(uint32_t v)
{
	static const int multiplyDeBruijnBitPosition[32] =
	{
		0, 9, 1, 10, 13, 21, 2, 29, 11, 14, 16, 18, 22, 25, 3, 30,
		8, 12, 20, 28, 15, 17, 24, 7, 19, 27, 23, 6, 26, 5, 4, 31
	};

	v |= v >> 1; // first round down to one less than a power of 2
	v |= v >> 2;
	v |= v >> 4;
	v |= v >> 8;
	v |= v >> 16;

	return multiplyDeBruijnBitPosition[(uint32_t)(v * 0x07C4ACDDU) >> 27];
}

int ageToBracket(uint32_t maxAge)
{
	return mostSignificantBit(maxAge / 1024);
}

Statistics::Statistics(uint32_t maxAge)
	:m_maxAge(maxAge), m_averageAvailableNutrients(make_float2(0.0,0.0))
{
	allocateArray((void **)&m_dAgesAtDeath, maxAge * sizeof(uint64_t));
	allocateArray((void **)&m_dAgesEver, maxAge * sizeof(uint64_t));
	allocateArray((void **)&m_dCurrentAges, maxAge * sizeof(uint32_t));
	allocateArray((void **)&m_dCurrentAgesAlt, maxAge * sizeof(uint32_t));
	allocateArray((void **)&m_dDeaths, maxAge * sizeof(uint32_t));
	allocateArray((void **)&m_dBirthsAtAge, maxAge * sizeof(uint64_t));
	allocateArray((void **)&m_dNutritionalStatesByAge, (ageToBracket(m_maxAge) + 1) * 90000 * sizeof(uint32_t));
	allocateArray((void **)&m_dNutritionalStatesAtMaturity, 90000 * sizeof(uint32_t));
	allocateArray((void **)&m_dAggregateNutrientsAtDeath, 90000 * sizeof(uint32_t)); 
	allocateArray((void **)&m_dIntakeTargets, 90000 * sizeof(uint32_t));

	allocateArray((void **)&m_dLifetimeReproductiveOutput, 256 * sizeof(uint64_t));

	reset();
}

void Statistics::resize(uint32_t newMaxAge)
{
	//only resize arrays if we're increasing the max age.
	//This prevents losing the data by decreasing available space
	if (newMaxAge < m_maxAge)
	{
		m_maxAge = newMaxAge;
		return;
	}
	freeArray(m_dCurrentAgesAlt);
	allocateArray((void **)&m_dCurrentAgesAlt, newMaxAge * sizeof(uint32_t));
	changeDeviceArraySize(m_dAgesAtDeath, m_maxAge , newMaxAge);
	changeDeviceArraySize(m_dCurrentAges, m_maxAge, newMaxAge );
	changeDeviceArraySize(m_dAgesEver, m_maxAge, newMaxAge);
	changeDeviceArraySize(m_dDeaths, m_maxAge, newMaxAge);
	changeDeviceArraySize(m_dNutritionalStatesByAge, (ageToBracket(m_maxAge) + 1) * 90000, (ageToBracket(newMaxAge) + 1) * 90000);
	changeDeviceArraySize(m_dBirthsAtAge, m_maxAge, newMaxAge);

	m_maxAge = newMaxAge;
}

void Statistics::reset()
{
	setAllToValue(m_dAgesAtDeath, m_maxAge, 0);
	setAllToValue(m_dAgesEver, m_maxAge, 0);
	setAllToValue(m_dCurrentAges, m_maxAge, 0);
	setAllToValue(m_dNutritionalStatesByAge, (ageToBracket(m_maxAge) + 1) * 90000, 0);
	setAllToValue(m_dNutritionalStatesAtMaturity, 90000, 0);
	setAllToValue(m_dAggregateNutrientsAtDeath, 90000, 0);
	setAllToValue(m_dIntakeTargets, 90000, 0);
	setAllToValue(m_dLifetimeReproductiveOutput, 256, 0);
	setAllToValue(m_dBirthsAtAge,m_maxAge, 0);
}

void Statistics::save(std::string filename)
{
	outPutToFile(filename + "MaxAge", &m_maxAge, sizeof(uint32_t));
	outPutToFileDeviceData(filename + "CurrentAges.bin", m_dCurrentAges, m_maxAge * sizeof(uint32_t));
	outPutToFileDeviceData(filename + "AgesAtDeath.bin", m_dAgesAtDeath, m_maxAge * sizeof(uint64_t));
	outPutToFileDeviceData(filename + "AgesEver.bin", m_dAgesEver, m_maxAge * sizeof(uint64_t));
	outPutToFileDeviceData(filename + "BirthsAtAge.bin", m_dBirthsAtAge, m_maxAge * sizeof(uint64_t));

	outPutToFileDeviceData(filename + "Deaths.bin", m_dDeaths, m_maxAge * sizeof(uint32_t));
	outPutToFileDeviceData(filename + "NutritionalStatesByAge.bin", m_dNutritionalStatesByAge, (ageToBracket(m_maxAge) + 1) * 90000 * sizeof(uint32_t));
	outPutToFileDeviceData(filename + "NutritionalStatesAtMaturity.bin", m_dNutritionalStatesAtMaturity, 90000 * sizeof(uint32_t));
	outPutToFileDeviceData(filename + "AggregateNutrientsAtDeath.bin", m_dAggregateNutrientsAtDeath, 90000 * sizeof(uint32_t));
	outPutToFileDeviceData(filename + "IntakeTargets.bin", m_dIntakeTargets, 90000 * sizeof(uint32_t));
	outPutToFileDeviceData(filename + "LifetimeReproductiveFitness.bin", m_dLifetimeReproductiveOutput, 256 * sizeof(uint64_t));

}

bool Statistics::load(std::string filename)
{
	uint32_t newMaxAge = m_maxAge;
	if (!inputFromFile(filename + "MaxAge", &newMaxAge, sizeof(uint32_t))) return false;
	resize(newMaxAge);

	if (!inputFromFileDeviceData(filename + "CurrentAges.bin", m_dCurrentAges, m_maxAge * sizeof(uint32_t))) return false;
	if (!inputFromFileDeviceData(filename + "AgesAtDeath.bin", m_dAgesAtDeath, m_maxAge * sizeof(uint64_t))) return false;
	if (!inputFromFileDeviceData(filename + "AgesEver.bin", m_dAgesEver, m_maxAge * sizeof(uint64_t))) return false;

	if (!inputFromFileDeviceData(filename + "Deaths.bin", m_dDeaths, m_maxAge * sizeof(uint32_t))) return false;
	if (!inputFromFileDeviceData(filename + "NutritionalStatesByAge.bin", m_dNutritionalStatesByAge, (ageToBracket(m_maxAge) + 1) * 90000 * sizeof(uint32_t)))
	{
		setAllToValue(m_dNutritionalStatesByAge, (ageToBracket(m_maxAge) + 1) * 90000, 0);
	}
	if (!inputFromFileDeviceData(filename + "NutritionalStatesAtMaturity.bin", m_dNutritionalStatesAtMaturity, 90000 * sizeof(uint32_t)))
	{
		setAllToValue(m_dNutritionalStatesAtMaturity, 90000, 0);
	}
	if (!inputFromFileDeviceData(filename + "AggregateNutrientsAtDeath.bin", m_dAggregateNutrientsAtDeath, 90000 * sizeof(uint32_t)))
	{
		setAllToValue(m_dAggregateNutrientsAtDeath, 90000, 0);
	}
	if (!inputFromFileDeviceData(filename + "LifetimeReproductiveFitness.bin", m_dLifetimeReproductiveOutput, 256 * sizeof(uint64_t)))
	{
		setAllToValue(m_dLifetimeReproductiveOutput, 256, 0);
	}
	if (!inputFromFileDeviceData(filename + "IntakeTargets.bin", m_dIntakeTargets, 90000 * sizeof(uint32_t)))
	{
		setAllToValue(m_dIntakeTargets, 90000, 0);
	}
	if (!inputFromFileDeviceData(filename + "BirthsAtAge.bin", m_dBirthsAtAge, m_maxAge * sizeof(uint64_t)))
	{
		setAllToValue(m_dBirthsAtAge, m_maxAge, 0);
	}

	return true;
}

__global__ void DsetCurrentAges(float * ages, uint32_t * countedAges, uint32_t popSize)
{
	uint index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index >= popSize)
		return;
	atomicAdd(countedAges + __float2uint_rn(ages[index] - 1), 1);
}

void Statistics::setCurrentAges(float * ages, uint32_t popSize, ParticleSystem& p)
{

	float newMaxAge;
	//calculate current max age
	float * result;
	cudaMalloc(&result, sizeof(float));

	size_t   temp_storage_bytes = 0;
	cub::DeviceReduce::Max(NULL, temp_storage_bytes, ages, result, popSize);
	p.increaseScratchPadSize(temp_storage_bytes);
	cub::DeviceReduce::Max(p.dev.scratchPadSpace, p.dev.scratchPadSize, ages, result, popSize);
	
	cudaMemcpy(&newMaxAge, result, sizeof(float), cudaMemcpyDeviceToHost);
	cudaFree(result);

	//set new max age as double current max age
	resize(2*static_cast<uint32_t>(newMaxAge));
	setAllToValue(m_dCurrentAges, m_maxAge, 0);

	uint numThreads, numBlocks;
	computeGridSize(popSize, 1024, numBlocks, numThreads);
	DsetCurrentAges <<<numBlocks, numThreads >>> (ages,m_dCurrentAges,popSize);
	checkKernelExecution
}

__global__ void Dupdate(uint32_t *oldAges, uint32_t * newAges, uint32_t * deathsAtAge, uint64_t * agesEver, uint64_t * totalDeathsAtAge, int32_t populationDelta, uint32_t maxAge)
{
	uint index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index >= maxAge)
		return;
	if (index == 0)
	{
		uint32_t newAgents = newAges[0] + populationDelta;
		newAges[0] = newAgents;
		agesEver[0] += newAgents;	
		totalDeathsAtAge[0] += deathsAtAge[0];
	}
	else
	{
		uint32_t deaths = deathsAtAge[index];
		uint32_t numberAtAge = oldAges[index - 1];
		agesEver[index] += numberAtAge;
		numberAtAge -= deaths;
		newAges[index] = numberAtAge;
		totalDeathsAtAge[index] += deaths;
		
	}

}

/*
*deaths[0] should always be 0 since this update method should be called directly after reproducation/death at which point
*agents who have just been born are the only ones at age 0 and haven't had a chance to die yet
*/
void Statistics::update(uint32_t oldPop, uint32_t newPop,ParticleSystem& p)
{

	size_t   temp_storage_bytes = 0;
	cub::DeviceReduce::Sum(NULL, temp_storage_bytes, m_dDeaths, m_dCurrentAgesAlt, m_maxAge);

	p.increaseScratchPadSize(temp_storage_bytes);

	cub::DeviceReduce::Sum(p.dev.scratchPadSpace, p.dev.scratchPadSize, m_dDeaths, m_dCurrentAgesAlt, m_maxAge);

	uint numThreads, numBlocks;
	computeGridSize(m_maxAge, 1024, numBlocks, numThreads);
	Dupdate << <numBlocks, numThreads >> > (m_dCurrentAges, m_dCurrentAgesAlt, m_dDeaths,m_dAgesEver,m_dAgesAtDeath,newPop-oldPop,m_maxAge);
	checkKernelExecution
	
	std::swap(m_dCurrentAges, m_dCurrentAgesAlt);
	uint32_t agentsAtMaxAge;
	copyData(&agentsAtMaxAge, &m_dCurrentAges[m_maxAge - 1], sizeof(uint32_t));
	if (agentsAtMaxAge != 0)
	{
		resize(m_maxAge * 2);
	}
}

void Statistics::outputAgesEver(const std::string &filename) const
{
	auto agesEver = std::unique_ptr<uint64_t[]>((uint64_t*)getDeviceArray(m_dAgesEver, sizeof(uint64_t)*m_maxAge));

	std::ofstream outfile;
	outfile.open(filename);
	double sum = 0;
	for (int i = 0; i < m_maxAge; ++i)
	{
		sum += (double)agesEver[i];
	}
	for (int i = 0; i < m_maxAge; ++i)
	{
		outfile << i << "\t" << (double)agesEver[i] <<"\t" << (double)agesEver[i]/sum << "\n";
	}
	outfile.close();
}

void Statistics::outputDeathRates(const std::string &filename) const
{
	auto agesAtDeath = std::unique_ptr<uint64_t[]>((uint64_t*)getDeviceArray(m_dAgesAtDeath, sizeof(uint64_t)*m_maxAge));
	auto agesEver = std::unique_ptr<uint64_t[]>((uint64_t*)getDeviceArray(m_dAgesEver, sizeof(uint64_t)*m_maxAge));

	std::ofstream outfile;
	outfile.open(filename);
	for (int i = 0; i < m_maxAge; ++i)
	{
		if(agesEver[i]!=0)
			outfile << i << "\t" << (double)agesAtDeath[i] / (double)agesEver[i] << "\n";
	}
	outfile.close();
}

void Statistics::outputAgesAtDeath(const std::string &filename) const
{
	auto agesAtDeath = std::unique_ptr<uint64_t[]>((uint64_t*)getDeviceArray(m_dAgesAtDeath, sizeof(uint64_t)*m_maxAge));
	double sum = 0;
	for (int i = 0; i < m_maxAge; ++i)
	{
		sum += agesAtDeath[i];
	}
	std::ofstream outfile;
	outfile.open(filename);
	for (int i = 0; i < m_maxAge; ++i)
	{
		outfile << i << "\t" << agesAtDeath[i]/sum << "\n";
	}
	outfile.close();
}

void Statistics::outputNutritionalStatesByAge(const std::string &filename) const
{
	int ageBrackets = ageToBracket(m_maxAge) + 1;
	std::unique_ptr<uint32_t[]> nutritionalStatesByAge = std::unique_ptr<uint32_t[]>(new uint32_t[ageBrackets * 90000]);
	copyData(nutritionalStatesByAge.get(), m_dNutritionalStatesByAge, ageBrackets * 90000 * sizeof(uint32_t));
	for (int ageBracket = 0; ageBracket < ageBrackets; ageBracket++)
	{

		double sum = 0;
		for (int i = 0; i < 90000; ++i)
		{
			sum += nutritionalStatesByAge[90000 * ageBracket + i];
		}
		if (sum > 0)
		{
			std::ofstream outfile;
			std::stringstream ss;
			ss << filename << "Ages" << 1024 * ((1 << 21) >> (22 - ageBracket)) << "to" << (1024 << ageBracket);
			outfile.open(ss.str());
			for (int i = 0; i < 300; ++i)
			{
				for (int j = 0; j < 300; j++)
				{
					uint32_t num = nutritionalStatesByAge[90000 * ageBracket + 300 * i + j];
					if (num > 0)
						outfile << 0.01*(float)i - 1.0f << "\t" << 0.01*(float)j - 1.0f << "\t" << num << "\t" << num / sum << "\n";
				}
			}
			outfile.close();
		}
	}
}
	
void Statistics::outputNutritionalStatesAtMaturity(const std::string &filename) const
{
	std::unique_ptr<uint32_t[]> nutritionalStatesAtMaturity = std::unique_ptr<uint32_t[]>(new uint32_t[90000]);
	copyData(nutritionalStatesAtMaturity.get(), m_dNutritionalStatesAtMaturity, 90000 * sizeof(uint32_t));
	double sum = 0;
	for (int i = 0; i < 90000; ++i)
	{
		sum += nutritionalStatesAtMaturity[i];
	}
	std::ofstream outfile;
	outfile.open(filename);
	for (int i = 0; i < 300; ++i)
	{
		for (int j = 0; j < 300; j++)
		{
			uint32_t num = nutritionalStatesAtMaturity[300 * i + j];
			if (num > 0)
				outfile << 0.01*(float)i - 1.0f << "\t" << 0.01*(float)j - 1.0f << "\t" << num << "\t" << num / sum << "\n";
		}
	}
	outfile.close();	
}

void Statistics::outputNutritionalStatesHistogram(const std::string &filename) const
{
	int ageBrackets = ageToBracket(m_maxAge) + 1;
	std::unique_ptr<uint32_t[]> nutritionalStatesByAge = std::unique_ptr<uint32_t[]>(new uint32_t[ageBrackets * 90000]);
	copyData(nutritionalStatesByAge.get(), m_dNutritionalStatesByAge, ageBrackets * 90000 * sizeof(uint32_t));
	std::unique_ptr<uint64_t[]> hist = std::unique_ptr<uint64_t[]>(new uint64_t[100]);
	uint64_t sum = 0;
	for (int i = 0; i < 100; ++i)
	{
		hist[i] = 0;
	}
	for (int ageBracket = 0; ageBracket < ageBrackets; ageBracket++)
	{

		for (int i = 101; i < 200; ++i)
		{
			for (int j = 101; j < 200; j++)
			{
				uint32_t num = nutritionalStatesByAge[90000 * ageBracket + 300 * i + j];
				sum += num;
				hist[(100 * (i - 100)) / (i - 100 + j - 100)] += num;
					
			}
		}
		
	}
	std::ofstream outfile;
	outfile.open(filename);
	for (int i = 0; i < 100; ++i)
	{
		outfile << double(i)*0.01 << "\t" << (double)hist[i] <<"\t" << (double)hist[i]/(double)sum << "\n";
	}
	outfile.close();
}

void Statistics::outputNutritionalStates2Histograms(const std::string &filename) const
{
	int ageBrackets = ageToBracket(m_maxAge) + 1;
	std::unique_ptr<uint32_t[]> nutritionalStatesByAge = std::unique_ptr<uint32_t[]>(new uint32_t[ageBrackets * 90000]);
	copyData(nutritionalStatesByAge.get(), m_dNutritionalStatesByAge, ageBrackets * 90000 * sizeof(uint32_t));
	std::unique_ptr<uint64_t[]> carbHist = std::unique_ptr<uint64_t[]>(new uint64_t[300]);
	std::unique_ptr<uint64_t[]> proteinHist = std::unique_ptr<uint64_t[]>(new uint64_t[300]);
	uint64_t sum = 0;
	for (int i = 0; i < 300; ++i)
	{
		carbHist[i] = 0;
		proteinHist[i] = 0;
	}
	for (int ageBracket = 0; ageBracket < ageBrackets; ageBracket++)
	{

		for (int i = 0; i < 300; ++i)
		{
			for (int j = 0; j < 300; j++)
			{
				uint32_t num = nutritionalStatesByAge[90000 * ageBracket + 300 * i + j];
				sum += num;
				proteinHist[i] += num;
				carbHist[j] += num;
			}
		}

	}
	std::ofstream outfile;
	outfile.open(filename+"P");
	for (int i = 0; i < 300; ++i)
	{
		outfile << double(i)*0.01 - 1.0 << "\t" << (double)proteinHist[i] << "\t" << (double)proteinHist[i] / (double)sum << "\n";
	}
	outfile.close();

	outfile.open(filename + "C");
	for (int i = 0; i < 300; ++i)
	{
		outfile << double(i)*0.01 - 1.0 << "\t" << (double)carbHist[i] <<"\t"<< (double)carbHist[i] / (double)sum << "\n";
	}
	outfile.close();

}

void Statistics::outputAggregateNutritionalStates(const std::string &filename) const
{
	std::unique_ptr<uint32_t[]> aggregateNutrientsAtDeath = std::unique_ptr<uint32_t[]>(new uint32_t[90000]);
	copyData(aggregateNutrientsAtDeath.get(), m_dAggregateNutrientsAtDeath, 90000 * sizeof(uint32_t));
	double sum = 0;
	for (int i = 0; i < 90000; ++i)
	{
		sum += aggregateNutrientsAtDeath[i];
	}
	std::ofstream outfile;
	outfile.open(filename);
	for (int i = 0; i < 300; ++i)
	{
		for (int j = 0; j < 300; j++)
		{
			uint32_t num = aggregateNutrientsAtDeath[300 * i + j];
			if (num > 0)
				outfile << 0.01*(float)i - 1.0f << "\t" << 0.01*(float)j - 1.0f << "\t" << num <<"\t" << num/sum << "\n";
		}
	}
	outfile.close();
}

void Statistics::outputLifetimeReproductiveOutput(const std::string &filename) const
{
	auto lifeTimeReproductiveOutput = std::unique_ptr<uint64_t[]>((uint64_t*)getDeviceArray(m_dLifetimeReproductiveOutput, sizeof(uint64_t)*256));

	std::ofstream outfile;
	outfile.open(filename);
	int last = 0;
	for (int i = 0; i < 256; ++i)
	{
		if (lifeTimeReproductiveOutput[i])
		{
			last = i;
		}
	}
	for (int i = 0; i <= last; ++i)
	{
		outfile << i << "\t" << lifeTimeReproductiveOutput[i] << "\n";
	}
	outfile.close();
}

void Statistics::outputIntakeTargets(const std::string &filename) const
{
	std::unique_ptr<uint32_t[]> intakeTargets = std::unique_ptr<uint32_t[]>(new uint32_t[90000]);
	copyData(intakeTargets.get(), m_dIntakeTargets, 90000 * sizeof(uint32_t));
	double sum = 0;
	for (int i = 0; i < 90000; ++i)
	{
		sum += intakeTargets[i];
	}
	std::ofstream outfile;
	outfile.open(filename);
	for (int i = 0; i < 300; ++i)
	{
		for (int j = 0; j < 300; j++)
		{
			uint32_t num = intakeTargets[300 * i + j];
			if (num > 0)
				outfile << 0.01*(float)i << "\t" << 0.01*(float)j << "\t"<< num <<"\t" << num / sum << "\n";
		}
	}
	outfile.close();
}

void Statistics::outputBirthsAtAge(const std::string & filename) const
{
	auto birthsAtAge = std::unique_ptr<uint64_t[]>((uint64_t*)getDeviceArray(m_dBirthsAtAge, sizeof(uint64_t)*m_maxAge));
	double sum = 0;
	for (int i = 0; i < m_maxAge; ++i)
	{
		sum += birthsAtAge[i];
	}
	std::ofstream outfile;
	outfile.open(filename);
	for (int i = 0; i < m_maxAge; ++i)
	{
		outfile << i << "\t" << birthsAtAge[i] << "\t" << birthsAtAge[i] / sum << "\n";
	}
	outfile.close();
}

Statistics::~Statistics()
{
	freeArray(m_dAgesAtDeath);
	freeArray(m_dAgesEver);
	freeArray(m_dCurrentAges);
	freeArray(m_dCurrentAgesAlt);
	freeArray(m_dDeaths);
	freeArray(m_dNutritionalStatesByAge);
	freeArray(m_dNutritionalStatesAtMaturity);
	freeArray(m_dAggregateNutrientsAtDeath);
	freeArray(m_dLifetimeReproductiveOutput);
	freeArray(m_dIntakeTargets);

}
